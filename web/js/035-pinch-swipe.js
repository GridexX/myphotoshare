(function() {

	var util = new Utilities();
	var swipeSpeed = 350;
	var pinchSpeed = 500;
	var dragSpeed = 500;
	var mediaContainerSelector = ".media-box#center .media-box-inner";
	var mediaSelector = mediaContainerSelector + " img";
	var zoomIncrement = 1.5625, zoomDecrement = 1 / zoomIncrement;
	var currentTranslateX = 0;
	var currentTranslateY = 0;
	var previousClientX;
	var previousClientY;
	var baseZoom;

	var maxAllowedTranslateX, maxAllowedTranslateY;
	var mediaWidth, mediaHeight;
	var mediaBoxInnerWidth, mediaBoxInnerHeight;
	var photoWidth, photoHeight;
	var previousFingerEnd;


	var dragVector;
	var pastMediaWidthOnScreen, pastMediaHeightOnScreen, pastMediaRatioOnScreen;

	/* constructor */
	function PinchSwipe() {
	}

	PinchSwipe.cssWidth = function(mediaSelector) {
		return $(mediaSelector).css("width");
	};

	PinchSwipe.cssHeight = function(mediaSelector) {
		return $(mediaSelector).css("height");
	};

	PinchSwipe.reductionWidth = function(mediaSelector) {
		return $(mediaSelector).attr("width");
	};

	PinchSwipe.reductionHeight = function(mediaSelector) {
		return $(mediaSelector).attr("height");
	};

	PinchSwipe.pinched = function(mediaSelector, initialCssWidth) {
		return PinchSwipe.cssWidth(mediaSelector) > initialCssWidth;
	};

	PinchSwipe.scrollMedia = function(distance) {
		$("#media-box-container").css("transition-duration", "0s");

		//inverse the number we set in the css
		var value = (distance <= 0 ? "" : "-") + Math.abs(distance).toString();
		$("#media-box-container").css("transform", "translate(" + value + "px,0)");
	};

	PinchSwipe.swipeMedia = function(distance) {
		$("#media-box-container").css("transition-duration", swipeSpeed + "ms");

		//inverse the number we set in the css
		var value = (distance <= 0 ? "" : "-") + Math.abs(distance).toString();
		$("#media-box-container").css("transform", "translate(" + value + "px,0)");
	};

	PinchSwipe.pinchInOut = function(finalZoom, duration, center) {
		return new Promise(
			function(resolve_pinchInOut) {
				var startZoom = env.currentZoom;
				var windowCenter = {x: env.windowWidth / 2, y: env.windowHeight / 2};
				if (center === null)
					center = windowCenter;
				var centersDifference = {x: center.x - windowCenter.x, y: center.y - windowCenter.y};
				var [currentReductionSize, currentReductionIndex] = util.currentSizeAndIndex();
				var width, height;
				var photoSize = Math.max(... env.currentMedia.metadata.size);
				// scaleZoom is the value we must give to the scale part of the transform css property.
				// In css("transform", ...), scale(1) means that the image fits into the given width/height values
				var scaleZoom = finalZoom / env.initialZoom;
				if (env.currentMedia.isRotated($("#media-center")))
					scaleZoom *= PinchSwipe.scaleFactorForRotation(env.currentMedia, $("#media-center"));

				var object = PinchSwipe.getTransformParameters($(mediaSelector));
				var cssTransformRotate = object.rotate;
				var cssTransformTranslateX = object.translateX;
				var cssTransformTranslateY = object.translateY;

				// if (startZoom > env.initialZoom) {
				if (finalZoom > env.initialZoom) {
					// translation must be changed accordingly
					cssTransformTranslateX = (cssTransformTranslateX - centersDifference.x) * (finalZoom / startZoom) + centersDifference.x;
					cssTransformTranslateY = (cssTransformTranslateY - centersDifference.y) * (finalZoom / startZoom) + centersDifference.y;
				}

				// the following values are expressed in terms of the current zoom sizes

				// maxAllowedTranslateX = (photoWidth * finalZoom - env.windowWidth) / 2;
				// if (maxAllowedTranslateX < 0)
				// 	maxAllowedTranslateX = 0;
				// maxAllowedTranslateY = (photoHeight * finalZoom - env.windowHeight) / 2;
				// if (maxAllowedTranslateY < 0)
				// 	maxAllowedTranslateY = 0;
				//
				// cssTransformTranslateX = Math.max(Math.min(cssTransformTranslateX, maxAllowedTranslateX), - maxAllowedTranslateX);
				// cssTransformTranslateY = Math.max(Math.min(cssTransformTranslateY, maxAllowedTranslateY), - maxAllowedTranslateY);

				var cssTranslateXString = cssTransformTranslateX.toString();
				var cssTranslateYString = cssTransformTranslateY.toString();
				var cssScale = scaleZoom.toString();
				var parameter = PinchSwipe.prepareTransformParameter(cssTransformRotate, cssTranslateXString, cssTranslateYString, cssScale);
				if (duration === undefined)
					duration = 200;

				$(mediaSelector).css("transition-duration", duration + "ms");
				$(mediaSelector).css("transform", parameter);

				if (finalZoom > startZoom) {
					var nextReductionSize = currentReductionSize;
					var nextReductionIndex = currentReductionIndex;
					if (currentReductionIndex !== -1) {
						while (nextReductionIndex !== -1 && nextReductionSize < photoSize * finalZoom) {
							if (nextReductionIndex === 0) {
								nextReductionIndex = -1;
								nextReductionSize = photoSize;
							} else {
								nextReductionIndex -= 1;
								nextReductionSize = env.options.reduced_sizes[nextReductionIndex];
							}
						}

						if (photoWidth > photoHeight) {
							width = nextReductionSize;
							height = parseInt(nextReductionSize / photoWidth * photoHeight);
						} else {
							height = nextReductionSize;
							width = parseInt(nextReductionSize / photoHeight * photoWidth);
						}
						let photoSrc;
						if (nextReductionIndex === -1)
							photoSrc = env.currentMedia.originalMediaPath();
						else
							photoSrc = env.currentMedia.mediaPath(nextReductionSize);
						$(mediaSelector).attr("width", width).attr("height", height).attr("src", photoSrc);
					}
				}

				if (finalZoom <= env.initialZoom)
					// resolving will possibly show the title and the bottom thumbnails
					window.setTimeout(resolve_pinchInOut, duration * 1.2);

				if (finalZoom > startZoom) {
					// preload next size photo
					let nextReduction = util.nextReduction();
					if (nextReduction !== false && ! env.options.save_data) {
						$.preloadImages(nextReduction);
					}
					$(mediaSelector).css("cursor", "all-scroll");
				} else {
					$(mediaSelector).css("cursor", "");
				}

				env.currentZoom = finalZoom;

				util.setFinalOptions(env.currentMedia, false);
			}
		);
	};

	PinchSwipe.pinchIn = function(event, finalZoom, duration = pinchSpeed, center = null) {
		var windowRatio;

		// pause the slideshow
		if (env.slideshowId && ! $("#fullscreen-wrapper").hasClass("paused")) {
			env.slideshowWasPausedBeforePinchingIn = true;
			$("#started-slideshow-pause")[0].click();
		}

		if (
			env.currentZoom === env.initialZoom &&
			! $("#album-and-media-container.show-media #thumbs").hasClass("hidden-by-pinch") && (
				$("#center .title").is(":visible") || $("#album-and-media-container.show-media #thumbs").is(":visible")
			)
		) {
			// hide the title and the bottom thumbnails

			pastMediaWidthOnScreen = $(mediaSelector)[0].width;
			pastMediaHeightOnScreen = $(mediaSelector)[0].height;
			pastMediaRatioOnScreen = pastMediaWidthOnScreen / pastMediaHeightOnScreen;
			windowRatio = env.windowWidth / env.windowHeight;

			$("#center .title").addClass("hidden-by-pinch");
			$("#album-and-media-container.show-media #thumbs").addClass("hidden-by-pinch");

			if (event === null)
				event = {};
			event.data = {};
			event.data.resize = true;
			event.data.pinch = true;
			event.data.id = "center";

			let scalePromise = env.currentMedia.scale(event);
			scalePromise.then(
				function() {
					$("#media-center").off("load").on(
						"load",
						function() {
							$("#media-center").off("load");
							var newInitialZoom = PinchSwipe.screenZoom();
							if (newInitialZoom !== env.initialZoom) {
								// hiding the bottom thumbnails has resized the image
								env.initialZoom = newInitialZoom;

								util.setFinalOptions(env.currentMedia, false);

								mediaWidth = $(mediaSelector).css("width");
								mediaHeight = $(mediaSelector).css("height");
								mediaBoxInnerWidth = parseInt($(mediaContainerSelector).css("width"));
								mediaBoxInnerHeight = parseInt($(mediaContainerSelector).css("height"));

								env.currentZoom = env.initialZoom;
								if (typeof finalZoom === "undefined")
									finalZoom = null;
								if (finalZoom === null || finalZoom < env.currentZoom)
									finalZoom = env.currentZoom;
							} else if (finalZoom === null || typeof finalZoom === "undefined") {
								finalZoom = util.roundDecimals(env.currentZoom * zoomIncrement);
							}
							let pinchInOutPromise = PinchSwipe.pinchInOut(finalZoom, duration, center);
							pinchInOutPromise.then(
								function() {
									// do nothing
								}
							);
						}
					);
					// in case the image has been already loaded, trigger the event
					if ($("#media-center")[0].complete)
						$("#media-center").trigger("load");
				}
			);
		} else {
			if (finalZoom === undefined || finalZoom === null)
				finalZoom = util.roundDecimals(env.currentZoom * zoomIncrement);
			let pinchInOutPromise = PinchSwipe.pinchInOut(finalZoom, duration, center);
			pinchInOutPromise.then(
				function() {
					// do nothing
				}
			);
		}
	};

	PinchSwipe.pinchOut = function(event, finalZoom, duration, center = null) {
		// var mediaWidthOnScreen, mediaHeightOnScreen;
		var mediaRatioOnScreen, windowRatio;
		var finalZoomWasZero = false;
		if (typeof finalZoom === "undefined")
			finalZoom = null;
		if (finalZoom === null)
			finalZoom = util.roundDecimals(env.currentZoom * zoomDecrement);
		if (! finalZoom)
			finalZoomWasZero = true;

		// restart the slideshow when coming up to the initial zoom
		if (env.slideshowId && finalZoom === env.initialZoom && env.slideshowWasPausedBeforePinchingIn) {
			var resumeIntervalId = setInterval(
				function() {
					clearInterval(resumeIntervalId);
					$("#started-slideshow-play")[0].click();
					env.slideshowWasPausedBeforePinchingIn = false;
				},
				env.slideshowInterval * 1000 / 2
			);
		}

		if (env.currentZoom > env.initialZoom) {
			if (finalZoom < env.initialZoom)
				finalZoom = env.initialZoom;

			let pinchInOutPromise = PinchSwipe.pinchInOut(finalZoom, duration, center);
			pinchInOutPromise.then(
				function() {
					if (! env.fullScreenStatus) {
						// check whether the final pinchout (re-establishing title and the bottom thumbnails) has to be performed
						// mediaWidthOnScreen = $(mediaSelector)[0].width;
						// mediaHeightOnScreen = $(mediaSelector)[0].height;
						mediaRatioOnScreen = pastMediaWidthOnScreen / pastMediaHeightOnScreen;
						windowRatio = env.windowWidth / env.windowHeight;
						mediaBoxInnerWidth = parseInt($(mediaContainerSelector).css("width"));
						mediaBoxInnerHeight = parseInt($(mediaContainerSelector).css("height"));

						if (
							finalZoomWasZero ||
							mediaRatioOnScreen > windowRatio &&
							Math.round($(mediaSelector).outerWidth()) === env.windowWidth || (
								$("#center .title").hasClass("hidden") ||
								$("#center .title").hasClass("hidden-by-option") ||
								$("#center .title").hasClass("hidden-by-fullscreen")
							) && (
								$("#album-and-media-container.show-media #thumbs").hasClass("hidden") ||
								$("#album-and-media-container.show-media #thumbs").hasClass("hidden-by-option") ||
								$("#album-and-media-container.show-media #thumbs").hasClass("hidden-by-fullscreen")
							)
						)
							showTitleAndBottomThumbnails();
					}
				}
			);
		} else if (! env.fullScreenStatus) {
			showTitleAndBottomThumbnails();
		}
		// end of function body

		function showTitleAndBottomThumbnails() {
			$("#center .title").removeClass("hidden-by-pinch");
			if ($("#album-and-media-container.show-media #thumbs").hasClass("hidden-by-pinch")) {
				$("#album-and-media-container.show-media #thumbs").removeClass("hidden-by-pinch");

				var object = PinchSwipe.getTransformParameters($(mediaSelector));
				var parameter = PinchSwipe.prepareTransformParameter(
					object.rotate,
					0,
					0,
					object.scale * (
						object.rotate === 0 ?
						1 :
						PinchSwipe.scaleFactorForRotation(env.currentMedia, $(mediaSelector))
					)
				);
				$(mediaSelector).css(env.transformParameterName, parameter);

				var event = {data: {}};
				event.data.resize = true;
				event.data.pinch = true;
				event.data.id = "center";
				pastMediaWidthOnScreen = $(mediaSelector)[0].width;
				let scalePromise = env.currentMedia.scale(event);
				scalePromise.then(
					function() {
						util.setFinalOptions(env.currentMedia, false);

						mediaWidth = $(mediaSelector).css("width");
						mediaHeight = $(mediaSelector).css("height");
						env.initialZoom = PinchSwipe.screenZoom();
						env.currentZoom = env.initialZoom;
					}
				);
			}
		}
	};

	PinchSwipe.getTransformParameters = function(mediaElement) {
		// from https://gist.github.com/mbostock/1340727
		function transformDot(a, b) {
			return a[0] * b[0] + a[1] * b[1];
		}

		function normalize(a) {
			var k = Math.sqrt(transformDot(a, a));
			a[0] /= k;
			a[1] /= k;
			return k;
		}

		if (mediaElement === undefined)
			mediaElement = $("#media-center");
		var matrix = mediaElement.css("transform");

		var result = {
			rotate: 0,
			translateX: 0,
			translateY: 0,
			scale: 1
		};

		if (matrix !== undefined && matrix !== "none") {
			matrix = matrix.slice("matrix(".length, -1).split(", ").map(x => Number(x));
			// matrix = matrix.slice("matrix(".length, -1).split(", ").map(x => Math.abs(x) < Number.EPSILON * 1.5 ? 0 : Number(x));
			result = {
				rotate: util.roundDecimals(Math.atan2(matrix[1], matrix[0]) * 180 / Math.PI),
				translateX: util.roundDecimals(matrix[4]),
				translateY: util.roundDecimals(matrix[5]),
				scale: util.roundDecimals(normalize([matrix[0], matrix[1]]))
			};
		}

		//
		// var match = mediaElement.parent().html().match("transform\:[^;]+;");
		// if (match === null)
		// 	return result;
		//
		// var array = match[0].slice(0, -1).split(":").splice(1)[0].trim().split(")").slice(0, -1).map(element => element.trim() + ")");
		// var keys = array.map(element => element.substring(0, element.indexOf("(")).trim());
		// var values = array.map(element => element.substring(element.indexOf("(") + 1, element.indexOf(")")));
		// keys.forEach(
		// 	function(key, i)	 {
		// 		zoomDecimalPlaces = env.zoomDecimalPlaces;
		// 		if (key === "rotate")
		// 			zoomDecimalPlaces = 0;
		// 		if (key === "translate") {
		// 			[valueX, valueY] = values[i].split(",").map(value => parseFloat(value));
		// 			result["translateX"] = util.roundDecimals(valueX);
		// 			result["translateY"] = util.roundDecimals(valueY);
		// 		} else {
		// 			let float = parseFloat(values[i]);
		// 			result[key] = util.roundDecimals(float, zoomDecimalPlaces);
		// 		}
		// 	}
		// );
		return result;
	};

	PinchSwipe.drag = function(distance, dragVector, duration = dragSpeed) {
		$(mediaSelector).css("transition-duration", duration + "ms");

		var object = PinchSwipe.getTransformParameters($(mediaSelector));
		var cssTransformRotate = object.rotate;
		var cssTransformTranslateX = object.translateX;
		var cssTransformTranslateY = object.translateY;
		var cssTransformScale = object.scale;

		cssTransformTranslateX = util.roundDecimals(cssTransformTranslateX + distance * dragVector.x);
		cssTransformTranslateY = util.roundDecimals(cssTransformTranslateY + distance * dragVector.y);
		if (maxAllowedTranslateX !== undefined) {
			cssTransformTranslateX = Math.max(Math.min(cssTransformTranslateX, maxAllowedTranslateX), - maxAllowedTranslateX);
			cssTransformTranslateY = Math.max(Math.min(cssTransformTranslateY, maxAllowedTranslateY), - maxAllowedTranslateY);
		}

		var cssTranslateXString = cssTransformTranslateX.toString();
		var cssTranslateYString = cssTransformTranslateY.toString();

		var parameter = PinchSwipe.prepareTransformParameter(cssTransformRotate, cssTranslateXString, cssTranslateYString, cssTransformScale);
		$(mediaSelector).css("transform", parameter);
	};

	PinchSwipe.scaleFactorForRotation = function(singleMedia, mediaElement) {
		let mediaWidth, mediaHeight;
		// if (singleMedia.isVideo()) {
		// 	mediaWidth = mediaElement.attr("width");
		// 	mediaHeight = mediaElement.attr("height");
		// } else {
		mediaWidth = mediaElement.outerWidth();
		mediaHeight = mediaElement.outerHeight();
		// }

		return Math.min(env.windowWidth / mediaHeight, env.windowHeight / mediaWidth);
	};

	PinchSwipe.prepareTransformParameter = function(cssTransformRotate, cssTranslateXString, cssTranslateYString, cssTransformScale) {
		var parameters = [];
		var rotateParameter = "";
		var translateParameter = "";
		var someTranslateX = Number(cssTranslateXString) !== 0;
		var someTranslateY = Number(cssTranslateYString) !== 0;
		if (someTranslateX || someTranslateY) {
			translateParameter = "translate(";
			// if (someTranslateX)
			translateParameter += cssTranslateXString + "px";
			translateParameter += ",";
			// if (someTranslateY)
			translateParameter += cssTranslateYString + "px";
			translateParameter += ")";
			parameters.push(translateParameter);
		}
		if (cssTransformRotate) {
			rotateParameter = "rotate(" + cssTransformRotate.toString() + "deg)";
			parameters.push(rotateParameter);
		}
		var scaleParameter = "";
		if (cssTransformScale) {
			scaleParameter = "scale(" + cssTransformScale.toString() + ")";
			parameters.push(scaleParameter);
		}

		return parameters.join(" ");
	};


	// define the actions to be taken on tap
	PinchSwipe.prototype.addAlbumGesturesDetection = function() {
		function tapOnDescription(event, target) {
			if ($("#description-wrapper").css("opacity") === "1")
				util.addDescriptionOpacity();
			else
				util.removeDescriptionOpacity();
		}

		var tapOptions = {
			tap: tapOnDescription,
			fingers: 1
		};
		$(".description-wrapper").off().swipe(tapOptions);
	};

	// define the actions to be taken on pinch, swipe, tap, double tap
	PinchSwipe.prototype.addMediaGesturesDetection = function() {
		// swipe and drag gestures are detected on the media
		// pinch gesture is detected on its container, .media-box-inner
		// they must be separated because it seems that detecting drag and pinch on the same selector
		// has troubles: start event is reported on pinch or on drag, but not on both

		/**
		 * Catch each phase of the swipe.
		 * move : we drag the div
		 * cancel : we animate back to where we were
		 * end : we animate to the next image
		 */
		function swipeStatus(event, phase, direction, distance, duration, fingerCount) {
			//If we are moving before swipe, and we are going L or R in X mode, or U or D in Y mode then drag.

			if (event.button === 2 && (event.shiftKey || event.ctrlKey || event.altKey)) {
				return;
			}

			var clientX, clientY;

			if (phase === "start") {
				isLongTap = false;
			}

			// if (true || event.buttons > 0) {
			// when dragging with the mouse, fingerCount is 0
			if (event.touches === undefined || fingerCount === 1 && ! event.shiftKey && ! event.ctrlKey && ! event.altKey) {
				if (env.currentZoom === env.initialZoom) {
					// zoom = 1: swipe
					if (phase === "move" && event.buttons > 0) {
						if (direction === "left") {
							PinchSwipe.scrollMedia(env.windowWidth + distance);
						} else if (direction === "right") {
							PinchSwipe.scrollMedia(env.windowWidth - distance);
						}
					} else if (phase === "cancel" && event.buttons > 0) {
						PinchSwipe.swipeMedia(env.windowWidth);
					} else if (phase === "end") {
						if (direction === "right") {
							env.prevMedia.swipeRight();
						} else if (direction === "left") {
							env.nextMedia.swipeLeft();
						} else if (direction === "down" || direction === "up") {
							PinchSwipe.swipeDown(util.upHash());
						}
					}
				} else if (! event.shiftKey && ! event.ctrlKey && ! event.altKey){
					// zoom > 1: drag

					if (event.clientX !== undefined) {
						clientX = event.clientX;
						clientY = event.clientY;
					} else if (event.touches !== undefined && event.touches.length > 0) {
						clientX = event.touches[0].clientX;
						clientY = event.touches[0].clientY;
					} else if (event.changedTouches !== undefined && event.changedTouches.length > 0) {
						clientX = event.changedTouches[0].clientX;
						clientY = event.changedTouches[0].clientY;
					}
					if (phase === "start"  && event.buttons > 0 || phase === "end" || phase === "cancel"  && event.buttons > 0 || distance === 0) {
						if (phase === "start") {
							previousClientX = clientX;
							previousClientY = clientY;
						}
						// distance = 0
						// baseTranslateX = currentTranslateX;
						// baseTranslateY = currentTranslateY;
					} else {
						var dragVectorX = clientX - previousClientX;
						var dragVectorY = clientY - previousClientY;
						previousClientX = clientX;
						previousClientY = clientY;
						// var dragVectorX = event.movementX;
						// var dragVectorY = event.movementY;
						var dragVectorLength = Math.sqrt(dragVectorX * dragVectorX + dragVectorY * dragVectorY);
						dragVector = {x: 0, y: 0};
						if (dragVectorLength) {
							// normalize the vector
							dragVector = {
								x: dragVectorX / dragVectorLength,
								y: dragVectorY / dragVectorLength
							};
						}

						// PinchSwipe.drag(dragVectorLength / devicePixelRatio, dragVector, 0);
						PinchSwipe.drag(dragVectorLength, dragVector, 0);
					}
				}
			}
			// }
		}

		function pinchStatus(event, phase, direction, distance , duration , fingerCount, pinchZoom, fingerData) {
			// the drag vector is calculated here for use in the swipeStatus function
			// lamentably, swipeStatus doesn't return info about the swipe vector

			pinchZoom = parseFloat(pinchZoom);
			if (
				event.button === 1 ||
				event.button === 2 && (event.shiftKey || event.ctrlKey || event.altKey)
			) {
				return;
			}

			if (phase === "start") {
				// distance = 0
				baseZoom = env.currentZoom;
				if (fingerCount < 2)
					previousFingerEnd = {x: fingerData[0].start.x, y: fingerData[0].start.y};
			} else if (phase === "move" && fingerCount >= 2) {
				// phase is "move"
				let center = {x: 0, y: 0};
				for (let i = 0; i < event.touches.length; i ++) {
					center.x += event.touches[i].clientX / event.touches.length;
					center.y += event.touches[i].clientY / event.touches.length;

					// if (env.currentMedia.isRotated($("#media-center"))) {
					// 	// take into account image rotation
					// 	// the center must be rotated 90 degrees around the center
					// 	let screenCenter = {x: env.windowWidth / 2, y: env.windowHeight / 2}
					// 	center = {x: - (center.y - screenCenter.y) - screenCenter.x, y: (center.x - screenCenter.x) + screenCenter.y};
					// 	center.x = Math.max(0, center.x);
					// 	center.x = Math.min(env.windowWidth, center.x);
					// 	center.y = Math.max(0, center.y);
					// 	center.y = Math.min(env.windowHeight, center.y);
					// }
				}

				let finalZoom = util.roundDecimals(baseZoom * pinchZoom);
				// env.currentZoom = baseZoom;
				if (pinchZoom >= 1) {
					PinchSwipe.pinchIn(event, finalZoom, 0, center);
				} else {
					PinchSwipe.pinchOut(event, finalZoom, 0, center);
				}
			}

			if (fingerCount < 2) {
				// calculate the dragVector for dragging
				dragVector = {x: 0, y: 0};
				if (previousFingerEnd !== undefined) {
					var dragVectorX = fingerData[0].end.x - previousFingerEnd.x;
					var dragVectorY = fingerData[0].end.y - previousFingerEnd.y;
					previousFingerEnd = {x: fingerData[0].end.x, y: fingerData[0].end.y};
					var dragVectorLength = Math.sqrt(dragVectorX * dragVectorX + dragVectorY * dragVectorY);
					if (dragVectorLength) {
						// normalize the vector
						dragVector = {
							x: dragVectorX / dragVectorLength,
							y: dragVectorY / dragVectorLength
						};
						if (env.currentMedia.isRotated($("#media-center")))
							// take into account image rotation
							dragVector = {x: dragVector.y, y: - dragVector.x};
					}
				}
			}
		}

		function hold(event, target) {
			isLongTap = true;
		}

		function tapOnMedia(event, target) {
			if (env.currentZoom === env.initialZoom) {
				if (event.button === 2) {
					// right click
					if (env.prevMedia !== null && ! event.shiftKey && ! event.ctrlKey && ! event.altKey) {
						env.prevMedia.swipeRight();
						return false;
					}
				} else if (! isLongTap) {
					if (! fromResetZoom) {
						if (
							env.nextMedia !== null &&
							! event.shiftKey && ! event.ctrlKey && ! event.altKey &&
							! (env.isAnyMobile && env.currentMedia.isVideo())
						) {
							env.nextMedia.swipeLeft();
							return false;
						}
					} else
						fromResetZoom = false;
				}
			}
			return true;
		}

		function longTap(event, target) {
			env.prevMedia.swipeRight();
		}

		function doubleTap(event, target) {
			if (env.currentZoom === env.initialZoom) {
				env.prevMedia.swipeRight();
			} else {
				// env.currentZoom > env.initialZoom
				// image scaled up, reduce it to base zoom

				var object = PinchSwipe.getTransformParameters($(mediaSelector));
				var parameter = PinchSwipe.prepareTransformParameter(object.rotate, 0, 0, 1);
				$(mediaSelector).css("transform", parameter);
				env.currentZoom = env.initialZoom;
				fromResetZoom = true;
			}
		}

		if (env.currentMedia.isImage()) {
			// var tapDistanceThreshold = 2;
			var isLongTap;
			var fromResetZoom = false;

			env.initialZoom = PinchSwipe.screenZoom();
			mediaWidth = $(mediaSelector).css("width");
			mediaHeight = $(mediaSelector).css("height");

			env.currentZoom = env.initialZoom;
			currentTranslateX = 0;
			currentTranslateY = 0;

			$(mediaSelector).css("transition-duration", "0ms");

			var pinchOptions = {
				triggerOnTouchEnd: true,
				allowPageScroll: "none",
				preventDefaultEvents: true,
				pinchStatus: pinchStatus,
				// allowPageScroll: "vertical",
				threshold: 10,
				fingers: 2
			};
			$(mediaSelector).swipe(pinchOptions);
		}

		PinchSwipe.initializePincheSwipe();

		var swipeOrDragOptions = {
			triggerOnTouchEnd: true,
			allowPageScroll: "none",
			swipeStatus: swipeStatus,
			tap: tapOnMedia,
			longTap: longTap,
			doubleTap: doubleTap,
			hold: hold,
			// allowPageScroll: "vertical",
			threshold: 75,
			fingers: 1
		};
		$(mediaContainerSelector).swipe(swipeOrDragOptions);
	};

	PinchSwipe.screenZoom = function() {
		var imageRatio = env.currentMedia.metadata.size[0] / env.currentMedia.metadata.size[1];
		var mediaBoxInnerRatio = parseInt($("#center .media-box-inner").css("width")) / parseInt($("#center .media-box-inner").css("height"));
		var resultZoom;
		var imageWidth = env.currentMedia.metadata.size[0];
		var imageHeight = env.currentMedia.metadata.size[1];
		if (imageRatio > mediaBoxInnerRatio)
			resultZoom = $(mediaSelector)[0].width / imageWidth;
		else
			resultZoom = $(mediaSelector)[0].height / imageHeight;

		var object = PinchSwipe.getTransformParameters($(mediaSelector));
		if (object.rotate === 90)
			resultZoom *= PinchSwipe.scaleFactorForRotation(env.currentMedia, $(mediaSelector));

		return util.roundDecimals(resultZoom);
	};

	PinchSwipe.getInitialZoom = function () {
		return env.initialZoom;
	};

	PinchSwipe.initializePincheSwipe = function () {
		// $('#album-view').swipe('destroy');

		if (env.currentMedia.isImage()) {
			mediaBoxInnerWidth = parseInt($(mediaContainerSelector).css("width"));
			mediaBoxInnerHeight = parseInt($(mediaContainerSelector).css("height"));
			photoWidth = env.currentMedia.metadata.size[0];
			photoHeight = env.currentMedia.metadata.size[1];

			var newInitialZoom = PinchSwipe.screenZoom();
			if (newInitialZoom === env.initialZoom)
				env.currentZoom = newInitialZoom;
			env.initialZoom = newInitialZoom;
		}

		util.setFinalOptions(env.currentMedia, false);
	};

	PinchSwipe.prototype.swipeOnWheel = function(event, delta) {
		if (env.currentMedia === null)
			return true;
		if (! event.shiftKey && ! event.altKey && ! event.ctrlKey) {
			if (env.currentMedia.isVideo() || env.currentMedia.isImage() && env.currentZoom === env.initialZoom) {
				// mouse wheel with no key: swipe
				if (delta < 0) {
					env.nextMedia.swipeLeft();
					return false;
				} else if (delta > 0) {
					env.prevMedia.swipeRight();
					return false;
				}
			} else {
				// drag
				if (event.deltaY < 0) {
					PinchSwipe.drag(photoHeight / 5, {x: 0, y: -1});
					return false;
				} else if (event.deltaY > 0) {
					PinchSwipe.drag(photoHeight / 5, {x: 0, y: 1});
					return false;
				} else if (event.deltaX < 0) {
					PinchSwipe.drag(photoWidth / 5, {x: 1, y: 0});
					return false;
				} else if (event.deltaX > 0) {
					PinchSwipe.drag(photoWidth / 5, {x: -1, y: 0});
					return false;
				}
			}
		} else if (env.currentMedia.isImage()) {
			let center = {x: event.offsetX, y: event.offsetY};
			// mouse wheel with shift/control/alt key: pinch
			if (delta < 0) {
				PinchSwipe.pinchOut(event, env.currentZoom * 0.95, 0, center);
				return false;
			} else if (delta > 0) {
				PinchSwipe.pinchIn(event, env.currentZoom * 1.05, 0, center);
				return false;
			}
		}
		return true;
	};

	PinchSwipe.swipeUp = function(dest) {
		// Actually swiping up is passing from an album to a media, so there is no animation
		// ...or... the media could be let enter from below, as in horizontal swipe... TO DO
		// As is, it doesn't work
		if (dest) {
			$(mediaContainerSelector).stop().animate(
				{
					top: "-=" + window.innerHeight,
				},
				300,
				function() {
					window.location.href = dest;
					$(mediaContainerSelector).hide().css('top', "");
				}
			);
		}
	};

	PinchSwipe.swipeDown = function(dest) {
		if (dest && window.location.hash !== dest) {
			$(mediaContainerSelector).stop().animate(
				{
					top: "+=" + window.innerHeight,
					opacity: 0
				},
				300,
				function() {
					// if (window.location.hash !== dest)
					// 	$(window).hashchange();
					// else
					window.location.href = dest;
					// $("#media-view").addClass("hidden");
					$(mediaContainerSelector).css('top', "");
				}
			);
		}
	};

	/* make static methods callable as member functions */
	PinchSwipe.prototype.swipeDown = PinchSwipe.swipeDown;
	PinchSwipe.prototype.drag = PinchSwipe.drag;
	PinchSwipe.prototype.pinchIn = PinchSwipe.pinchIn;
	PinchSwipe.prototype.pinchOut = PinchSwipe.pinchOut;
	PinchSwipe.prototype.swipeUp = PinchSwipe.swipeUp;
	PinchSwipe.prototype.swipeMedia = PinchSwipe.swipeMedia;
	PinchSwipe.prototype.getTransformParameters = PinchSwipe.getTransformParameters;
	PinchSwipe.prototype.screenZoom = PinchSwipe.screenZoom;
	PinchSwipe.prototype.scaleFactorForRotation = PinchSwipe.scaleFactorForRotation;

	window.PinchSwipe = PinchSwipe;
}());
