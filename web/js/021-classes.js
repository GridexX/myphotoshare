(function() {

	var translations = new Translations();

	class Env {
		constructor() {
			this.translations = translations.getTranslations();
			this.shortcuts = translations.getShortcuts();
			this.currentAlbumIsAlbumWithOneMedia = false;
			this.previousAlbumIsAlbumWithOneMedia = false;
			this.guessedPasswordCodes = [];
			this.guessedPasswordsMd5 = [];
			this.searchWordsFromJsonFile = [];
			this.searchAlbumCacheBasesFromJsonFile = [];
			this.searchAlbumSubalbumsFromJsonFile = [];
			this.fullScreenStatus = false;
			this.currentAlbum = null;
			this.currentMedia = null;
			this.currentMediaIndex = -1;
			this.previousAlbum = null;
			this.albumOfPreviousState = null;
			this.albumInSubalbumDiv = null;
			this.previousMedia = null;
			this.nextMedia = null;
			this.prevMedia = null;
			this.isABrowsingModeChangeFromMouseClick = false;
			// beware: $(window).innerWidth(); doesn't cosider the scroll bar yet
			this.windowWidth = $(window).innerWidth();
			this.windowHeight = $(window).innerHeight();
			this.fromEscKey = false;
			this.firstEscKey = true;
			this.mapRefreshType = "none";
			this.selectorClickedToOpenTheMap = false;
			this.popupRefreshType = "previousAlbum";
			this.hashBeginning = "#!/";
			this.isFromAuthForm = false;
			this.highlightedObjectId = null;
			this.selectingSelectors = [];
			// var nextLink = "", prevLink = "";
			this.mediaLink = "";
			this.searchWords = [];
			this.loadedImages = [];
			this.isASaveDataChange = false;
			this.isRevertingFromHidingGeotaggedMedia = false;
			// scrollbarWidth;
			// contextMenu = false;

			this.albumCacheBase = "";
			this.mediaCacheBase = "";
			this.mediaFolderCacheBase = "";
			this.foundAlbumCacheBase = "";
			this.collectionCacheBase = "";
			this.minimumDistanceForCollisions = 5;

			// initialSizes = {};
			// initialSizes[0] = new ImagesAndVideos();
			this.positionMarker = "<marker>position</marker>";
			this.markTagBegin = "<mark data-markjs";
			this.br = "<br />";

			this.lastMapPositionAndZoom = {};
			this.keepShowingGeolocationSuggestText = MenuFunctions.getKeepShowingGeolocationSuggestTextState();

			this.options = {};
			this.cache = new Cache();
			var self = this;
			this.isMobile = {
				Android: function() {
					return navigator.userAgent.match(/Android/i);
				},
				BlackBerry: function() {
					return navigator.userAgent.match(/BlackBerry/i);
				},
				iOS: function() {
					return navigator.userAgent.match(/iPhone|iPad|iPod/i);
				},
				Opera: function() {
					return navigator.userAgent.match(/Opera Mini/i);
				},
				Windows: function() {
					return navigator.userAgent.match(/IEMobile/i);
				},
				any: function() {
					return (self.isMobile.Android() || self.isMobile.BlackBerry() || self.isMobile.iOS() || self.isMobile.Opera() || self.isMobile.Windows());
				}
			};

			this.slideshowId = 0;

			this.isFullScreenToggling = false;

			this.zoomDecimalPlaces = 2;

			this.slideshowIntervalDefault = 5;
			var slideshowIntervalCookie = MenuFunctions.getNumberCookie("slideshowInterval");
			// interval for slideshow in seconds
			this.slideshowInterval = this.slideshowIntervalDefault;
			if (slideshowIntervalCookie !== null)
				this.slideshowInterval = slideshowIntervalCookie;

			this.slideshowAutoRotation = false;
			this.slideshowWasPausedBeforePinchingIn = false;
			this.slideshowCurrentMediaWasNullBefore = false;

			// determine what is the name of the transform parameter in the browser that is used
			let imgObject = $("<img>");
			imgObject.css("transform", "rotate(90deg)");
			let transforms = ["transform", "-webkit-transform", "-moz-transform", "-o-transform", "-ms-transform"];
			for (let i = 0; i < transforms.length; i ++) {
				if (imgObject.css(transforms[i]) !== undefined) {
					this.transformParameterName = transforms[i];
					break;
				}
			}

			this.isAnyMobile = false;
			// the property devicePixelRatio permits to take into account the real mobile device pixels when deciding the size of reduced size image which is going to be loaded
			if (this.isMobile.any()) {
				this.isAnyMobile = true;

				this.devicePixelRatio =  window.devicePixelRatio || 1;

				// on mobile default is auto rotation
				this.slideshowAutoRotation = true;
				$("#started-slideshow-orientation-auto").addClass("active");
			} else {
				this.devicePixelRatio = 1;
			}

			this.maxSize = 0;
			this.language = "en";
			this.userLanguages = "en-EN";
			this.numberFormat = false;

			this.titleWrapper = "";
			this.maxWidthForPopupContent = 0;
			this.maxWidthForImagesInPopup = 0;
			this.maxHeightForPopupContent = 0;
			this.mymap = null;
			this.popup = null;

			if ($(".media-box#center").length) {
				var originalMediaBoxContainerHtml = $(".media-box#center")[0].outerHTML;
				if (originalMediaBoxContainerHtml.indexOf('<div class="title">') === -1) {
					var titleContent = $("#album-view").clone().children().first();
					this.originalMediaBoxContainerContent = $(originalMediaBoxContainerHtml).prepend(titleContent)[0].outerHTML;
				} else {
					this.originalMediaBoxContainerContent = originalMediaBoxContainerHtml;
				}
			}
		}
	}


	class SingleMediaInPositions {
		constructor(object) {
			Object.keys(object).forEach(
				(key) => {
					this[key] = object[key];
				}
			);
		}
	}

	class MediaInPositions extends Array {
		constructor(mediaInPositions) {
			if (Array.isArray(mediaInPositions))
				super(... mediaInPositions.map(singleMediaInPositions => new SingleMediaInPositions(singleMediaInPositions)));
			else
				super(mediaInPositions);
		}
	}

	class ImagesAndVideos {
		constructor(object) {
			if (typeof object === "undefined") {
				this.images = 0;
				this.videos = 0;
			} else {
				this.images = object.images;
				this.videos = object.videos;
			}
		}
	}

	class IncludedFiles {
		constructor(object) {
			if (typeof object === "undefined") {
				// do nothing, the resulting object will be the void object
			} else  {
				Object.keys(object).forEach(
					(key) => {
						this[key] = object[key];
					}
				);
			}
		}
	}

	class NumsProtected {
		constructor(object) {
			if (typeof object === "undefined") {
				this[","] = new ImagesAndVideos();
			} else {
				Object.keys(object).forEach(
					(key) => {
						this[key] = new ImagesAndVideos(object[key]);
					}
				);
			}
		}
	}

	class Sizes {
		constructor(object) {
			var self = this;
			if (typeof object === "undefined") {
				env.options.cache_images_formats.forEach(
					format => {
						self[format] = {};
						self[format][0] = new ImagesAndVideos();
						for (let iSize = 0; iSize < env.options.reduced_sizes.length; iSize ++) {
							self[format][env.options.reduced_sizes[iSize]] = new ImagesAndVideos();
						}
					}
				);
			} else {
				Object.keys(object).forEach(
					format => {
						self[format] = {};
						Object.keys(object[format]).forEach(
							size => {
								self[format][size] = new ImagesAndVideos(object[format][size]);
							}
						);
					}
				);
			}
		}
	}

	class PositionAndMedia {
		constructor(object) {
			Object.keys(object).forEach(
				(key) => {
					this[key] = object[key];
				}
			);
			this.mediaList = new MediaInPositions(this.mediaList);
		}
	}

	class PositionsAndMedia extends Array {
		constructor(positionsAndMedia) {
			if (Array.isArray(positionsAndMedia))
				super(... positionsAndMedia.map(positionAndMedia => new PositionAndMedia(positionAndMedia)));
			else
				super(positionsAndMedia);
		}
	}

	class SingleMedia {
		constructor(object) {
			Object.keys(object).forEach(
				(key) => {
					this[key] = object[key];
				}
			);
			if (this.hasOwnProperty("fileSizes"))
				this.fileSizes = new Sizes(this.fileSizes);
		}
	}

	class Media extends Array {
		constructor(media) {
			if (Array.isArray(media)) {
				super(... media.map(singleMedia => new SingleMedia(singleMedia)));
				this.getAndPutIntoCache();
			} else {
				super(media);
			}
		}
	}

	class Subalbum {
		constructor(object, isCloning = false) {
			var subalbumProperties = [];
			if (isCloning) {
				subalbumProperties = [
					"cacheBase",
					"name",
					"numsMedia",
					"numsMediaInSubTree",
					"numsProtectedMediaInSubTree",
					"path",
					"randomMedia",
					"captionsForSelection",
					"captionForSelectionSorting",
					"captionsForSearch",
					"captionForSearchSorting",
					"sizesOfAlbum",
					"sizesOfSubTree",
					"unicodeWords",
					"words",
					"tags"
				];
				if (env.options.expose_image_dates) {
					subalbumProperties.push("date");
				}
				if (env.options.expose_image_positions) {
					subalbumProperties.push("nonGeotagged");
				}
				subalbumProperties.push("numPositionsInTree");
			}

			Object.keys(object).forEach(
				key => {
					if (isCloning && subalbumProperties.indexOf(key) === -1) {
						delete object[key];
					} else if (key !== "nonGeotagged")
						this[key] = object[key];
				}
			);

			if (env.options.expose_image_positions) {
				this.nonGeotagged = {};
				this.nonGeotagged.numsMedia = new ImagesAndVideos(object.nonGeotagged.numsMedia);
				this.nonGeotagged.numsMediaInSubTree = new ImagesAndVideos(object.nonGeotagged.numsMediaInSubTree);
				this.nonGeotagged.sizesOfSubTree = new Sizes(object.nonGeotagged.sizesOfSubTree);
				this.nonGeotagged.sizesOfAlbum = new Sizes(object.nonGeotagged.sizesOfAlbum);
			}
			this.numsMedia = new ImagesAndVideos(this.numsMedia);
			this.numsMediaInSubTree = new ImagesAndVideos(this.numsMediaInSubTree);
			this.numsProtectedMediaInSubTree = new NumsProtected(this.numsProtectedMediaInSubTree);
			this.sizesOfAlbum = new Sizes(this.sizesOfAlbum);
			this.sizesOfSubTree = new Sizes(this.sizesOfSubTree);
			if (this.hasOwnProperty("randomMedia")) {
				if (! Array.isArray(this.randomMedia))
					this.randomMedia = [new SingleMedia(this.randomMedia)];
			}
		}
	}

	class Subalbums extends Array {
		constructor(subalbums, isCloning = false) {
			if (Array.isArray(subalbums))
				super(
					... subalbums.map(
						albumOrSubalbum => {
							if (albumOrSubalbum instanceof Album)
								return (albumOrSubalbum.toSubalbum());
							else
								return new Subalbum(albumOrSubalbum, isCloning);
						}
					)
				);
			else
				super(subalbums);
		}
	}

	class Album {
		// album types:
		// - folder albums: the original albums, as they are on disk
		// - by date albums: the albums where the media are organized by year/month/day; they are generated by the python scanner
		// - by gps albums: the albums where the media are organized by country/state-region/province; they are generated by the python scanner
		// - by search albums: the result of a js search, they may have both media (searched by name) and albums (searched by their name as folder album)
		// - by map albums: the result of a set of click on a map: they are first presented in a map popup, and from there they can be showed as the other albums; they only have media
		// - by selection albums: the result of the manual or group (through menu commands) selection of media and albums

		// album groups:
		// - generated albums: all types except folder albums
		// - transversal albums: by date and by gps
		// - virtual albums: by map and by selection: they are generated through user direct choices, and for this reason they cannot be represented by a cache base
		// - collection albums: by search, by map and by selection albums

		constructor(objectOrCacheBase, isCloning = false) {
			if (typeof objectOrCacheBase === "string") {
				let cacheBase = objectOrCacheBase;
				this.cacheBase = cacheBase;
				this.media = new Media([]);
				this.numsMedia = new ImagesAndVideos();
				this.numsMediaInSubTree = new ImagesAndVideos();
				this.sizesOfSubTree = new Sizes();
				this.sizesOfAlbum = new Sizes();
				this.subalbums = new Subalbums([]);
				this.positionsAndMediaInTree = new PositionsAndMedia([]);
				this.numPositionsInTree = 0;
				if (env.options.expose_image_positions) {
					this.nonGeotagged = {};
					this.nonGeotagged.numsMedia = new ImagesAndVideos();
					this.nonGeotagged.numsMediaInSubTree = new ImagesAndVideos();
					this.nonGeotagged.sizesOfAlbum = new Sizes();
					this.nonGeotagged.sizesOfSubTree = new Sizes();
				}
				// this.numsProtectedMediaInSubTree = new NumsProtected();
				if (cacheBase.split(env.options.cache_folder_separator).length === 1)
					this.ancestorsCacheBase = [cacheBase];
				// this.path = cacheBase.replace(env.options.cache_folder_separator, "/");
				this.physicalPath = this.path;
				this.empty = false;
				if (Utilities.isMapCacheBase(cacheBase)) {
					this.clickHistory = [];
				}
			} else if (typeof objectOrCacheBase === "object") {
				Object.keys(objectOrCacheBase).forEach(
					key => {
						if (key !== "nonGeotagged")
							this[key] = objectOrCacheBase[key];
					}
				);

				if (this.hasOwnProperty("media")) {
					if (isCloning) {
						let self = this;
						this.media.forEach(
							function(singleMedia, index) {
								self.media[index] = singleMedia.cloneAndDeleteParent();
							}
						);
					}
					this.media = new Media(this.media);
					this.numsMedia = this.media.imagesAndVideosCount();
				}
				if (env.options.expose_image_positions) {
					this.nonGeotagged = {};
					this.nonGeotagged.numsMedia = new ImagesAndVideos(objectOrCacheBase.nonGeotagged.numsMedia);
					this.nonGeotagged.numsMediaInSubTree = new ImagesAndVideos(objectOrCacheBase.nonGeotagged.numsMediaInSubTree);
					this.nonGeotagged.sizesOfSubTree = new Sizes(objectOrCacheBase.nonGeotagged.sizesOfSubTree);
					this.nonGeotagged.sizesOfAlbum = new Sizes(objectOrCacheBase.nonGeotagged.sizesOfAlbum);
				}
				if (this.hasOwnProperty("numsMedia"))
					this.numsMedia = new ImagesAndVideos(this.numsMedia);
				this.numsMediaInSubTree = new ImagesAndVideos(this.numsMediaInSubTree);
				if (this.hasOwnProperty("numsProtectedMediaInSubTree"))
					this.numsProtectedMediaInSubTree = new NumsProtected(this.numsProtectedMediaInSubTree);
				if (this.hasOwnProperty("positionsAndMediaInTree"))
					this.positionsAndMediaInTree = new PositionsAndMedia(this.positionsAndMediaInTree);
				this.sizesOfAlbum = new Sizes(this.sizesOfAlbum);
				this.sizesOfSubTree = new Sizes(this.sizesOfSubTree);
				if (isCloning) {
					let self = this;
					this.subalbums.forEach(
						function(albumOrSubalbum, index) {
							if (albumOrSubalbum instanceof Album)
								self.subalbums[index] = albumOrSubalbum.toSubalbum();
						}
					);
				}
				this.subalbums = new Subalbums(this.subalbums, isCloning);

				this.removeUnnecessaryPropertiesAndAddParentToMedia();
			} else if (objectOrCacheBase === undefined) {
				this.empty = true;
			}

			// if (objectOrCacheBase !== undefined) {
			// 	if (! this.hasOwnProperty("includedFilesByCodesSimpleCombination")) {
			// 		this.includedFilesByCodesSimpleCombination = new IncludedFiles({",": false});
			// 	}
			// }
			if (! isCloning && objectOrCacheBase !== undefined && this.codesComplexCombination === undefined) {
				env.cache.putAlbum(this);
			}
		}
	}

	class Cache {
		constructor() {
			this.js_cache_levels = [
				{mediaThreshold: 10000, max: 1},
				{mediaThreshold: 2000, max: 2},
				{mediaThreshold: 500, max: 10},
				{mediaThreshold: 200, max: 50}
			];
			this.albums = {};
			this.albums.index = {};

			this.media = {};

			this.inexistentFiles = [];
		}
	}

	window.Env = Env;
	window.Album = Album;
	window.Subalbum = Subalbum;
	window.Subalbums = Subalbums;
	window.SingleMedia = SingleMedia;
	window.Media = Media;
	window.ImagesAndVideos = ImagesAndVideos;
	window.IncludedFiles = IncludedFiles;
	window.NumsProtected = NumsProtected;
	window.Sizes = Sizes;
	window.PositionAndMedia = PositionAndMedia;
	window.PositionsAndMedia = PositionsAndMedia;
	window.SingleMediaInPositions = SingleMediaInPositions;
	window.Cache = Cache;
}());
