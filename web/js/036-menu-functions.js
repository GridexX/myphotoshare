(function() {

	var phFl = new PhotoFloat();
	var util = new Utilities();
	var pS = new PinchSwipe();

	/* constructor */
	function MenuFunctions() {
	}

	MenuFunctions.hideDescriptionMenuEntry = function() {
		var isPopup = util.isPopup();
		var isMap = util.isMap();

		var popupHasSomeDescription, albumHasSomeDescription, subalbumsHaveSomeDescription, mediaHaveSomeDescription;
		if (isPopup)
			popupHasSomeDescription =
				env.mapAlbum.media.some(singleMedia => singleMedia.hasSomeDescription("title")) ||
				env.mapAlbum.media.some(singleMedia => singleMedia.hasSomeDescription("description"));
		if (env.currentAlbum !== null) {
			albumHasSomeDescription =
				env.currentAlbum.hasSomeDescription("title") ||
				env.currentAlbum.hasSomeDescription("description");
			subalbumsHaveSomeDescription =
				env.currentAlbum.subalbums.length > 0 && (
					env.currentAlbum.subalbums.some(subalbum => subalbum.hasSomeDescription("title")) ||
					env.currentAlbum.subalbums.some(subalbum => subalbum.hasSomeDescription("description"))
				);
			mediaHaveSomeDescription =
				env.currentAlbum.media.length > 0 && (
					env.currentAlbum.media.some(singleMedia => singleMedia.hasSomeDescription("title")) ||
					env.currentAlbum.media.some(singleMedia => singleMedia.hasSomeDescription("description"))
				);
		}
		var singleMediaHasSomeDescription;
		if (env.currentMedia !== null)
			singleMediaHasSomeDescription =
				env.currentMedia.hasSomeDescription("title") ||
				env.currentMedia.hasSomeDescription("description");

		return (
			isMap ||
			isPopup && ! popupHasSomeDescription ||
			env.currentMedia === null && ! albumHasSomeDescription && ! subalbumsHaveSomeDescription && ! mediaHaveSomeDescription ||
			env.currentMedia !== null && ! (singleMediaHasSomeDescription || ! env.currentMedia.hasSomeDescription("tags") && albumHasSomeDescription)
		);
	};

	MenuFunctions.hideTagsMenuEntry = function() {
		var isPopup = util.isPopup();
		var isMap = util.isMap();

		var popupHasSomeTag, albumHasSomeTag, subalbumsHaveSomeTag, mediaHaveSomeTag;
		if (isPopup)
			popupHasSomeTag =
				env.mapAlbum.media.some(singleMedia => singleMedia.hasSomeDescription("tags"));
		if (env.currentAlbum !== null) {
			albumHasSomeTag =
				env.currentAlbum.hasSomeDescription("tags");
			subalbumsHaveSomeTag =
				env.currentAlbum.subalbums.length > 0 && (
					env.currentAlbum.subalbums.some(subalbum => subalbum.hasSomeDescription("tags"))
				);
			mediaHaveSomeTag =
				env.currentAlbum.media.length > 0 && (
					env.currentAlbum.media.some(singleMedia => singleMedia.hasSomeDescription("tags"))
				);
		}
		var singleMediaHasSomeTag;
		if (env.currentMedia !== null)
			singleMediaHasSomeTag = env.currentMedia.hasSomeDescription("tags");

		return (
			isMap ||
			isPopup && ! popupHasSomeTag ||
			env.currentMedia === null && ! albumHasSomeTag && ! subalbumsHaveSomeTag && ! mediaHaveSomeTag ||
			env.currentMedia !== null && ! singleMediaHasSomeTag && ! albumHasSomeTag
		);
	};

	MenuFunctions.updateMenu = function(thisAlbum) {
		var albumOrMedia;
		var isPopup = util.isPopup();
		var isMap = ($('#mapdiv').html() ? true : false) && ! isPopup;
		var isMapOrPopup = isMap || isPopup;
		var geotaggedContentIsHidden = util.geotaggedContentIsHidden();

		if (thisAlbum === undefined)
			thisAlbum = env.currentAlbum;

		var thisAlbumIsAlbumWithOneMedia = thisAlbum.isAlbumWithOneMedia();
		var isTransversalAlbum = thisAlbum.isTransversal();
		var isSingleMedia = (env.currentMedia !== null || thisAlbumIsAlbumWithOneMedia);
		var isAnyRoot = thisAlbum.isAnyRoot();

		var subalbumsLength = thisAlbum.subalbums.filter(subalbum => ! geotaggedContentIsHidden || ! subalbum.hasGpsData()).length;
		var mediaLength = thisAlbum.media.filter(singleMedia => ! geotaggedContentIsHidden || ! singleMedia.hasGpsData()).length;

		var numsMedia = geotaggedContentIsHidden ? thisAlbum.nonGeotagged.numsMedia : thisAlbum.numsMedia;

		var nothingIsSelected = util.nothingIsSelected();
		var everySubalbumIsSelected = false;
		if (! isPopup)
			everySubalbumIsSelected = thisAlbum.everySubalbumIsSelected();
		var noSubalbumIsSelected = thisAlbum.noSubalbumIsSelected();
		var everyMediaIsSelected;
		if (isPopup)
			everyMediaIsSelected = env.mapAlbum.everyMediaIsSelected();
		else
			everyMediaIsSelected = thisAlbum.everyMediaIsSelected();
		var noMediaIsSelected;
		if (isPopup)
			noMediaIsSelected = env.mapAlbum.noMediaIsSelected();
		else
			noMediaIsSelected = thisAlbum.noMediaIsSelected();
		var highMediaNumberInTransversalAlbum = isTransversalAlbum && ! env.options.show_big_virtual_folders && numsMedia.imagesAndVideosTotal() > env.options.big_virtual_folders_threshold;

		var hasGpsData, thisMedia;

		if (isSingleMedia) {
			if (env.currentMedia !== null)
				thisMedia = env.currentMedia;
			else
				thisMedia = thisAlbum.media.filter(singleMedia => ! geotaggedContentIsHidden || ! singleMedia.hasGpsData())[0];
			hasGpsData = thisMedia.hasGpsData();
		} else if (isAnyRoot) {
			hasGpsData = (thisAlbum.hasOwnProperty("numPositionsInTree") && thisAlbum.numPositionsInTree > 0);
		} else {
			hasGpsData = false;
		}

		// add the correct classes to the menu buttons

		////////////////// BROWSING MODES //////////////////////////////

		$(".browsing-mode-switcher").off("click");

		if (
			isMapOrPopup ||
			thisAlbum === null ||
			! isSingleMedia && ! isAnyRoot
		) {
			$(".browsing-mode-switcher").addClass("hidden");
		} else {
			$(".browsing-mode-switcher").removeClass("hidden").removeClass("selected");
			$(".first-level.browsing-mode-switcher li").addClass("active");

			if (! env.options.expose_image_dates) {
				$("#by-date-view").addClass("hidden");
			}
			if (! hasGpsData) {
				$("#by-gps-view").addClass("hidden");
			}

			if (
				nothingIsSelected || ! (
					isSingleMedia && thisMedia.isSelected() ||
					isAnyRoot
				)
			) {
				$("#by-selection-view").addClass("hidden");
			}

			if (
				! util.somethingIsInMapAlbum() || ! (
					isSingleMedia && thisMedia.isInMapAlbum() ||
					isAnyRoot
				)
			) {
				$("#by-map-view").addClass("hidden");
			}

			if (
				! (
					isAnyRoot && util.somethingIsSearched() ||
					isSingleMedia && (
						// util.somethingIsSearched() ||
						// env.collectionCacheBase && util.isSearchCacheBase(env.collectionCacheBase)
						thisAlbum.isSearch() ||
						thisMedia.isSearched() ||
						env.collectionCacheBase && util.isSearchCacheBase(env.collectionCacheBase) ||
						thisMedia.isInFoundAlbum() !== false
					)
				)
			) {
				$("#by-search-view").addClass("hidden");
			}

			if (thisAlbum.isFolder() && ! (env.collectionCacheBase && util.isSearchCacheBase(env.collectionCacheBase))) {
				// folder album: change to by date or by gps view
				$("#folders-view").addClass("selected").removeClass("active");
			} else if (thisAlbum.isByDate()) {
				$("#by-date-view").addClass("selected").removeClass("active");
			} else if (thisAlbum.isByGps()) {
				$("#by-gps-view").addClass("selected").removeClass("active");
			} else if (thisAlbum.isMap()) {
				$("#by-map-view").removeClass("hidden").addClass("selected").removeClass("active");
			} else if (
				thisAlbum.isSearch() ||
				env.collectionCacheBase && util.isSearchCacheBase(env.collectionCacheBase)
			) {
				$("#by-search-view").removeClass("hidden").addClass("selected").removeClass("active");
			} else if (thisAlbum.isSelection()) {
				$("#by-selection-view").removeClass("hidden").addClass("selected").removeClass("active");
			}
		}

		// bind the click events

		$("#folders-view").off("click").on(
			"click",
			function changeToFoldersView(ev) {
				ev.stopPropagation();
				MenuFunctions.addHighlightToItem($(this));
				TopFunctions.showBrowsingModeMessage(ev, "#folders-browsing");

				if (isSingleMedia) {
					$(".title").removeClass("hidden-by-pinch");
					$("#album-and-media-container.show-media #thumbs").removeClass("hidden-by-pinch");
					window.location.href =
						env.hashBeginning + util.pathJoin([thisMedia.foldersCacheBase, thisMedia.cacheBase]);
				} else if (isAnyRoot) {
					window.location.href = env.hashBeginning + encodeURIComponent(env.options.folders_string);
				}

				return false;
			}
		);

		$("#by-date-view").off("click").on(
			"click",
			function changeToByDateView(ev) {
				ev.stopPropagation();
				MenuFunctions.addHighlightToItem($(this));
				TopFunctions.showBrowsingModeMessage(ev, "#by-date-browsing");

				if (isSingleMedia) {
					$(".title").removeClass("hidden-by-pinch");
					$("#album-and-media-container.show-media #thumbs").removeClass("hidden-by-pinch");
					window.location.href =
						env.hashBeginning + util.pathJoin([thisMedia.dayAlbumCacheBase, thisMedia.foldersCacheBase, thisMedia.cacheBase]);
				} else if (isAnyRoot) {
					window.location.href = env.hashBeginning + encodeURIComponent(env.options.by_date_string);
				}

				return false;
			}
		);

		$("#by-gps-view").off("click").on(
			"click",
			function changeToByGpsView(ev) {
				ev.stopPropagation();
				MenuFunctions.addHighlightToItem($(this));
				TopFunctions.showBrowsingModeMessage(ev, "#by-gps-browsing");

				if (isSingleMedia) {
					$(".title").removeClass("hidden-by-pinch");
					$("#album-and-media-container.show-media #thumbs").removeClass("hidden-by-pinch");
					window.location.href =
						env.hashBeginning + util.pathJoin([thisMedia.gpsAlbumCacheBase, thisMedia.foldersCacheBase, thisMedia.cacheBase]);
				} else if (isAnyRoot) {
					window.location.href = env.hashBeginning + encodeURIComponent(env.options.by_gps_string);
				}

				return false;
			}
		);

		$("#by-map-view").off("click").on(
			"click",
			function changeToByMapView(ev) {
				ev.stopPropagation();
				MenuFunctions.addHighlightToItem($(this));
				TopFunctions.showBrowsingModeMessage(ev, "#by-map-browsing");
				if (isSingleMedia) {
					$(".title").removeClass("hidden-by-pinch");
					$("#album-and-media-container.show-media #thumbs").removeClass("hidden-by-pinch");
					window.location.href = phFl.encodeHash(env.mapAlbum.cacheBase, thisMedia);
				} else if (isAnyRoot) {
					window.location.href = phFl.encodeHash(env.mapAlbum.cacheBase, null);
				}

				return false;
			}
		);

		$("#by-search-view").off("click").on(
			"click",
			function changeToBySearchView(ev) {
				ev.stopPropagation();
				MenuFunctions.addHighlightToItem($(this));
				TopFunctions.showBrowsingModeMessage(ev, "#by-search-browsing");
				if (isSingleMedia) {
					$(".title").removeClass("hidden-by-pinch");
					$("#album-and-media-container.show-media #thumbs").removeClass("hidden-by-pinch");
					// if (thisMedia.hasOwnProperty("searchHashes") && thisMedia.searchHashes.length)
					var foundAlbum = thisMedia.isInFoundAlbum();
					if (foundAlbum !== false) {
						window.location.href = phFl.encodeHash(thisMedia.foldersCacheBase, thisMedia, foundAlbum.cacheBase, env.searchAlbum.cacheBase);
					} else {
						window.location.href = phFl.encodeHash(env.searchAlbum.cacheBase, thisMedia);
					}
				} else if (isAnyRoot) {
					window.location.href = phFl.encodeHash(env.searchAlbum.cacheBase, null);
				}

				return false;
			}
		);

		// WARNING: the ":not(.hidden)" is missing intentionally, in order to permit to trigger a click even if the menu item isn't shown
		$("#by-selection-view").off("click").on(
			"click",
			function(ev) {
				ev.stopPropagation();
				MenuFunctions.addHighlightToItem($(this));
					util.changeToBySelectionView(ev, thisMedia);

				return false;
			}
		);

		////////////////// SEARCH //////////////////////////////

		if (isMap)
			$("#search-menu").addClass("hidden-by-map");
		else
			$("#search-menu").removeClass("hidden-by-map");

		if (
			thisAlbum !== null
		) {
			if (isPopup)
				$("#search-field, #search-button").attr("title", util._t("#refine-popup-content"));
			else
				$("#search-field, #search-button").attr("title", util._t("#real-search"));

			if (env.options.search_inside_words)
				$("li#inside-words").addClass("selected");
			else
				$("li#inside-words").removeClass("selected");
			if (env.options.search_any_word)
				$("li#any-word").addClass("selected");
			else
				$("li#any-word").removeClass("selected");
			if (env.options.search_case_sensitive)
				$("li#case-sensitive").addClass("selected");
			else
				$("li#case-sensitive").removeClass("selected");
			if (env.options.search_accent_sensitive)
				$("li#accent-sensitive").addClass("selected");
			else
				$("li#accent-sensitive").removeClass("selected");
			if (env.options.search_tags_only)
				$("li#tags-only").addClass("selected");
			else
				$("li#tags-only").removeClass("selected");
			// if (env.options.cache_base_to_search_in === env.options.folders_string || isPopup) {
				// $("li#album-search").removeClass("selected").removeClass("active").off("click").attr("title", "");
			// } else {
				// $("li#album-search").addClass("active").off("click").on("click", util.toggleCurrentAbumSearch);
			if (env.options.cache_base_to_search_in === env.options.folders_string) {
				// TO DO: actually code could enter here for any root album, i.e. for by date, ecc. too
				$("#album-search").attr('title', util._t("#current-album-is") + '""');
			} else {
				let albumNamePromise = util.getAlbumNameFromCacheBase(env.options.cache_base_to_search_in);
				albumNamePromise.then(
					function(path) {
						$("#album-search").attr('title', util._t("#current-album-is") + '"' + path + '"');
					}
				);
			}

			if (env.options.search_current_album)
				$("li#album-search").addClass("selected");
			else
				$("li#album-search").removeClass("selected");
			// }
		}

		////////////////// UI //////////////////////////////

		if (isMap) {
			$("#right-and-search-menu li.ui").addClass("hidden");
		} else {
			$("#right-and-search-menu li.ui").removeClass("hidden");

			if (isMapOrPopup) {
				$("#right-and-search-menu li.show-title").addClass("hidden");
			} else {
				$("#right-and-search-menu li.show-title").removeClass("hidden");
				if (
					env.options.hide_title ||
					$("#album-and-media-container").hasClass("show-media") &&
					$("#album-and-media-container #media-view #center .title").css("display") === "none"
				)
					$("#right-and-search-menu li.show-title").removeClass("selected");
				else
					$("#right-and-search-menu li.show-title").addClass("selected");
			}

			if (MenuFunctions.hideDescriptionMenuEntry(thisAlbum)) {
				$("#right-and-search-menu li.show-descriptions").addClass("hidden");
			} else {
				$("#right-and-search-menu li.show-descriptions").removeClass("hidden");
				if (env.options.hide_descriptions)
					$("#right-and-search-menu li.show-descriptions").removeClass("selected");
				else
					$("#right-and-search-menu li.show-descriptions").addClass("selected");
			}

			if (MenuFunctions.hideTagsMenuEntry()) {
				$("#right-and-search-menu li.show-tags").addClass("hidden");
			} else {
				$("#right-and-search-menu li.show-tags").removeClass("hidden");
				if (env.options.hide_tags)
					$("#right-and-search-menu li.show-tags").removeClass("selected");
				else
					$("#right-and-search-menu li.show-tags").addClass("selected");
			}

			if (
				isMapOrPopup ||
				env.currentMedia !== null ||
				thisAlbumIsAlbumWithOneMedia ||
				thisAlbum !== null && subalbumsLength === 0 && env.options.hide_title
			) {
				$("#right-and-search-menu li.show-media-counts").addClass("hidden");
			} else {
				$("#right-and-search-menu li.show-media-counts").removeClass("hidden");
				if (env.options.show_album_media_count)
					$("#right-and-search-menu li.show-media-counts").addClass("selected");
				else
					$("#right-and-search-menu li.show-media-counts").removeClass("selected");
			}


			if (isMap || isPopup && env.mapAlbum.media.length <= 1 || ! $("#thumbs").is(":visible"))
				$("#right-and-search-menu li.spaced").addClass("hidden");
			else
				$("#right-and-search-menu li.spaced").removeClass("hidden");
			if (env.options.spacing)
				$("#right-and-search-menu li.spaced").addClass("selected");
			else
				$("#right-and-search-menu li.spaced").removeClass("selected");

			if (
				isMapOrPopup ||
				env.currentMedia !== null ||
				thisAlbumIsAlbumWithOneMedia ||
				thisAlbum !== null && subalbumsLength === 0
			) {
				$("#right-and-search-menu li.square-album-thumbnails").addClass("hidden");
			} else {
				$("#right-and-search-menu li.square-album-thumbnails").removeClass("hidden");
				if (env.options.album_thumb_type.indexOf("square") > -1)
					$("#right-and-search-menu li.square-album-thumbnails").addClass("selected");
				else
					$("#right-and-search-menu li.square-album-thumbnails").removeClass("selected");
			}

			if (
				isMapOrPopup ||
				env.currentMedia !== null ||
				thisAlbumIsAlbumWithOneMedia ||
				thisAlbum !== null && subalbumsLength === 0
			) {
				$("#right-and-search-menu li.slide").addClass("hidden");
			} else {
				$("#right-and-search-menu li.slide").removeClass("hidden");
				if (env.options.albums_slide_style)
					$("#right-and-search-menu li.slide").addClass("selected");
				else
					$("#right-and-search-menu li.slide").removeClass("selected");
			}

			if (
				isMapOrPopup ||
				env.currentMedia !== null ||
				thisAlbumIsAlbumWithOneMedia ||
				thisAlbum !== null && (subalbumsLength === 0 || ! thisAlbum.isFolder())
			) {
				$("#right-and-search-menu li.show-album-names").addClass("hidden");
			} else {
				$("#right-and-search-menu li.show-album-names").removeClass("hidden");
				if (env.options.show_album_names_below_thumbs)
					$("#right-and-search-menu li.show-album-names").addClass("selected");
				else
					$("#right-and-search-menu li.show-album-names").removeClass("selected");
			}

			if (isMap || ! $("#thumbs").is(":visible"))
				$("#right-and-search-menu li.square-media-thumbnails").addClass("hidden");
			else
				$("#right-and-search-menu li.square-media-thumbnails").removeClass("hidden");
			if (env.options.media_thumb_type.indexOf("square") > -1)
			 	$("#right-and-search-menu li.square-media-thumbnails").addClass("selected");
			else
				$("#right-and-search-menu li.square-media-thumbnails").removeClass("selected");

			if (
				isMap ||
				! isMapOrPopup && (
					env.currentMedia !== null ||
					thisAlbumIsAlbumWithOneMedia ||
					thisAlbum !== null && (
						numsMedia.imagesAndVideosTotal() === 0 ||
						highMediaNumberInTransversalAlbum
					)
				)
			) {
				$("#right-and-search-menu li.show-media-names").addClass("hidden");
			} else {
				$("#right-and-search-menu li.show-media-names").removeClass("hidden");
				if (env.options.show_media_names_below_thumbs)
					$("#right-and-search-menu li.show-media-names").addClass("selected");
				else
					$("#right-and-search-menu li.show-media-names").removeClass("selected");
			}

			if (
				isMapOrPopup ||
				env.currentMedia === null || thisAlbumIsAlbumWithOneMedia  || ! $("#thumbs").is(":visible")
			) {
				$("#right-and-search-menu li.show-bottom-thumbnails").addClass("hidden");
			} else {
				$("#right-and-search-menu li.show-bottom-thumbnails").removeClass("hidden");

				if (env.options.hide_bottom_thumbnails)
					$("#right-and-search-menu li.show-bottom-thumbnails").removeClass("selected");
				else
					$("#right-and-search-menu li.show-bottom-thumbnails").addClass("selected");
			}
		}

		////////////////// BIG ALBUMS //////////////////////////////

		if (
			isMapOrPopup ||
			thisAlbum === null ||
			// in the following line the non geotagged mode must not be taken into account
			numsMedia.imagesAndVideosTotal() < env.options.big_virtual_folders_threshold ||
			! isTransversalAlbum
		) {
			$("#right-and-search-menu .big-albums").addClass("hidden");
		} else {
			$("#right-and-search-menu .big-albums").removeClass("hidden");
			if (env.options.show_big_virtual_folders)
			 	$("#right-and-search-menu .big-albums").addClass("selected");
			else
				$("#right-and-search-menu .big-albums").removeClass("selected");
		}

		////////////////// SORT //////////////////////////////

		if (
			isMap ||
			! isPopup && (env.currentMedia !== null || numsMedia.imagesAndVideosTotal() <= 1 && subalbumsLength <= 1) ||
			isPopup && env.mapAlbum.media.length <= 1
		) {
			// showing a media or a map or a popup on the map, nothing to sort
			$("#right-and-search-menu li.sort").addClass("hidden");
		} else if (thisAlbum !== null) {
			if (numsMedia.imagesAndVideosTotal() <= 1) {
				// no media or one media
				$("#right-and-search-menu li.media-sort").addClass("hidden");
			} else {
				$("#right-and-search-menu li.media-sort").removeClass("hidden");
			}

			if (subalbumsLength <= 1 || isMapOrPopup) {
				// no subalbums or one subalbum
				$("#right-and-search-menu li.album-sort").addClass("hidden");
			} else {
				$("#right-and-search-menu li.album-sort").removeClass("hidden");
			}

			if (
				numsMedia.imagesAndVideosTotal() <= 1 && (! isPopup || env.mapAlbum.media.length <= 1) ||
				highMediaNumberInTransversalAlbum
			) {
				// no media or one media or too many media
				$("#right-and-search-menu li.media-sort").addClass("hidden");
			} else {
				$("#right-and-search-menu li.media-sort").removeClass("hidden");
			}

			var modes = ["album", "media"];
			for (var i in modes) {
				if (modes.hasOwnProperty(i)) {
					albumOrMedia = modes[i];
					if (thisAlbum[albumOrMedia + "NameSort"]) {
						$("#right-and-search-menu li." + albumOrMedia + "-sort.by-name").removeClass("active").addClass("selected");
						$("#right-and-search-menu li." + albumOrMedia + "-sort.by-date").addClass("active").removeClass("selected");
					} else {
						$("#right-and-search-menu li." + albumOrMedia + "-sort.by-date").removeClass("active").addClass("selected");
						$("#right-and-search-menu li." + albumOrMedia + "-sort.by-name").addClass("active").removeClass("selected");
					}

					if (
						thisAlbum[albumOrMedia + "ReverseSort"]
					) {
						$("#right-and-search-menu li." + albumOrMedia + "-sort.reverse").addClass("selected");
					} else {
						$("#right-and-search-menu li." + albumOrMedia + "-sort.reverse").removeClass("selected");
					}
				}
			}
		}

		////////////////// SELECTION //////////////////////////////

		if (isMap || thisAlbum.isSearch() && ! mediaLength && ! subalbumsLength) {
			$(".select").addClass("hidden");
		} else {
			$(".select").removeClass("hidden").removeClass("selected");
			if (thisAlbum.isSelection()) {
				$(".select.global-reset, .select.go-to-selected").addClass("hidden");
			} else {
				let goToSelectedText = util._t(".select.go-to-selected");
				goToSelectedText += " (";
				if (env.selectionAlbum.subalbums.length)
					goToSelectedText += env.selectionAlbum.subalbums.length + " " + util._t(".title-albums");
				if (env.selectionAlbum.subalbums.length && env.selectionAlbum.media.length)
					goToSelectedText += ", ";
				if (env.selectionAlbum.media.length)
					goToSelectedText += env.selectionAlbum.media.length + " " + util._t(".title-media");
				goToSelectedText += ")";
				$(".select.go-to-selected").html(goToSelectedText);
			}
			if (nothingIsSelected) {
				$(".select.global-reset, .select.go-to-selected").addClass("hidden");
				$(".select.nothing").addClass("hidden");
				$(".select.no-albums").addClass("hidden");
				$(".select.no-media").addClass("hidden");
			}
			if (! subalbumsLength || noSubalbumIsSelected || everySubalbumIsSelected || noMediaIsSelected) {
				$(".select.no-albums").addClass("hidden");
			}
			if (! numsMedia.imagesAndVideosTotal() || noMediaIsSelected || everyMediaIsSelected || noSubalbumIsSelected) {
				$(".select.no-media").addClass("hidden");
			}

			if (! isPopup) {
				if (! numsMedia.imagesAndVideosTotal() || ! subalbumsLength)
					$(".select.media, .select.albums, .select.no-media, .select.no-albums").addClass("hidden");
				if (! numsMedia.imagesAndVideosTotal() && ! subalbumsLength)
					$(".select.everything, .select.everything-individual").addClass("hidden");
			} else if (isPopup) {
				$(".select.albums, .select.no-albums").addClass("hidden");
				$(".select.media, .select.no-media").addClass("hidden");
			}

			if (everySubalbumIsSelected) {
				$(".select.albums").addClass("selected");
			}

			if (everyMediaIsSelected) {
				$(".select.media").addClass("selected");
			}

			if ((isPopup || everySubalbumIsSelected) && everyMediaIsSelected) {
				$(".select.everything").addClass("selected");
			}

			if (highMediaNumberInTransversalAlbum) {
				$(".select.everything, .select.media").addClass("hidden");
			}

			if (! subalbumsLength || isPopup) {
				$(".select.everything-individual").addClass("hidden");
			} else {
				let everythingIndividualPromise = thisAlbum.recursivelyAllMediaAreSelected();
				everythingIndividualPromise.then(
					function isTrue() {
						$(".select.everything-individual").addClass("selected");
					},
					function isFalse() {
						// do nothing
					}
				);
			}


			$(".select.everything:not(.hidden):not(.shortcut-help)").off("click").on(
				"click",
				function() {
					MenuFunctions.addHighlightToItem($(this));
					var albumToUse;
					if (isPopup)
						albumToUse = env.mapAlbum;
					else
						albumToUse = thisAlbum;
					$("#working").addClass("select-everything");
					$("#working").show();
					if (albumToUse.everySubalbumIsSelected() && albumToUse.everyMediaIsSelected()) {
						albumToUse.removeAllMediaFromSelection();
						let promise = albumToUse.removeAllSubalbumsFromSelection();
						promise.then(
							function() {
								$("#working").removeClass("select-everything");
								if (! util.numClassesInWorking())
									$("#working").hide();
								if (util.nothingIsSelected())
									util.initializeSelectionAlbum();
								MenuFunctions.updateMenu();
							}
						);
					} else {
						albumToUse.addAllMediaToSelection();
						let promise = albumToUse.addAllSubalbumsToSelection();
						promise.then(
							function() {
								$("#working").removeClass("select-everything");
								if (! util.numClassesInWorking())
									$("#working").hide();
								MenuFunctions.updateMenu();
							}
						);
					}
					return false;
				}
			);

			$(".select.everything-individual:not(.hidden)").off("click").on(
				"click",
				function() {
					MenuFunctions.addHighlightToItem($(this));
					$("#working").addClass("select-everything-individual");
					$("#working").show();
					let everythingIndividualPromise = thisAlbum.recursivelyAllMediaAreSelected();
					everythingIndividualPromise.then(
						function isTrue() {
							let firstPromise = thisAlbum.recursivelyRemoveMedia();
							firstPromise.then(
								function() {
									$("#working").removeClass("select-everything-individual");
									if (! util.numClassesInWorking())
										$("#working").hide();
									if (util.nothingIsSelected())
										util.initializeSelectionAlbum();
									MenuFunctions.updateMenu();
									$("#removed-individually").stop().fadeIn(
										1000,
										function() {
											$("#removed-individually").stop().fadeOut(3000);
										}
									);
								}
							);
						},
						function isFalse() {
							let firstPromise = thisAlbum.recursivelySelectMedia();
							firstPromise.then(
								function() {
									$("#working").removeClass("select-everything-individual");
									if (! util.numClassesInWorking())
										$("#working").hide();
									$("#added-individually").stop().fadeIn(
										1000,
										function() {
											if (thisAlbum.isSelection())
												env.currentAlbum.showMedia();
											MenuFunctions.updateMenu();
											$("#added-individually").stop().fadeOut(3000);
										}
									);
								}
							);
						}
					);
					return false;
				}
			);

			$(".select.media:not(.hidden)").off("click").on(
				"click",
				function() {
					MenuFunctions.addHighlightToItem($(this));
					var albumToUse;
					if (isPopup)
						albumToUse = env.mapAlbum;
					else
						albumToUse = thisAlbum;
					if (albumToUse.everyMediaIsSelected()) {
						albumToUse.removeAllMediaFromSelection();
						if (util.nothingIsSelected())
							util.initializeSelectionAlbum();
					} else {
						albumToUse.addAllMediaToSelection();
					}
					MenuFunctions.updateMenu();
					return false;
				}
			);

			$(".select.albums:not(.hidden)").off("click").on(
				"click",
				function() {
					MenuFunctions.addHighlightToItem($(this));
					$("#working").addClass("select-albums");
					$("#working").show();
					if (thisAlbum.everySubalbumIsSelected()) {
						let promise = thisAlbum.removeAllSubalbumsFromSelection();
						promise.then(
							function() {
								$("#working").removeClass("select-albums");
								if (! util.numClassesInWorking())
									$("#working").hide();
								if (util.nothingIsSelected())
									util.initializeSelectionAlbum();
								MenuFunctions.updateMenu();
							}
						);
					} else {
						var promise = thisAlbum.addAllSubalbumsToSelection();
						promise.then(
							function() {
								$("#working").removeClass("select-albums");
								if (! util.numClassesInWorking())
									$("#working").hide();
								MenuFunctions.updateMenu();
							}
						);
					}
					return false;
				}
			);

			$(".select.global-reset:not(.hidden)").off("click").on(
				"click",
				function() {
					MenuFunctions.addHighlightToItem($(this));
					$("#working").addClass("select-global-reset");
					$("#working").show();
					env.selectionAlbum.removeAllMediaFromSelection();
					let subalbumsPromise = env.selectionAlbum.removeAllSubalbumsFromSelection();
					subalbumsPromise.then(
						function allSubalbumsRemoved() {
							$("#working").removeClass("select-global-reset");
							if (! util.numClassesInWorking())
								$("#working").hide();
							if (util.nothingIsSelected())
								util.initializeSelectionAlbum();
							MenuFunctions.updateMenu();
						}
					);
					return false;
				}
			);

			$(".select.nothing:not(.hidden)").off("click").on(
				"click",
				function() {
					MenuFunctions.addHighlightToItem($(this));
					$("#working").addClass("select-nothing");
						$("#working").show();
					var albumToUse;
					if (isPopup)
						albumToUse = env.mapAlbum;
					else
						albumToUse = thisAlbum;
					albumToUse.removeAllMediaFromSelection();
					let subalbumsPromise = albumToUse.removeAllSubalbumsFromSelection();
					subalbumsPromise.then(
						function allSubalbumsRemoved() {
							$("#working").removeClass("select-nothing");
							if (! util.numClassesInWorking())
								$("#working").hide();
							if (util.nothingIsSelected())
								util.initializeSelectionAlbum();
							MenuFunctions.updateMenu();
						}
					);
					return false;
				}
			);

			$(".select.no-albums:not(.hidden)").off("click").on(
				"click",
				function() {
					MenuFunctions.addHighlightToItem($(this));
					$("#working").addClass("select-no-albums");
					$("#working").show();
					let subalbumsPromise = thisAlbum.removeAllSubalbumsFromSelection();
					subalbumsPromise.then(
						function allSubalbumsRemoved() {
							$("#working").removeClass("select-no-albums");
							if (! util.numClassesInWorking())
								$("#working").hide();
							if (util.nothingIsSelected())
								util.initializeSelectionAlbum();
							MenuFunctions.updateMenu();
						}
					);
					return false;
				}
			);

			$(".select.no-media:not(.hidden)").off("click").on(
				"click",
				function() {
					MenuFunctions.addHighlightToItem($(this));
					var albumToUse;
					if (isPopup)
						albumToUse = env.mapAlbum;
					else
						albumToUse = thisAlbum;

					albumToUse.removeAllMediaFromSelection();

					return false;
				}
			);

			$(".select.go-to-selected:not(.hidden)").off("click").on(
				"click",
				function(ev) {
					MenuFunctions.addHighlightToItem($(this));
					util.changeToBySelectionView(ev);
					return false;
				}
			);
		}


		////////////////// DOWNLOAD //////////////////////////////

		if (isMap) {
			$(".download-album").addClass("hidden");
		} else {
			$(".download-album").removeClass("hidden");
			const maximumZipSize = 2000000000;
			const bigZipSize = 500000000;

			$(".download-single-media").addClass("hidden").addClass("active");
			$(".download-album").addClass("hidden").removeClass("red");
			$("ul li.download-album:not(.selection)").addClass("active");
			$(".download-album.sized").addClass("hidden");

			let geotaggedContentIsHidden = util.geotaggedContentIsHidden();

			if (
				(
					geotaggedContentIsHidden && env.selectionAlbum.nonGeotagged.numsMediaInSubTree.imagesAndVideosTotal() ||
					! geotaggedContentIsHidden && env.selectionAlbum.numsMediaInSubTree.imagesAndVideosTotal()
				) &&
				! thisAlbum.isSelection()
			) {
				$(".download-album.selection").removeClass("hidden");
				$(".download-album.selection").attr("title", util._t(".how-to-download-selection").replace(/<br \/>/gm, ' '));
			}

			if (thisAlbum.isSearch() && ! mediaLength && ! subalbumsLength) {
				// download menu item remains hidden
			} else if (env.currentMedia !== null || thisAlbumIsAlbumWithOneMedia) {
				$(".download-album.expandable, .download-album.caption").removeClass("hidden");
				if (env.options.expose_original_media || env.options.expose_full_size_media_in_cache) {
					$(".download-single-media").removeClass("hidden");
					let originalMediaPath;
					if (thisAlbumIsAlbumWithOneMedia)
						originalMediaPath = encodeURI(thisAlbum.media.filter(singleMedia => ! geotaggedContentIsHidden || ! singleMedia.hasGpsData())[0].originalMediaPath());
					else
						originalMediaPath = encodeURI(env.currentMedia.originalMediaPath());
					$(".download-single-media .download-link").attr("href", originalMediaPath).attr("download", "");
				}
			} else if (thisAlbum !== null) {
				$(".download-album.expandable, .download-album.caption").removeClass("hidden");

				let showDownloadEverything = false;

				let albumForDownload;
				if (isPopup)
					albumForDownload = env.mapAlbum;
				else
					albumForDownload = thisAlbum;
				let albumForDownloadNumsMedia = albumForDownload.numsMedia;
				let albumForDownloadNumsMediaInSubTree = albumForDownload.numsMediaInSubTree;
				let albumForDownloadSizesOfAlbum = albumForDownload.sizesOfAlbum;
				let albumForDownloadSizesOfSubTree = albumForDownload.sizesOfSubTree;
				let albumForDownloadNumSubalbums = albumForDownload.subalbums.filter(subalbum => ! geotaggedContentIsHidden || ! subalbum.hasGpsData());

				if (geotaggedContentIsHidden) {
					albumForDownloadNumsMedia = albumForDownload.nonGeotagged.numsMedia;
					albumForDownloadNumsMediaInSubTree = albumForDownload.nonGeotagged.numsMediaInSubTree;
					albumForDownloadSizesOfAlbum = albumForDownload.nonGeotagged.sizesOfAlbum;
					albumForDownloadSizesOfSubTree = albumForDownload.nonGeotagged.sizesOfSubTree;
				}

				if (albumForDownloadNumSubalbums) {
					$(".download-album.everything.all.full").removeClass("hidden");
					// reset the html
					$(".download-album.everything.all").html(util._t(".download-album.everything.all"));

					let nMediaInSubTree = albumForDownloadNumsMediaInSubTree.imagesAndVideosTotal();
					let numImages = albumForDownloadNumsMediaInSubTree.images;
					let numVideos = albumForDownloadNumsMediaInSubTree.videos;
					let what = util._t(".title-media");
					if (numImages === 0)
						what = util._t(".title-videos");
					if (numVideos === 0)
						what = util._t(".title-images");

					if (albumForDownload.isSearch()) {
						// in search albums, numsMediaInSubTree doesn't include the media in the albums found, the values that goes into the DOM must be update by code here
						for (let iSubalbum = 0; iSubalbum < albumForDownload.subalbums.length; iSubalbum ++) {
							if (! geotaggedContentIsHidden)
								nMediaInSubTree += albumForDownload.subalbums[iSubalbum].numsMediaInSubTree.imagesAndVideosTotal();
							else if (albumForDownload.subalbums[iSubalbum].hasGpsData())
								nMediaInSubTree += albumForDownload.subalbums[iSubalbum].nonGeotagged.numsMediaInSubTree.imagesAndVideosTotal();
						}
					}

					let treeSize = albumForDownloadSizesOfSubTree[env.options.format][0].imagesAndVideosTotal();
					if (treeSize) {
						$(".download-album.everything.all.full").append(
							": " + env.numberFormat.format(nMediaInSubTree) + " " + what + ", " + util.humanFileSize(treeSize)
						);
						if (treeSize < bigZipSize) {
							// maximum allowable size is 500MB (see https://github.com/eligrey/FileSaver.js/#supported-browsers)
							// actually it can be less (Chrome on Android)
							// It may happen that the files are collected but nothing is saved
							$(".download-album.everything.all.full").attr("title", "");
						} else if (treeSize < maximumZipSize) {
							$(".download-album.everything.all.full").addClass("red").attr("title", util._t("#download-difficult"));
						} else {
							$(".download-album.everything.all.full").addClass("red").removeClass("active").attr("title", util._t("#cant-download"));
						}
					} else {
						$(".download-album.everything.all.full").addClass("hidden");
					}

					if (! treeSize || treeSize >= bigZipSize && env.options.reduced_sizes.length) {
						// propose to download the resized media
						for (let iSize = 0; iSize < env.options.reduced_sizes.length; iSize++) {
							let reducedSize = env.options.reduced_sizes[iSize];
							let treeSize = albumForDownloadSizesOfSubTree[env.options.format][reducedSize].imagesAndVideosTotal();
							if (treeSize < bigZipSize) {
								$(".download-album.everything.all.sized").append(
									", " + env.numberFormat.format(reducedSize) + " px: " +
									env.numberFormat.format(albumForDownloadNumsMediaInSubTree.imagesAndVideosTotal()) + " " + what + ", " +
									util.humanFileSize(treeSize)
								);
								$(".download-album.everything.all.sized").attr("size", reducedSize);
								$(".download-album.everything.all.sized").removeClass("hidden");
								break;
							}
						}
					}
					showDownloadEverything = true;

					let mediaInThisAlbum = albumForDownloadNumsMedia.imagesAndVideosTotal();
					let mediaInThisTree = albumForDownloadNumsMediaInSubTree.imagesAndVideosTotal();
					if (numImages && numImages !== mediaInThisAlbum && numImages !== mediaInThisTree && mediaInThisAlbum !== mediaInThisTree) {
						$(".download-album.everything.images.full").removeClass("hidden");
						// reset the html
						$(".download-album.everything.images").html(util._t(".download-album.everything.images"));

						// add the download size
						let imagesSize = albumForDownloadSizesOfSubTree[env.options.format][0].images;
						if (imagesSize) {
							$(".download-album.everything.images.full").append(
								": " + env.numberFormat.format(numImages) + " " + util._t(".title-images") + ", " +
								util.humanFileSize(imagesSize)
							);
							// check the size and decide if they can be downloaded
							if (imagesSize < bigZipSize) {
								// maximum allowable size is 500MB (see https://github.com/eligrey/FileSaver.js/#supported-browsers)
								// actually it can be less (Chrome on Android)
								// It may happen that the files are collected but nothing is saved
								$(".download-album.everything.images.full").attr("title", "");
							} else if (imagesSize < maximumZipSize) {
								$(".download-album.everything.images.full").addClass("red").attr("title", util._t("#download-difficult"));
							} else {
								$(".download-album.everything.images.full").addClass("red").removeClass("active").attr("title", util._t("#cant-download"));
							}
						} else {
							$(".download-album.everything.images.full").addClass("hidden");
						}

						if (! imagesSize || imagesSize >= bigZipSize && env.options.reduced_sizes.length) {
							// propose to download the resized media
							for (let iSize = 0; iSize < env.options.reduced_sizes.length; iSize++) {
								let reducedSize = env.options.reduced_sizes[iSize];
								let imagesSize = albumForDownloadSizesOfSubTree[env.options.format][reducedSize].images;
								if (imagesSize < bigZipSize) {
									$(".download-album.everything.images.sized").append(
										", " + env.numberFormat.format(reducedSize) + " px: " +
										env.numberFormat.format(numImages) + " " + util._t(".title-images") + ", " +
										util.humanFileSize(imagesSize)
									);
									$(".download-album.everything.images.sized").attr("size", reducedSize);
									$(".download-album.everything.images.sized").removeClass("hidden");
									break;
								}
							}
						}
					}

					if (numVideos && numVideos !== mediaInThisAlbum && numVideos !== mediaInThisTree && mediaInThisAlbum !== mediaInThisTree) {
						$(".download-album.everything.videos.full").removeClass("hidden");
						// reset the html
						$(".download-album.everything.videos").html(util._t(".download-album.everything.videos"));

						// add the download size
						let videosSize = albumForDownloadSizesOfSubTree[env.options.format][0].videos;
						if (videosSize) {
							$(".download-album.everything.videos.full").append(": " + numVideos + " " + util._t(".title-videos") + ", " + util.humanFileSize(videosSize));
							// check the size and decide if they can be downloaded
							if (videosSize < bigZipSize) {
								// maximum allowable size is 500MB (see https://github.com/eligrey/FileSaver.js/#supported-browsers)
								// actually it can be less (Chrome on Android)
								// It may happen that the files are collected but nothing is saved
								$(".download-album.everything.videos.full").attr("title", "");
							} else if (videosSize < maximumZipSize) {
								$(".download-album.everything.videos.full").addClass("red").attr("title", util._t("#download-difficult"));
							} else {
								$(".download-album.everything.videos.full").addClass("red").removeClass("active").attr("title", util._t("#cant-download"));
							}
						} else {
							$(".download-album.everything.videos.full").addClass("hidden");
						}

						if (! videosSize || videosSize >= bigZipSize && env.options.reduced_sizes.length) {
							// propose to download the resized video
							// in albumForDownloadSizesOfSubTree[iSize] all the reduced sizes have the same value, corresponding to the transcoded videos
							let reducedSize = env.options.reduced_sizes[0];
							let videosSize = albumForDownloadSizesOfSubTree[env.options.format][reducedSize].videos;
							if (videosSize < bigZipSize) {
								$(".download-album.everything.videos.sized").append(
									", " + util._t(".title-transcoded") + ": " +
									env.numberFormat.format(numVideos) + " " + util._t(".title-videos") + ", " +
									util.humanFileSize(videosSize)
								);
								// $(".download-album.everything.videos.sized").attr("size", reducedSize);
								$(".download-album.everything.videos.sized").removeClass("hidden");
							}
						}
					}
				}

				let numImages = albumForDownloadNumsMedia.images;
				let numVideos = albumForDownloadNumsMedia.videos;
				let what = util._t(".title-media");
				if (numImages === 0)
					what = util._t(".title-videos");
				if (numVideos === 0)
					what = util._t(".title-images");

				if (albumForDownloadNumsMedia.imagesAndVideosTotal()) {
					$(".download-album.media-only.all.full").removeClass("hidden");
					// reset the html
					if (showDownloadEverything)
						$(".download-album.media-only.all").html(util._t(".download-album.media-only.all"));
					else
						$(".download-album.media-only.all").html(util._t(".download-album.simple.all"));

					// add the download size
					let albumSize = albumForDownloadSizesOfAlbum[env.options.format][0].imagesAndVideosTotal();
					if (albumSize) {
						$(".download-album.media-only.all.full").append(
							": " + env.numberFormat.format(albumForDownloadNumsMedia.imagesAndVideosTotal()) + " " + what + ", " +
							util.humanFileSize(albumSize)
						);
						// check the size and decide if they can be downloaded
						if (albumSize < bigZipSize) {
							// maximum allowable size is 500MB (see https://github.com/eligrey/FileSaver.js/#supported-browsers)
							// actually it can be less (Chrome on Android)
							// It may happen that the files are collected but nothing is saved
							$(".download-album.media-only.all.full").attr("title", "");
						} else if (albumSize < maximumZipSize) {
							$(".download-album.media-only.all.full").addClass("red").attr("title", util._t("#download-difficult"));
						} else {
							$(".download-album.media-only.all.full").addClass("red").removeClass("active").attr("title", util._t("#cant-download"));
						}
					} else {
						$(".download-album.media-only.all.full").addClass("hidden");
					}

					if (! albumSize || albumSize >= bigZipSize && env.options.reduced_sizes.length) {
						// propose to download the resized media
						for (let iSize = 0; iSize < env.options.reduced_sizes.length; iSize++) {
							let reducedSize = env.options.reduced_sizes[iSize];
							let albumSize = albumForDownloadSizesOfAlbum[env.options.format][reducedSize].imagesAndVideosTotal();
							if (albumSize < bigZipSize) {
								$(".download-album.media-only.all.sized").append(
									", " + env.numberFormat.format(reducedSize) + " px: " +
									env.numberFormat.format(albumForDownloadNumsMedia.imagesAndVideosTotal()) + " " + what + ", " +
									util.humanFileSize(albumSize)
								);
								$(".download-album.media-only.all.sized").attr("size", reducedSize);
								$(".download-album.media-only.all.sized").removeClass("hidden");
								break;
							}
						}
					}
				}

				if (numImages && numImages !== albumForDownloadNumsMedia.imagesAndVideosTotal()) {
					$(".download-album.media-only.images.full").removeClass("hidden");
					// reset the html
					if (showDownloadEverything)
						$(".download-album.media-only.images").html(util._t(".download-album.media-only.images"));
					else
						$(".download-album.media-only.images").html(util._t(".download-album.simple.images"));

					// add the download size
					let imagesSize = albumForDownloadSizesOfAlbum[env.options.format][0].images;
					if (imagesSize) {
						$(".download-album.media-only.images.full").append(
							": " + env.numberFormat.format(numImages) + " " + util._t(".title-images") + ", " +
							util.humanFileSize(imagesSize)
						);
						// check the size and decide if they can be downloaded
						if (imagesSize < bigZipSize) {
							// maximum allowable size is 500MB (see https://github.com/eligrey/FileSaver.js/#supported-browsers)
							// actually it can be less (Chrome on Android)
							// It may happen that the files are collected but nothing is saved
							$(".download-album.media-only.images.full").attr("title", "");
						} else if (imagesSize < maximumZipSize) {
							$(".download-album.media-only.images.full").addClass("red").attr("title", util._t("#download-difficult"));
						} else {
							$(".download-album.media-only.images.full").addClass("red").removeClass("active").attr("title", util._t("#cant-download"));
						}
					} else {
						$(".download-album.media-only.images.full").addClass("hidden");
					}

					if (! imagesSize || imagesSize >= bigZipSize && env.options.reduced_sizes.length) {
						// propose to download the resized media
						for (let iSize = 0; iSize < env.options.reduced_sizes.length; iSize++) {
							let reducedSize = env.options.reduced_sizes[iSize];
							let imagesSize = albumForDownloadSizesOfAlbum[env.options.format][reducedSize].images;
							if (imagesSize < bigZipSize) {
								$(".download-album.media-only.images.sized").append(
									", " + env.numberFormat.format(reducedSize) + " px: " +
									env.numberFormat.format(numImages) + " " + util._t(".title-images") + ", " +
									util.humanFileSize(imagesSize)
								);
								$(".download-album.media-only.images.sized").attr("size", reducedSize);
								$(".download-album.media-only.images.sized").removeClass("hidden");
								break;
							}
						}
					}
				}

				if (numVideos && numVideos !== albumForDownloadNumsMedia.imagesAndVideosTotal()) {
					$(".download-album.media-only.videos.full").removeClass("hidden");
					// reset the html
					if (showDownloadEverything)
						$(".download-album.media-only.videos").html(util._t(".download-album.media-only.videos"));
					else
						$(".download-album.media-only.videos").html(util._t(".download-album.simple.videos"));

					// add the download size
					let videosSize = albumForDownloadSizesOfAlbum[env.options.format][0].videos;
					if (videosSize) {
						$(".download-album.media-only.videos.full").append(
							": " + env.numberFormat.format(numVideos) + " " + util._t(".title-videos") + ", " +
							util.humanFileSize(videosSize)
						);
						// check the size and decide if they can be downloaded
						if (videosSize < bigZipSize) {
							// maximum allowable size is 500MB (see https://github.com/eligrey/FileSaver.js/#supported-browsers)
							// actually it can be less (Chrome on Android)
							// It may happen that the files are collected but nothing is saved
							$(".download-album.media-only.videos.full").attr("title", "");
						} else if (videosSize < maximumZipSize) {
							$(".download-album.media-only.videos.full").addClass("red").attr("title", util._t("#download-difficult"));
						} else {
							$(".download-album.media-only.videos.full").addClass("red").removeClass("active").attr("title", util._t("#cant-download"));
						}
					} else {
						$(".download-album.media-only.videos.full").addClass("hidden");
					}

					if (! videosSize || videosSize >= bigZipSize && env.options.reduced_sizes.length) {
						// propose to download the resized video
						// in albumForDownloadSizesOfSubTree[iSize] all the reduced sizes have the same value, corresponding to the transcoded videos
						let reducedSize = env.options.reduced_sizes[0];
						let videosSize = albumForDownloadSizesOfSubTree[env.options.format][reducedSize].videos;
						if (videosSize < bigZipSize) {
							$(".download-album.media-only.videos.sized").append(
								", " + util._t(".title-transcoded") + ": " +
								env.numberFormat.format(numVideos) + " " + util._t(".title-videos") + ", " +
								util.humanFileSize(videosSize)
							);
							// $(".download-album.everything.videos.sized").attr("size", reducedSize);
							$(".download-album.media-only.videos.sized").removeClass("hidden");
						}
					}
				}
			}
		}

		////////////////// NON-GEOTAGGED ONLY MODE //////////////////////////////

		if (env.options.expose_image_positions) {
			let mediaCount = thisAlbum.numsMediaInSubTree.imagesAndVideosTotal();
			let nonGeotaggedMediaCount = thisAlbum.nonGeotagged.numsMediaInSubTree.imagesAndVideosTotal();
			if (
				isMap || isPopup ||
				! nonGeotaggedMediaCount || nonGeotaggedMediaCount === mediaCount
			) {
				$(".non-geotagged-only").addClass("hidden");
			} else {
				$(".non-geotagged-only").removeClass("hidden");
				if (geotaggedContentIsHidden)
					$("#hide-geotagged-media").addClass("selected");
				else
					$("#hide-geotagged-media").removeClass("selected");
			}

			$("#hide-geotagged-media").off("click").on(
				"click",
				function() {
					// css manages the showing/hiding of media/subalbums, the title change and the subalbums caption change
					// this function performs additional operation: highlighting the correct object and showing the correct image in subalbums

					var geotaggedContentWasHidden = util.geotaggedContentIsHidden();
					// toggle the class that hides/shows changes through css
					$("#fullscreen-wrapper").toggleClass("hide-geotagged");

					// activate lazy loader
					if (util.isPopup())
						$('#popup-images-wrapper').scroll();
					else if (env.currentMedia !== null)
						$("#album-view").scroll();
					else
						$(window).scroll();

					util.checkAlbumWithOneMedia();

					// highlight the menu item
					MenuFunctions.addHighlightToItem($(this));
					MenuFunctions.updateMenu();

					if (! geotaggedContentWasHidden)
						util.addClickToHiddenGeotaggedMediaPhrase();

					if (! geotaggedContentWasHidden && env.currentMedia !== null) {
						// single media view, we must hide the geotagged content
						if (env.currentMedia.hasGpsData()) {
							// I cannot keep showing the current media which is geotagged, show the album
							env.fromEscKey = true;
							$("#loading").show();
							pS.swipeDown(util.upHash());
						} else {
							// reload, so that if previous or next media is geotagged, new media will be loaded
							$(window).hashchange();
						}
					} else {
						let currentObject = util.highlightedObject();
						let currentObjectIsASubalbums = util.aSubalbumIsHighlighted();
						let newObject = currentObject;
						if (! geotaggedContentWasHidden) {
							// we have to correct subalbum or media highlighting, too
							if (
								currentObjectIsASubalbums && currentObject.parent().hasClass("all-gps") ||
								! currentObjectIsASubalbums && currentObject.parent().hasClass("gps")
							) {
								newObject = util.nextObjectForHighlighting(currentObject);
								if (
									currentObjectIsASubalbums && ! util.aSubalbumIsHighlighted() ||
									! currentObjectIsASubalbums && util.aSubalbumIsHighlighted()
								) {
									newObject = util.prevObjectForHighlighting(newObject);
								}
							}
						}

						util.addHighlightToMediaOrSubalbum(newObject);
						if (currentObjectIsASubalbums)
							util.scrollAlbumViewToHighlightedSubalbum(newObject);
						else
							util.scrollAlbumViewToHighlightedThumb(newObject);

						// adapt subalbums and media caption height
						util.adaptSubalbumCaptionHeight();
						util.adaptMediaCaptionHeight();

						if (! geotaggedContentWasHidden) {
							// since we have hidden the geotagged content, we have to check and possibly correct the subalbum image
							$("#subalbums > a:not(.all-gps) img.thumbnail").each(
								function() {
									var [randomMedia, randomMediaAlbumCacheBase] = util.getMediaFromImgObject($(this));
									if (! randomMedia.hasGpsData()) {
										return;
									} else  {
										let iSubalbum = env.currentAlbum.subalbums.findIndex(subalbum => randomMediaAlbumCacheBase.indexOf(subalbum.cacheBase) === 0);
										env.currentAlbum.pickRandomMediaAndInsertIt(iSubalbum, true);
									}
								}
							);
						} else {
							// we are showing the geotagged content, a reload is needed
							env.isRevertingFromHidingGeotaggedMedia = true;
							$(window).hashchange();
						}
					}
				}
			);
		} else {
			$("#hide-geotagged-media").hide();
		}

		////////////////// PROTECTED CONTENT //////////////////////////////

		let selector = ".protection";
		if (thisAlbum !== null) {
			if (thisAlbum.hasVeiledProtectedContent()) {
				$(selector).removeClass("hidden");
				$(selector).off("click").on(
					"click",
					function() {
						util.showAuthForm();
						MenuFunctions.closeMenu();
					}
				);
			} else {
				$(selector).addClass("hidden");
			}
		} else {
			$(selector).addClass("hidden");
		}

		////////////////// SAVE DATA MODE //////////////////////////////

		if (env.options.save_data)
			$("#save-data").addClass("selected");
		else
			$("#save-data").removeClass("selected");


		////////////////// RESTORE SETTINGS //////////////////////////////

		if (! util.settingsChanged())
			$("#restore").addClass("hidden");
		else
			$("#restore").removeClass("hidden");


		////////////////// be sure that the highlighted menu entry is visible //////////////////////////////

		if ($(".first-level ul li.highlighted-menu.hidden").length) {
			// the highlingting is inside a closed first-level menu entry, move it to the parent
			$(".first-level ul li.highlighted-menu").parent().parent().addClass("highlighted-menu");
			$(".first-level ul li.highlighted-menu").removeClass("highlighted-menu");
		}

		////////////////// ACCORDION EFFECT //////////////////////////////

		// accordion effect on right menu
		$("#right-and-search-menu li.expandable .caption").off("click").on(
			"click",
			function() {
				if ($(this).hasClass("caption"))
					MenuFunctions.addHighlightToItem($(this).parent());
				else
					MenuFunctions.addHighlightToItem($(this));
				var wasExpanded = $(this).parent().hasClass("expanded");
				$("#right-and-search-menu li.expandable").removeClass("expanded");
				$("#right-and-search-menu li.first-level ul").addClass("hidden");
				if (! wasExpanded) {
					$(this).nextAll().first().removeClass("hidden");
					$(this).parent().addClass("expanded");
				} else if ($(".first-level ul li.highlighted-menu.hidden").length) {
					// the highlingting is inside a closed first-level menu entry, move it to the parent
					$(".first-level ul li.highlighted-menu").parent().parent().addClass("highlighted-menu");
					$(".first-level ul li.highlighted-menu").removeClass("highlighted-menu");
				}
			}
		);
	};

	MenuFunctions.removeHighligthsToItems = function() {
		if (! $("#search-menu").hasClass("hidden-by-menu-selection"))
			$("#search-menu.highlighted-menu, #search-menu .highlighted-menu").removeClass("highlighted-menu");
		else
			$("#right-and-search-menu .first-level.highlighted-menu, #right-and-search-menu .first-level .highlighted-menu").removeClass("highlighted-menu");
	};

	MenuFunctions.prototype.prevItemForHighlighting = function(object) {
		var isSearchMother = object.hasClass("search");
		var isFirstLevel = object.hasClass("first-level");
		if (isSearchMother) {
			return object.children("ul").children(".active:not(.hidden)").last();
		} else if (isFirstLevel) {
			let prevFirstLevelObject = object.prevAll(".first-level.active:not(.hidden)").first();
			if (! prevFirstLevelObject.length)
				prevFirstLevelObject = object.nextAll(".first-level.active:not(.hidden)").last();

			if (
				prevFirstLevelObject.hasClass("expandable") &&
				prevFirstLevelObject.hasClass("expanded")  &&
				prevFirstLevelObject.children("ul").children(".active:not(.hidden)").length
			) {
				// go to the last child of the previous first level menu item
				return prevFirstLevelObject.children("ul").children(".active:not(.hidden)").last();
			} else {
				// go to the previous first-level menu entry
				return prevFirstLevelObject;
			}
		} else if (! object.prevAll(".active:not(.hidden)").length) {
			// go to its first-level menu entry
			return object.parent().parent();
		} else {
			// go to the previous second-level menu entry
			return object.prevAll(".active:not(.hidden)").first();
		}
	};

	MenuFunctions.prototype.nextItemForHighlighting = function(object) {
		var isSearchMother = object.hasClass("search");
		var isFirstLevel = isSearchMother || object.hasClass("first-level");
		if (isFirstLevel) {
			if (
				! isSearchMother &&
				! object.hasClass("expandable") ||
				object.hasClass("expandable") && ! object.hasClass("expanded") ||
				object.hasClass("expandable") && object.hasClass("expanded") && ! object.children("ul").children(".active:not(.hidden)").length
			) {
				// go to the next first-level menu entry
				let nextFirstLevelObject = object.nextAll(".first-level.active:not(.hidden)").first();
				if (nextFirstLevelObject.length)
					return nextFirstLevelObject;
				else
					return object.siblings(".first-level:not(.hidden)").first();
			} else if (isSearchMother || object.hasClass("expandable") && object.hasClass("expanded")) {
				// go to its first child
				return object.children("ul").children(".active:not(.hidden)").first();
			}
		}

		var isSearchChild = object.parent().parent().hasClass("search");
		if (isSearchChild && ! object.nextAll(".active:not(.hidden)").length) {
			return object.parent().parent();
		} else if (! isSearchChild && ! object.nextAll(".active:not(.hidden)").length) {
			// go to the next first-level menu entry
			let nextFirstLevelObject = object.parent().parent().nextAll(".first-level.active:not(.hidden)").first();
			if (nextFirstLevelObject.length)
				return nextFirstLevelObject;
			else
				return object.parent().parent().siblings(".first-level.active:not(.hidden)").first();
		} else {
			return object.nextAll(".active:not(.hidden)").first();
		}
	};

	MenuFunctions.openSearchMenu = function(album) {
		$("#right-and-search-menu").addClass("expanded");
		$("#search-icon, #search-menu").addClass("expanded");
		$("#menu-icon, #right-menu").removeClass("expanded");

		$("#search-menu").removeClass("hidden-by-menu-selection");
		$(".first-level").addClass("hidden-by-menu-selection");

		MenuFunctions.highlightMenu();

		$("#album-and-media-container:not(.show-media) #album-view").css("opacity", "0.3");
		$(".media-popup .leaflet-popup-content-wrapper").css("background-color", "darkgray");

		MenuFunctions.updateMenu(album);
	};

	MenuFunctions.prototype.toggleSearchMenu = function(ev, album) {
		if (! $("#right-and-search-menu").hasClass("expanded") || ! $("#search-menu").hasClass("expanded"))
			MenuFunctions.openSearchMenu(album);
		else
			MenuFunctions.closeMenu();
	};

	MenuFunctions.openRightMenu = function(album) {
		$("#right-and-search-menu").addClass("expanded");
		$("#search-icon, #search-menu").removeClass("expanded");
		$("#menu-icon, #right-menu").addClass("expanded");

		$("#search-menu").addClass("hidden-by-menu-selection");
		$(".first-level").removeClass("hidden-by-menu-selection");

		MenuFunctions.highlightMenu();

		$("#album-and-media-container:not(.show-media) #album-view").css("opacity", "0.3");
		$(".media-popup .leaflet-popup-content-wrapper").css("background-color", "darkgray");
		MenuFunctions.updateMenu(album);
	};

	MenuFunctions.prototype.toggleRightMenu = function(ev, album) {
		if (! $("#right-and-search-menu").hasClass("expanded") || ! $("#right-menu").hasClass("expanded"))
			MenuFunctions.openRightMenu(album);
		else
			MenuFunctions.closeMenu();
	};

	MenuFunctions.openMenu = function(album) {
		if ($(".first-level").hasClass("hidden-by-menu-selection")) {
			MenuFunctions.openSearchMenu();
		} else {
			MenuFunctions.openRightMenu();
		}

	};

	MenuFunctions.closeMenu = function() {
		$("#right-and-search-menu").removeClass("expanded");
		$("#search-icon, #search-menu, #menu-icon, #right-menu").removeClass("expanded");
		$("#search-menu").addClass("hidden-by-menu-selection");

		$("#album-view").css("opacity", "");
		$(".media-popup .leaflet-popup-content-wrapper").css("background-color", "");
	};

	MenuFunctions.prototype.toggleMenu = function(ev, album) {
		if (! $("#right-and-search-menu").hasClass("expanded"))
			MenuFunctions.openMenu(album);
		else
			MenuFunctions.closeMenu();
	};

	MenuFunctions.prototype.focusSearchField = function() {
		if (! env.isAnyMobile) {
			$("#search-button").focus();
		} else {
			$("#search-button").blur();
		}
		if (
			! $("#search-menu .highlighted-menu").length &&
			! $("#search-menu .was-highlighted").length
		)
			$("#search-menu:not(.hidden-by-menu-selection)").addClass("highlighted-menu");
		else if (
			! $("#search-menu .highlighted-menu").length &&
			$("#search-menu .was-highlighted").length
		)
			$("#search-menu:not(.hidden-by-menu-selection) .was-highlighted").removeClass("was-highlighted").addClass("highlighted-menu");
	};

	MenuFunctions.highlightMenu = function() {
		if (
			! $("#search-menu.highlighted-menu").length &&
			! $("#search-menu .highlighted-menu").length &&
			! $("#search-menu.was-highlighted").length &&
			! $("#search-menu .was-highlighted").length
		)
			$("#search-menu:not(.hidden-by-menu-selection)").addClass("highlighted-menu");
		else if(
			! $("#search-menu.highlighted-menu").length &&
			! $("#search-menu .highlighted-menu").length && (
				$("#search-menu.was-highlighted").length ||
				$("#search-menu .was-highlighted").length
			)
		)
			$("#search-menu:not(.hidden-by-menu-selection).was-highlighted").removeClass("was-highlighted").addClass("highlighted-menu");

		if (
			! $("#right-and-search-menu li.first-level.highlighted-menu").length &&
			! $("#right-and-search-menu li.first-level .highlighted-menu").length &&
			! $("#right-and-search-menu li.first-level.was-highlighted").length &&
			! $("#right-and-search-menu li.first-level .was-highlighted").length
		)
			$("#right-and-search-menu li.first-level:not(.hidden-by-menu-selection)").first().addClass("highlighted-menu");
		else if(
			! $("#right-and-search-menu li.first-level.highlighted-menu").length &&
			! $("#right-and-search-menu li.first-level .highlighted-menu").length && (
				$("#right-and-search-menu li.first-level.was-highlighted").length ||
				$("#right-and-search-menu li.first-level .was-highlighted").length
			)
		)
			$("#right-and-search-menu li.first-level:not(.hidden-by-menu-selection).was-highlighted").removeClass("was-highlighted").addClass("highlighted-menu");
	};

	MenuFunctions.addHighlightToItem = function(object) {
		if(object.attr("id") !== "menu-icon" && object.attr("id") !== "search-icon")
			MenuFunctions.removeHighligthsToItems();
		object.addClass("highlighted-menu");
	};

	MenuFunctions.prototype.highlightedItemObject = function() {
		if (! $("#search-menu").hasClass("hidden-by-menu-selection"))
			return $("#search-menu.highlighted-menu, #search-menu .highlighted-menu");
		else
			return $("#right-and-search-menu .first-level.highlighted-menu, #right-and-search-menu .first-level .highlighted-menu");
	};

	MenuFunctions.prototype.setOptions = function() {
		$("body").css("background-color", env.options.background_color);

		util.setFinalOptions(env.currentMedia, false);
	};

	MenuFunctions.getBooleanCookie = function(key) {
		var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
		if (! keyValue)
			return null;
		else if (keyValue[2] === "1")
			return true;
		else
			return false;
	};

	MenuFunctions.setBooleanCookie = function(key, value) {
		if (value)
			value = 1;
		else
			value = 0;
		MenuFunctions.setCookie(key, value);
		return true;
	};

	MenuFunctions.getCookie = function(key) {
		var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
		if (! keyValue)
			return null;
		else
			return keyValue[2];
	};

	MenuFunctions.getNumberCookie = function(key) {
		var keyValue = MenuFunctions.getCookie(key);
		if (keyValue === null)
			return null;
		else
			return parseFloat(keyValue);
	};

	MenuFunctions.setCookie = function(key, value) {
		var expires = new Date();
		expires.setTime(expires.getTime() + util.tenYears() * 1000);
		document.cookie = key + '=' + value + ';expires=' + expires.toUTCString() + ';samesite=lax';
		return true;
	};

	MenuFunctions.setLastMapCenterAndZoom = function(center, zoom) {
		env.lastMapPositionAndZoom = {center: center, zoom: zoom};
		MenuFunctions.setCookie("centerAndZoom", JSON.stringify(env.lastMapPositionAndZoom));
	};

	MenuFunctions.prototype.getLastMapCenterAndZoom = function() {
		var centerAndZoomCookie = MenuFunctions.getCookie("centerAndZoom");
		if (centerAndZoomCookie)
			env.lastMapPositionAndZoom = JSON.parse(centerAndZoomCookie);
		else
			env.lastMapPositionAndZoom = {
				center: 0,
				zoom: 10
			};
	};

	MenuFunctions.setKeepShowingGeolocationSuggestTextState = function(state) {
		if (typeof env !== "undefined")
			env.keepShowingGeolocationSuggestText = state;
		MenuFunctions.setBooleanCookie("showGeolocationSuggestText", state);
	};

	MenuFunctions.getKeepShowingGeolocationSuggestTextState = function() {
		showLocationSuggestTextCookie = MenuFunctions.getBooleanCookie("showGeolocationSuggestText");
		if (showLocationSuggestTextCookie !== null) {
			MenuFunctions.setKeepShowingGeolocationSuggestTextState(showLocationSuggestTextCookie);
		} else {
			if (typeof env !== "undefined")
				env.keepShowingGeolocationSuggestText = true;
			MenuFunctions.setKeepShowingGeolocationSuggestTextState(true);
		}
	};

	MenuFunctions.prototype.getOptions = function() {
		function sendEmail(href, selectorToFadeOut) {
			// location.href = href;
			var request = new XMLHttpRequest();
			request.onreadystatechange = function() {
				var self = this;
				if (self.readyState === 4) {
					$(selectorToFadeOut).fadeOut(100);
					if (self.status >= 200 && self.status < 400) {
						if (self.responseText.indexOf("email sent!") !== -1) {
							// the email has been sent
							$("#email-sent").stop().fadeIn(
								500,
								function() {
									$("#email-sent").fadeOut(3000);
								}
							);
						} else {
							// the email hasn't been sent
							$("#email-not-sent").stop().fadeIn(
								500,
								function() {
									$("#email-not-sent").append("<br /><span class='little'>"+ self.responseText + "</span>");
									$("#email-not-sent").fadeOut(5000);
								}
							);
						}
					}
				}
			};
			request.open("GET", href, true);
			// request.open("GET", window.location.origin + window.location.pathname + href, true);
			// request.setRequestHeader('Content-Type', 'application/json');
			request.send();
		}

		function setEnvLanguage() {
			env.userLanguages = navigator.language || navigator.userLanguage || navigator.browserLanguage || navigator.systemLanguage;

			env.numberFormat = Intl.NumberFormat(env.userLanguages);

			var userLanguage = env.userLanguages.split('-')[0];
			if (userLanguage && env.translations[userLanguage] !== undefined)
				env.language = userLanguage;
			else if (env.options.language && env.translations[env.options.language] !== undefined)
				env.language = env.options.language;
			else
				env.language = "en";
		}

		// beginning of getOptions body
		if (Object.keys(env.options).length > 0) {
			if (! util.isSearchHash()) {
				// reset the return link from search
				env.options.cache_base_to_search_in = phFl.convertHashToCacheBase(env.albumCacheBase);
			}
			return Promise.resolve();
		} else {
			return new Promise(
				function(resolve_getOptions, reject_getOptions) {
					var promise = phFl.getJsonFile("options.json");
					promise.then(
						function(data) {
							// for map zoom levels, see http://wiki.openstreetmap.org/wiki/Zoom_levels

							for (let key in data) {
								if (data.hasOwnProperty(key))
									env.options[key] = data[key];
							}

							// temporarily add default values for options not included yet in options.json
							if (env.options.expose_original_media === undefined) {
								env.options.expose_original_media = true;
								env.options.expose_full_size_media_in_cache = false;
								env.options.expose_image_metadata = true;
								env.options.expose_image_dates = true;
								env.options.expose_image_positions = true;
							}

							if (env.options.save_data)
								// do not optimize image formats
								env.devicePixelRatio = 1;

							// save the options in order to restore them if requested
							env.defaultOptions = util.cloneObject(env.options);

							setEnvLanguage();
							util.translate();

							env.maxSize = env.options.reduced_sizes[env.options.reduced_sizes.length - 1];

							// override according to user selections

							var albumNameSortCookie = MenuFunctions.getBooleanCookie("albumNameSortRequested");
							env.albumNameSort = env.options.default_album_name_sort;
							if (albumNameSortCookie !== null)
								env.albumNameSort = albumNameSortCookie;

							var albumReverseSortCookie = MenuFunctions.getBooleanCookie("albumReverseSortRequested");
							env.albumReverseSort = env.options.default_album_reverse_sort;
							if (albumReverseSortCookie !== null)
								env.albumReverseSort = albumReverseSortCookie;

							var mediaNameSortCookie = MenuFunctions.getBooleanCookie("mediaNameSortRequested");
							env.mediaNameSort = env.options.default_media_name_sort;
							if (mediaNameSortCookie !== null)
								env.mediaNameSort = mediaNameSortCookie;

							var mediaReverseSortCookie = MenuFunctions.getBooleanCookie("mediaReverseSortRequested");
							env.mediaReverseSort = env.options.default_media_reverse_sort;
							if (mediaReverseSortCookie !== null)
								env.mediaReverseSort = mediaReverseSortCookie;

							var titleCookie = MenuFunctions.getBooleanCookie("hideTitle");
							if (titleCookie !== null)
								env.options.hide_title = titleCookie;

							var descriptionsCookie = MenuFunctions.getBooleanCookie("hideDescriptions");
							if (descriptionsCookie !== null)
								env.options.hide_descriptions = descriptionsCookie;

							var tagsCookie = MenuFunctions.getBooleanCookie("hideTags");
							if (tagsCookie !== null)
								env.options.hide_tags = tagsCookie;

							var bottomThumbnailsCookie = MenuFunctions.getBooleanCookie("hideBottomThumbnails");
							if (bottomThumbnailsCookie !== null)
								env.options.hide_bottom_thumbnails = bottomThumbnailsCookie;

							var saveDataCookie = MenuFunctions.getBooleanCookie("saveData");
							if (saveDataCookie !== null)
								env.options.save_data = saveDataCookie;

							var slideCookie = MenuFunctions.getBooleanCookie("albumsSlideStyle");
							if (slideCookie !== null)
								env.options.albums_slide_style = slideCookie;

							if (env.options.thumb_spacing)
								env.options.spacingSavedValue = env.options.thumb_spacing;
							else
								env.options.spacingSavedValue = env.options.media_thumb_size * 0.03;

							var spacingCookie = MenuFunctions.getNumberCookie("spacing");
							if (spacingCookie !== null) {
								env.options.spacing = spacingCookie;
							} else {
								env.options.spacing = env.options.thumb_spacing;
							}

							var showAlbumNamesCookie = MenuFunctions.getBooleanCookie("showAlbumNamesBelowThumbs");
							if (showAlbumNamesCookie !== null)
								env.options.show_album_names_below_thumbs = showAlbumNamesCookie;

							var showMediaCountCookie = MenuFunctions.getBooleanCookie("showAlbumMediaCount");
							if (showMediaCountCookie !== null)
								env.options.show_album_media_count = showMediaCountCookie;

							var showMediaNamesCookie = MenuFunctions.getBooleanCookie("showMediaNamesBelowThumbs");
							if (showMediaNamesCookie !== null)
								env.options.show_media_names_below_thumbs = showMediaNamesCookie;

							var squareAlbumsCookie = MenuFunctions.getCookie("albumThumbType");
							if (squareAlbumsCookie !== null)
								env.options.album_thumb_type = squareAlbumsCookie;

							var squareMediaCookie = MenuFunctions.getCookie("mediaThumbType");
							if (squareMediaCookie !== null)
								env.options.media_thumb_type = squareMediaCookie;

							env.options.search_inside_words = false;
							var searchInsideWordsCookie = MenuFunctions.getBooleanCookie("searchInsideWords");
							if (searchInsideWordsCookie !== null)
								env.options.search_inside_words = searchInsideWordsCookie;

							env.options.search_any_word = false;
							var searchAnyWordCookie = MenuFunctions.getBooleanCookie("searchAnyWord");
							if (searchAnyWordCookie !== null)
								env.options.search_any_word = searchAnyWordCookie;

							env.options.search_case_sensitive = false;
							var searchCaseSensitiveCookie = MenuFunctions.getBooleanCookie("searchCaseSensitive");
							if (searchCaseSensitiveCookie !== null)
								env.options.search_case_sensitive = searchCaseSensitiveCookie;

							env.options.search_accent_sensitive = false;
							var searchAccentSensitiveCookie = MenuFunctions.getBooleanCookie("searchAccentSensitive");
							if (searchAccentSensitiveCookie !== null)
								env.options.search_accent_sensitive = searchAccentSensitiveCookie;

							env.options.search_tags_only = false;
							var searchTagsOnlyCookie = MenuFunctions.getBooleanCookie("searchTagsOnly");
							if (searchTagsOnlyCookie !== null)
								env.options.search_tags_only = searchTagsOnlyCookie;

							env.options.search_current_album = true;
							var searchCurrentAlbumCookie = MenuFunctions.getBooleanCookie("searchCurrentAlbum");
							if (searchCurrentAlbumCookie !== null)
								env.options.search_current_album = searchCurrentAlbumCookie;

							env.options.show_big_virtual_folders = false;
							var showBigVirtualFoldersCookie = MenuFunctions.getBooleanCookie("showBigVirtualFolders");
							if (showBigVirtualFoldersCookie !== null)
								env.options.show_big_virtual_folders = showBigVirtualFoldersCookie;

							if (! env.options.hasOwnProperty('cache_base_to_search_in') || ! env.options.cache_base_to_search_in)
								env.options.cache_base_to_search_in = env.options.folders_string;
							if (! env.options.hasOwnProperty('saved_cache_base_to_search_in') || ! env.options.saved_cache_base_to_search_in)
								env.options.saved_cache_base_to_search_in = env.options.folders_string;

							env.slideAlbumButtonBorder = 1;
							env.slideBorder = 3;
							env.slideMarginFactor = 0.05;

							env.options.foldersStringWithTrailingSeparator = env.options.folders_string + env.options.cache_folder_separator;
							env.options.byDateStringWithTrailingSeparator = env.options.by_date_string + env.options.cache_folder_separator;
							env.options.byGpsStringWithTrailingSeparator = env.options.by_gps_string + env.options.cache_folder_separator;
							env.options.bySearchStringWithTrailingSeparator = env.options.by_search_string + env.options.cache_folder_separator;
							env.options.bySelectionStringWithTrailingSeparator = env.options.by_selection_string + env.options.cache_folder_separator;
							env.options.byMapStringWithTrailingSeparator = env.options.by_map_string + env.options.cache_folder_separator;

							if (util.isPhp() && env.options.request_password_email) {
								$("#request-password").off("click").on("click", util.showPasswordRequestForm);
								$("#password-request-form").submit(
									function() {
										let name = $("#form-name").val().trim();
										let email = $("#form-email").val().trim();
										let identity = $("#form-identity").val().trim();

										if (name && email && identity) {
											// alert(location.href.substr(0, - location.hash) + '?name=' + encodeURI($("#form-name").val()) + '&email=' + encodeURI($("#form-email").val()) + '&identity=' + encodeURI($("#form-identity").val()) + location.hash);
											var newHref = // location.href.substr(0, - location.hash) +
											 					'?siteurl=' + encodeURIComponent(location.href) +
																'&name=' + encodeURIComponent(name) +
																'&email=' + encodeURIComponent(email) +
																'&identity=' + encodeURIComponent(identity) +
																location.hash;
											$("#auth-text").stop().fadeOut(1000);
											$("#sending-email").stop().fadeIn(
												1000,
												function() {
													sendEmail(newHref, "#sending-email");
												}
											);
										} else {
											$("#please-fill").css("display", "table");
											$("#please-fill").fadeOut(5000);
										}
										return false;
									}
								);
							} else {
								$("#request-password").hide();
							}

							if (util.isPhp() && env.options.user_may_suggest_location && env.options.request_password_email) {
								$(".map-marker-centered-send-suggestion").off("click").on(
									"click",
									function() {
										var center = env.mymap.getCenter();
										var popupUrl =
										// var popupUrl = location.href.substring(0, location.href.length - location.hash.length) +
															'?siteurl=' + encodeURIComponent(location.href) +
															'&photo=' + encodeURIComponent(util.pathJoin([env.currentMedia.albumName, env.currentMedia.name])) +
															'&lat=' + encodeURIComponent(center.lat) +
															'&lng=' + encodeURIComponent(center.lng);
															// the following line is needed in order to bypass the browser (?) cache;
															// without the random number the php code isn't executed
										if (env.options.debug_js)
											popupUrl += '&random=' + Math.floor(Math.random() * 10000000);
										$("#sending-photo-position").stop().fadeIn(
											1000,
											function() {
												sendEmail(popupUrl, "#sending-photo-position");
											}
										);
										MenuFunctions.setLastMapCenterAndZoom(center, env.mymap.getZoom());
									}
								);
							}

							$("#padlock img").attr("alt", util._t("#padlock-img-alt-text"));

							// WARNING: do not initialize the search root album, the app must read it from its json file!
							util.initializeOrGetMapRootAlbum();
							util.initializeSelectionRootAlbum();

							env.mapAlbum = new Album();
							env.selectionAlbum = new Album();
							env.searchAlbum = new Album();
							env.searchAlbum.includedFilesByCodesSimpleCombination = new IncludedFiles();

							if (env.options.version !== undefined)
								$("#powered-by").attr("title", util._t("#software-version") + ": " + env.options.version + " - " + util._t("#json-version") + ": " + env.options.json_version.toString());
							// }

							// decide what format to use for cache images
							env.options.format = "jpg";
							for (let i = 0; i < env.options.cache_images_formats.length; i ++) {
								let format = env.options.cache_images_formats[i];
								if ($("html").hasClass(format) || ! $("html").hasClass("not" + format)) {
									env.options.format = format;
									break;
								}
							}

							resolve_getOptions();
						},
						function(jqXHR, textStatus, errorThrown) {
							if (errorThrown === "Not Found") {
								reject_getOptions();
							}
						}
					);
				}
			);
		}
	};

	MenuFunctions.prototype.toggleMetadataFromMouse = function(ev) {
		if (ev.button === 0 && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			ev.stopPropagation();
			MenuFunctions.toggleMetadata();
			return false;
		}
	};

	MenuFunctions.toggleMetadata = function() {
		if ($(".media-box .metadata").css("display") === "none") {
			$(".media-box .links").css("display", "inline").stop().fadeTo("slow", 0.5);
			$(".media-box .metadata-show").hide();
			$(".media-box .metadata-hide").show();
			$(".media-box .metadata")
				.stop()
				.css("height", 0)
				.css("padding-top", 0)
				.css("padding-bottom", 0)
				.show()
				.stop()
				.animate(
					{
						height: $("#center .metadata > table").height(),
						paddingTop: 3,
						paddingBottom: 3
					},
					"slow"
				);
		} else {
			if (env.isAnyMobile)
				$(".media-box .links").stop().fadeTo("slow", 0.25);
			else
				$(".media-box .links").stop().fadeOut("slow");
			$(".media-box .metadata-show").show();
			$(".media-box .metadata-hide").hide();
			$(".media-box .metadata")
				.stop()
				.animate(
					{
						height: 0,
						paddingTop: 0,
						paddingBottom: 0
					},
					"slow",
					function() {
						$(this).hide();
					}
				);
		}
	};

	MenuFunctions.toggleFullscreen = function(e, enterSlideshowFromFullscreen = false) {
		function afterToggling(isFullscreen) {
			if (isFullscreen || isFullscreen === undefined && env.fullScreenStatus) {
				// we have entered fullscreen
				$(".enter-fullscreen").hide();
				$(".exit-fullscreen").show();
				env.fullScreenStatus = true;

				if (env.slideshowId) {
					env.currentMedia.addRotation($("#media-center"));
					env.prevMedia.addRotation($("#media-left"));
					env.nextMedia.addRotation($("#media-right"));
					util.setFinalOptions(env.currentMedia, false);
				}

				if (env.currentMedia !== null && env.currentMedia.isVideo() && $("video#media-center")[0].paused)
					$("video#media-center")[0].play();
			} else {
				// we have exited fullscreen

				// the image is pinched in, go back to full screen and pinch the image out
				if (
					env.currentMedia !== null && (
						env.currentZoom > env.initialZoom || $(".media-box#center .title").hasClass("hidden-by-pinch")
					)
				) {
					pS.pinchOut(null, null);
					MenuFunctions.toggleFullscreen(e);
					return false;
				}

				$(".enter-fullscreen").show();
				$(".exit-fullscreen").hide();
				env.fullScreenStatus = false;

				if (env.slideshowId) {
					$("#slideshow-buttons").stop().fadeTo(1000, 0);
					$("#slideshow-buttons").off("mouseenter");
					$("#slideshow-buttons").off("mouseleave");
					clearInterval(env.slideshowId);
					env.slideshowId = 0;
					$("#fullscreen-wrapper").removeClass("paused");

					env.currentMedia.removeRotation($("#media-center"));
					env.prevMedia.removeRotation($("#media-left"));
					env.nextMedia.removeRotation($("#media-right"));
					util.setFinalOptions(env.currentMedia, false);

					if (env.slideshowCurrentMediaWasNullBefore) {
						env.slideshowCurrentMediaWasNullBefore = false;
						pS.swipeDown(util.upHash());
					}
				}
				if (env.currentMedia !== null && env.currentMedia.isVideo() && ! $("video#media-center")[0].paused)
					$("video#media-center")[0].pause();
			}
			$("#loading").hide();

			if (env.currentMedia !== null) {
				env.isFullScreenToggling = true;
				util.resizeSingleMediaWithPrevAndNext(env.currentMedia, env.currentAlbum);
			}
		}
		// end of auxiliary function

		if (Modernizr.fullscreen && ! enterSlideshowFromFullscreen) {
			e.preventDefault();

			$("#fullscreen-wrapper").fullScreen(
				{
					callback: function(isFullscreen) {
						afterToggling(isFullscreen);
					}
				}
			);
		} else {
			enterSlideshowFromFullscreen = false;
			afterToggling();
		}
	};


	MenuFunctions.prototype.toggleFullscreenFromMouse = function(ev) {
		if (ev.button === 0 && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			MenuFunctions.toggleFullscreen(ev);
			return false;
		}
	};

	MenuFunctions.prototype.showHideDownloadSelectionInfo = function() {
		if ($(".download-album.selection").hasClass("highlighted-menu") && $("#right-and-search-menu").hasClass("expanded") && $("#right-menu").hasClass("expanded")) {
			$("#how-to-download-selection").show();
			$('#how-to-download-selection-close').off("click").on(
				"click",
				function() {
					$("#how-to-download-selection").hide();
				}
			);
		} else {
			$('#how-to-download-selection-close')[0].click();
		}
	};

	MenuFunctions.prototype.getBooleanCookie = MenuFunctions.getBooleanCookie;
	MenuFunctions.prototype.setBooleanCookie = MenuFunctions.setBooleanCookie;
	MenuFunctions.prototype.setCookie = MenuFunctions.setCookie;
	MenuFunctions.prototype.updateMenu = MenuFunctions.updateMenu;
	MenuFunctions.prototype.toggleMetadata = MenuFunctions.toggleMetadata;
	MenuFunctions.prototype.toggleFullscreen = MenuFunctions.toggleFullscreen;
	MenuFunctions.prototype.openSearchMenu = MenuFunctions.openSearchMenu;
	MenuFunctions.prototype.closeMenu = MenuFunctions.closeMenu;
	MenuFunctions.prototype.openRightMenu = MenuFunctions.openRightMenu;
	MenuFunctions.prototype.highlightMenu = MenuFunctions.highlightMenu;
	MenuFunctions.prototype.addHighlightToItem = MenuFunctions.addHighlightToItem;
	MenuFunctions.prototype.setLastMapCenterAndZoom = MenuFunctions.setLastMapCenterAndZoom;
	MenuFunctions.prototype.setKeepShowingGeolocationSuggestTextState = MenuFunctions.setKeepShowingGeolocationSuggestTextState;

	window.MenuFunctions = MenuFunctions;
}());
