$(document).ready(function() {

	/*
	 * None of the globals here polutes the global scope, as everything
	 * is enclosed in an anonymous function.
	 *
	 */

	var env = new Env();
	window.env = env;

	var phFl = new PhotoFloat();
	var util = new Utilities();
	var pS = new PinchSwipe();
	var menuF = new MenuFunctions();
	var tF = new TopFunctions();

	if ($(".media-box#center").length) {
		// triplicate the #mediaview content in order to swipe the media
		var titleContent = $("#album-view").clone().children().first();
		$(".media-box#center").prepend(titleContent[0].outerHTML);
		util.mediaBoxGenerator('left');
		util.mediaBoxGenerator('right');
	}

	/* Event listeners */

	$(document).off("keydown").on("keydown", function(e) {
		if (e.key === undefined) {
			return true;
		}

		var isMap = util.isMap();
		var isPopup = util.isPopup();
		var isAuth = $("#auth-text").is(":visible");

		let upLink = util.upHash();

		if (e.key === "Escape") {
			// warning: modern browsers will always exit fullscreen when pressing esc

			if (isAuth) {
				$('#auth-close')[0].click();
				return false;
			} else if ($("#how-to-download-selection").is(":visible")) {
				$('#how-to-download-selection-close')[0].click();
				return false;
			} else if ($("#contextual-help").hasClass("visible")) {
				util.hideContextualHelp();
				return false;
			} else if ($("#you-can-suggest-photo-position").is(":visible")) {
				$("#you-can-suggest-photo-position").hide();
				return false;
			} else if ($("#tiny-url").is(":visible")) {
				$("#tiny-url").fadeOut();
				$("#album-and-media-container").stop().fadeTo(400, 1);
				return false;
			} else if ($("#right-and-search-menu").hasClass("expanded")) {
				menuF.closeMenu();
				menuF.showHideDownloadSelectionInfo();
				return false;
			} else if (env.currentMedia !== null && env.currentMedia.isVideo() && ! $("video#media-center")[0].paused) {
				// stop the video, otherwise it keeps playing
				$("video#media-center")[0].pause();
				return false;
			} else if (util.isShiftOrControl()) {
				// the +/- popup is there: close it
				$(".shift-or-control .leaflet-popup-close-button")[0].click();
				return false;
			} else if (isPopup) {
				// the popup is there: close it
				env.highlightedObjectId = null;
				$(".media-popup .leaflet-popup-close-button")[0].click();
				env.mapAlbum = util.initializeMapAlbum();
				// env.mapAlbum = {};
				// $('#popup #popup-content').html("");
				return false;
			} else if (isMap) {
				// we are in a map: close it
				$('.modal-close')[0].click();
				env.popupRefreshType = "previousAlbum";
				env.mapRefreshType = "none";
				// the menu must be updated here in order to have the browsing mode shortcuts workng
				menuF.updateMenu();
				return false;
			} else if (env.currentZoom > env.initialZoom || $(".media-box#center .title").hasClass("hidden-by-pinch")) {
				pS.pinchOut(null, null);
				return false;
			} else if (! Modernizr.fullscreen && env.fullScreenStatus) {
				// actually execution comes here only when the fullscreen mode is not the browser one
				// if the browser manages fullscreen mode, the callback of the toggleFullscreen function is executed instead
				menuF.toggleFullscreen(e);
				return false;
			} else if (upLink) {
				if (env.currentMedia !== null && env.currentMedia.isVideo() && $("video#media-center").length)
					// stop the video, otherwise it keeps playing
					$("video#media-center")[0].pause();
				if (
					env.currentAlbum.cacheBase == env.options.folders_string && util.isSearchHash() ||
					env.currentAlbum.cacheBase !== env.options.folders_string || env.currentMedia !== null && ! env.currentAlbumIsAlbumWithOneMedia
				) {
					env.fromEscKey = true;
					$("#loading").show();
					pS.swipeDown(upLink);
					return false;
				}
				if ($("#no-results").is(":visible")) {
					window.location.href = upLink;
					return false;
				}
			}
		} else if (! isAuth && ! isMap) {
			if (
				env.currentAlbum !== null &&
				env.currentAlbum.numsMedia.imagesAndVideosTotal() > 1 &&
				e.key.toLowerCase() === util._s(".slideshow-icon-title-shortcut")
			) {
				if (! env.slideshowId)
					$("#slideshow-icon").click();
				else
					menuF.toggleFullscreen(e);
				return false;
			}
			if (
				! $("#search-menu").hasClass("expanded") &&
				! e.shiftKey &&  ! e.ctrlKey &&  ! e.altKey &&
				e.key.toLowerCase() === util._s(".info-icon-shortcut")
			) {
				if ($("#contextual-help").is(":visible"))
					util.hideContextualHelp();
				else
					util.showContextualHelp();
				return false;
			} else if ($("#right-and-search-menu").hasClass("expanded")) {
				if (
					(
						! $("#search-field").is(":focus") || e.key === "Tab"
					) &&
					! e.ctrlKey && ! e.altKey
				) {
					let highlightedItemObject = menuF.highlightedItemObject();
					if (e.key === "Enter" || e.key === " ") {
						if (highlightedItemObject.hasClass("first-level") && highlightedItemObject.hasClass("expandable"))
							highlightedItemObject.children().click();
						else
							highlightedItemObject.click();
						return false;
					} else if (e.key === "ArrowDown" || e.key === "ArrowUp" || e.key === "Tab") {
						let nextItemFunction;
						if (e.key === "ArrowUp" && ! e.shiftKey || e.key === "ArrowDown" && e.shiftKey || e.key === "Tab" && e.shiftKey)
							nextItemFunction = menuF.prevItemForHighlighting;
						else
							nextItemFunction = menuF.nextItemForHighlighting;
						let nextItem = nextItemFunction(highlightedItemObject);
						menuF.addHighlightToItem(nextItem);
						$("#search-field").blur();
						menuF.showHideDownloadSelectionInfo();
						return false;
					} else if (e.key === "ArrowLeft" || e.key === "ArrowRight") {
						$("#right-and-search-menu li.first-level.hidden-by-menu-selection.was-highlighted, #search-menu.hidden-by-menu-selection.was-highlighted").addClass("highlighted-menu").removeClass("was-highlighted");
						$("#right-and-search-menu li.first-level:not(.hidden-by-menu-selection).highlighted-menu, #search-menu:not(.hidden-by-menu-selection).highlighted-menu").removeClass("highlighted-menu").addClass("was-highlighted");
						$("#right-and-search-menu li.first-level, #search-menu").toggleClass("hidden-by-menu-selection");
						$("#menu-icon, #search-icon, #right-menu, #search-menu").toggleClass("expanded");
						menuF.highlightMenu();
						menuF.focusSearchField();
						menuF.showHideDownloadSelectionInfo();
						return false;
					}
				}

				if (! $("#search-menu").hasClass("hidden-by-menu-selection") && ! $("#search-field").is(":focus")) {
					// focus the search field, so that the typed text is added
					$("#search-field").focus();
				}
			} else {
				// no class "expanded" in $("#right-and-search-menu")
 				if (! e.altKey && e.key === " ") {
					if (env.currentMedia === null || env.currentAlbumIsAlbumWithOneMedia) {
						let highlightedObject = util.highlightedObject();
						util.selectBoxObject(highlightedObject).click();
						return false;
					} else if (e.ctrlKey){
						$("#media-select-box .select-box").click();
						return false;
					}
				}
				if (! e.ctrlKey && ! e.altKey) {
					let highlightedObject = util.highlightedObject();
					if (env.currentMedia === null && e.key === "Enter") {
						highlightedObject.click();
						return false;
					} else if (
						env.currentMedia === null &&
						! e.shiftKey && ! e.ctrlKey && (
							e.key === "ArrowLeft" || e.key === "ArrowRight" || e.key === "ArrowDown" || e.key === "ArrowUp"
						) && (
							env.currentAlbum.subalbums.length || env.currentAlbum.numsMedia.imagesAndVideosTotal()
						)
					) {
						let nextObjectFunction;
						if (e.key === "ArrowLeft" || e.key === "ArrowUp")
							nextObjectFunction = util.prevObjectForHighlighting;
						else
							nextObjectFunction = util.nextObjectForHighlighting;

						let nextObject;
						if (e.key === "ArrowLeft" || e.key === "ArrowRight") {
							nextObject = nextObjectFunction(highlightedObject);
						} else {
							// e.key is "ArrowDown" or "ArrowUp"
							let currentObject = highlightedObject;
							let arrayDistances = [], objectsInLine = [];
							// first, we must reach the next line
							while (true) {
								nextObject = nextObjectFunction(currentObject);
								if (nextObject.html() === highlightedObject.html()) {
									// we have returned to the original object
									return false;
								} else if (util.verticalDistance(highlightedObject, nextObject) !== 0) {
									// we aren't on the original line any more!
									break;
								}
								// we are still on the original line
								currentObject = nextObject;
							}
							// we have reached the next line
							// now we have to reach the same column

							currentObject = nextObject;
							let firstObjectInLine = currentObject;
							while (true) {
								arrayDistances.push(Math.abs(util.horizontalDistance(highlightedObject, currentObject)));
								objectsInLine.push(nextObject);
								nextObject = nextObjectFunction(currentObject);
								if (util.verticalDistance(firstObjectInLine, nextObject) !== 0) {
									// we aren't on the following line any more!
									break;
								}
								// we are still on the following line
								currentObject = nextObject;
							}
							// choose the object which have the minimum horizontal distance
							if (! objectsInLine.length)
								return false;
							let minimumDistanceIndex = arrayDistances.indexOf(Math.min(... arrayDistances));
							nextObject = objectsInLine[minimumDistanceIndex];
						}

						util.addHighlightToMediaOrSubalbum(nextObject);
						if (nextObject.hasClass("thumb-and-caption-container")) {
							if (isPopup)
								util.scrollPopupToHighlightedThumb(nextObject);
							else
								util.scrollAlbumViewToHighlightedThumb(nextObject);
						} else {
							util.scrollAlbumViewToHighlightedSubalbum(nextObject);
						}
						return false;
					} else if (e.key === util._s(".hide-everytyhing-shortcut")) {
						e.preventDefault();
						tF.toggleTitleAndBottomThumbnailsAndDescriptionsAndTags(e);
						return false;
					} else if (e.key === "ArrowRight" && (env.currentZoom !== env.initialZoom || env.prevMedia) && env.currentMedia !== null) {
						if (env.currentZoom === env.initialZoom) {
							$("#album-and-media-container.show-media #thumbs").removeClass("hidden-by-pinch");
							$("#next")[0].click();
							// media.swipeLeft();
						} else {
							// drag
							if (! e.shiftKey)
								pS.drag(env.windowWidth / 10, {x: -1, y: 0});
							else
								// faster
								pS.drag(env.windowWidth / 3, {x: -1, y: 0});
						}
						return false;
					} else if (e.key === " " && ! e.shiftKey && env.currentMedia !== null && env.currentMedia.isVideo()) {
						if ($("video#media-center")[0].paused)
							// play the video
							$("video#media-center")[0].play();
						else
							// stop the video
							$("video#media-center")[0].pause();
						return false;
					} else if (env.slideshowId && e.key === " ") {
						if ($("#started-slideshow-pause").is(":visible"))
							$("#started-slideshow-pause")[0].click();
						else
							$("#started-slideshow-play")[0].click();
						return false;
					} else if (
						(e.key.toLowerCase() === util._s(".next-media-title-shortcut") || e.key === "Backspace" && e.shiftKey || (e.key === "Enter" || e.key === " ") && ! e.shiftKey) &&
						env.nextMedia && env.currentMedia !== null
					) {
						$("#next")[0].click();
						// env.nextMedia.swipeLeft();
						return false;
					} else if (
						(e.key.toLowerCase() === util._s(".prev-media-title-shortcut") || e.key === "Backspace" && ! e.shiftKey || (e.key === "Enter" || e.key === " ") && e.shiftKey) &&
						env.prevMedia && env.currentMedia !== null
					) {
						$("#album-and-media-container.show-media #thumbs").removeClass("hidden-by-pinch");
						$("#prev")[0].click();
						// env.prevMedia.swipeRight();
						return false;
					} else if (e.key === "ArrowLeft" && (env.currentZoom !== env.initialZoom || env.prevMedia) && env.currentMedia !== null) {
						if (env.currentZoom === env.initialZoom) {
							$("#album-and-media-container.show-media #thumbs").removeClass("hidden-by-pinch");
							$("#prev")[0].click();
							// media.swipeRight();
						} else {
							// drag
							if (! e.shiftKey)
								pS.drag(env.windowWidth / 10, {x: 1, y: 0});
							else
								// faster
								pS.drag(env.windowWidth / 3, {x: 1, y: 0});
						}
						return false;
					} else if ((e.key === "ArrowUp" || e.key === "PageUp")) {
						let canGoUp = upLink && upLink !== window.location.hash;
						if (canGoUp && e.shiftKey && env.currentMedia === null) {
							$("#loading").show();
							pS.swipeDown(upLink);
							return false;
						} else if (env.currentMedia !== null) {
							if (env.currentZoom === env.initialZoom) {
								if (canGoUp && e.shiftKey) {
								// if (e.shiftKey && ! $("#center .title").hasClass("hidden-by-pinch")) {
									pS.swipeDown(upLink);
									return false;
								}
							} else {
								// drag
								if (! e.shiftKey)
									pS.drag(env.windowHeight / 10, {x: 0, y: 1});
								else
									// faster
									pS.drag(env.windowHeight / 3, {x: 0, y: 1});
								return false;
							}
						}
					} else if (e.key === "ArrowDown" || e.key === "PageDown") {
					 	if (e.shiftKey && env.mediaLink && env.currentMedia === null) {
							pS.swipeUp(env.mediaLink);
							return false;
						} else if (env.currentMedia !== null) {
							if (env.currentZoom === env.initialZoom) {
								if (e.shiftKey) {
								// if (e.shiftKey && ! $("#center .title").hasClass("hidden-by-pinch")) {
									pS.swipeDown(upLink);
									return false;
								}
							} else {
								if (! e.shiftKey)
									pS.drag(env.windowHeight / 10, {x: 0, y: -1});
								else
									// faster
									pS.drag(env.windowHeight / 3, {x: 0, y: -1});
								return false;
							}
						}
					} else if (e.key.toLowerCase() === util._s(".download-link-shortcut")) {
						if (env.currentMedia !== null)
							$(".download-single-media .download-link")[0].click();
						return false;
					} else if (e.key.toLowerCase() === util._s(".enter-fullscreen-shortcut") && ! isPopup) {
					// } else if (e.key.toLowerCase() === util._s(".enter-fullscreen-shortcut") && env.currentMedia !== null) {
						menuF.toggleFullscreen(e);
						return false;
					} else if (e.key.toLowerCase() === util._s(".metadata-hide-shortcut") && env.currentMedia !== null) {
						menuF.toggleMetadata();
						return false;
					} else if (e.key.toLowerCase() === util._s(".original-link-shortcut") && env.currentMedia !== null) {
						$("#center .original-link")[0].click();
						return false;
					} else if (["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"].indexOf(e.key) > -1) {
						if (env.currentMedia !== null) {
							let number = parseInt(e.key);
							if (number === 0) {
								$("#album-and-media-container.show-media #thumbs").removeClass("hidden-by-pinch");
								$(".media-box#center .title").removeClass("hidden-by-pinch");
								pS.pinchOut(null, env.initialZoom);
							} else if (number > env.currentZoom) {
								// $("#album-and-media-container.show-media #thumbs").addClass("hidden-by-pinch");
								// $(".media-box#center .title").addClass("hidden-by-pinch");
								pS.pinchIn(null, number);
							} else {
								// $("#album-and-media-container.show-media #thumbs").addClass("hidden-by-pinch");
								// $(".media-box#center .title").addClass("hidden-by-pinch");
								pS.pinchOut(null, number);
							}
							return false;
						}
					} else if (e.key === "+") {
						if (env.currentMedia !== null && env.currentMedia.isImage()) {
							pS.pinchIn(null);
							return false;
						}
					} else if (e.key === "-") {
						if (env.currentMedia !== null && env.currentMedia.isImage()) {
							pS.pinchOut(null, null);
							return false;
						}
					} else if (
						e.key.toLowerCase() === util._s(".map-link-shortcut") &&
					 	! isPopup &&
						! util.geotaggedContentIsHidden() &&
						(
							env.currentMedia !== null && (
								env.currentMedia.hasGpsData() || util.isPhp() && env.options.user_may_suggest_location && env.options.request_password_email
							) ||
							env.currentMedia === null && ! env.options.save_data && env.currentAlbum.positionsAndMediaInTree.length
						)
					) {
						if ($(".map-popup-trigger-double")[0] !== undefined)
							$(".map-popup-trigger-double")[0].click();
						else
							$(".map-popup-trigger")[0].click();
						return false;
					} else if (
						e.key.toLowerCase() === util._s("#protected-content-unveil-shortcut") &&
						env.currentAlbum !== null
					) {
						if (
							env.currentAlbum.hasVeiledProtectedContent()
						) {
							util.showAuthForm();
							return false;
						}
					}

					if (e.key.toLowerCase() === util._s(".select.everything-shortcut")) {
						if (! e.shiftKey) {
							// select everything
							$(".select.everything:not(.hidden):not(.selected)").click();
						} else if (e.shiftKey) {
							// unselect everything
							$(".select.nothing").click();
						}
						return false;
					}

					if (
						env.currentMedia === null && (
							['[', ']'].indexOf(e.key) !== -1 && ! isPopup && env.currentAlbum.subalbums.length > 1 ||
							['{', '}'].indexOf(e.key) !== -1 && (env.currentAlbum.media.length > 1 || env.mapAlbum.media.length > 1)
						) && env.currentMedia === null
					) {
						// media and subalbums sort switcher

						var mode;
						var prevSortingModeMessageId, nextSortingModeMessageId;
						var sortingMessageIds = ['by-date', 'by-name', 'by-name-reverse', 'by-date-reverse'];
						var currentSortingIndex, prevSortingIndex, nextSortingIndex, prevSelector, nextSelector;

						if (['[', ']'].indexOf(e.key) !== -1) {
							mode = 'album';
						} else {
							mode = 'media';
						}

						if (
							$(".sort." + mode + "-sort.by-date").hasClass("selected") &&
							! $(".sort." + mode + "-sort.reverse").hasClass("selected")
						) {
							currentSortingIndex = 0;
							// console.log("currentSortingIndex = ", currentSortingIndex);
						} else if (
							$(".sort." + mode + "-sort.by-name").hasClass("selected") &&
							! $(".sort." + mode + "-sort.reverse").hasClass("selected")
						) {
							currentSortingIndex = 1;
							// console.log("currentSortingIndex = ", currentSortingIndex);
						} else if (
							$(".sort." + mode + "-sort.by-name").hasClass("selected") &&
							$(".sort." + mode + "-sort.reverse").hasClass("selected")
						) {
							currentSortingIndex = 2;
							// console.log("currentSortingIndex = ", currentSortingIndex);
						} else if (
							$(".sort." + mode + "-sort.by-date").hasClass("selected") &&
							$(".sort." + mode + "-sort.reverse").hasClass("selected")
						) {
							currentSortingIndex = 3;
							// console.log("currentSortingIndex = ", currentSortingIndex);
						}

						$(".sort-message").stop().hide().css("opacity", "");
						if (['[', '{'].indexOf(e.key) !== -1) {
							var prevSelectors = [".reverse", ".by-date", ".reverse", ".by-name", ];
							prevSelector = prevSelectors[currentSortingIndex];
							prevSortingIndex = (currentSortingIndex + 4 - 1) % 4;
							prevSortingModeMessageId = sortingMessageIds[prevSortingIndex] + "-" + mode + "-sorting";
							$("#" + prevSortingModeMessageId).show();
							$("#" + prevSortingModeMessageId).fadeOut(5000);
							$(".sort." + mode + "-sort" + prevSelector)[0].click();
							// console.log(".sort." + mode + "-sort" + prevSelector + " ------- " + prevSortingModeMessageId);
						} else {
							var nextSelectors = [".by-name", ".reverse", ".by-date", ".reverse"];
							nextSelector = nextSelectors[currentSortingIndex];
							nextSortingIndex = (currentSortingIndex + 1) % 4;
							nextSortingModeMessageId = sortingMessageIds[nextSortingIndex] + "-" + mode + "-sorting";
							$("#" + nextSortingModeMessageId).show();
							$("#" + nextSortingModeMessageId).fadeOut(5000);
							$(".sort." + mode + "-sort" + nextSelector)[0].click();
							// console.log(".sort." + mode + "-sort" + nextSelector + " ------- " + nextSortingModeMessageId);
						}
						return false;
					}
				}

				if (
					! env.slideshowId &&
					env.currentAlbum !== null && (
						env.currentAlbum.isAnyRoot() ||
						env.currentMedia !== null || env.currentAlbumIsAlbumWithOneMedia
					)
				) {
					// browsing mode switchers
					let nextBrowsingModeRequested = (e.key === '>');
					let prevBrowsingModeRequested = (e.key === '<');

					var filter = ".radio:not(.hidden):not(.selected)";
					if (nextBrowsingModeRequested) {
						let nextBrowsingModeObject = $(".browsing-mode-switcher.selected").nextAll(filter).first();
						if (nextBrowsingModeObject[0] === undefined)
							nextBrowsingModeObject = $(".browsing-mode-switcher.selected").siblings(filter).first();
						if (nextBrowsingModeObject[0] !== undefined) {
							$(".browsing-mode-switcher").removeClass("selected");
							nextBrowsingModeObject.addClass("selected");
							nextBrowsingModeObject[0].click();
							return false;
						}
					} else if (prevBrowsingModeRequested) {
						let prevBrowsingModeObject = $(".browsing-mode-switcher.selected").prevAll(filter).first();
						if (prevBrowsingModeObject[0] === undefined)
							prevBrowsingModeObject = $(".browsing-mode-switcher.selected").siblings(filter).last();
						if (prevBrowsingModeObject[0] !== undefined) {
							$(".browsing-mode-switcher").removeClass("selected");
							prevBrowsingModeObject.addClass("selected");
							prevBrowsingModeObject[0].click();
							return false;
						}
					}
				}
			}

			// "e" opens the menu, and closes it if focus is not in input field
			if (
				! e.shiftKey &&  ! e.ctrlKey &&  ! e.altKey &&
				e.key.toLowerCase() === util._s(".menu-icon-title-shortcut") && (
					! $("#right-and-search-menu").hasClass("expanded") ||
					$("#search-menu").hasClass("hidden-by-menu-selection")
				)
			) {
				menuF.toggleMenu();
				menuF.showHideDownloadSelectionInfo();
				return false;
			}
		}

		return true;
	});

	if (! util.isPhp()) {
		$(".ssk-show-url").hide();
	}

	util.setLinksVisibility();

	let nextTitle  = util._t(".next-media-title");
	let prevTitle  = util._t(".prev-media-title");
	if (! env.isAnyMobile) {
		nextTitle  += " [" + util._s(".next-media-title-shortcut") + "]";
		prevTitle  += " [" + util._s(".prev-media-title-shortcut") + "]";
	}
	$("#next").attr("title", nextTitle).attr("alt", ">");
	$("#prev").attr("title", prevTitle).attr("alt", "<");
	$("#pinch-in").attr("title", util._t("#pinch-in-title")).attr("alt", "+");
	$("#pinch-out").attr("title", util._t("#pinch-out-title")).attr("alt", "-");
	if (env.isAnyMobile) {
		$("#pinch-in").css("width", "30px").css("height", "30px");
		$("#pinch-out").css("width", "30px").css("height", "30px");
	}
	// $("#pinch-in").on("click", pS.pinchIn);
	// $("#pinch-out").on("click", pS.pinchOut);

	// search
	$('#search-button').off("click").on("click", function() {
		var searchOptions = '';

		// save the current hash in order to come back there when exiting from search
		if (util.isSearchCacheBase(env.albumCacheBase)) {
			// a plain search: get the folder to search in from the search album hash
			env.options.cache_base_to_search_in = env.albumCacheBase.split(env.options.cache_folder_separator).slice(2).join(env.options.cache_folder_separator);
		} else {
			// it's a subalbum of a search or it's not a search hash: use the current album hash
			env.options.cache_base_to_search_in = env.albumCacheBase;

			env.options.saved_cache_base_to_search_in = env.options.cache_base_to_search_in;
		}

		if (! env.options.hasOwnProperty('cache_base_to_search_in') || ! env.options.cache_base_to_search_in)
			env.options.cache_base_to_search_in = env.options.folders_string;

		var bySearchViewHash = env.hashBeginning + env.options.by_search_string;

		// build the search album part of the hash
		var wordsStringOriginal, wordsString;
		if (env.options.search_tags_only) {
			wordsStringOriginal = util.encodeNonLetters($("#search-field").val()).normalize().replace(/  /g, ' ').trim();
		} else {
			wordsStringOriginal = $("#search-field").val().normalize().replace(/[^\p{L}]/ug, ' ').replace(/  /g, ' ').trim();
		}
		if (! env.options.search_numbers) {
			wordsStringOriginal = wordsStringOriginal.replace(/[\p{N}]/ug, ' ').replace(/  /g, ' ').trim();
		}
		wordsString = encodeURIComponent(wordsStringOriginal.replace(/ /g, '_'));
		// TO DO: non-alphabitic words have to be filtered out
		if (wordsString) {
			if (util.isPopup()) {
				// refine the original popup content!

				// normalize the search terms
				// the normalized words are needed in order to compare with the search cache json files names, which are normalized
				var wordsStringNormalizedAccordingToOptions = util.normalizeAccordingToOptions(wordsStringOriginal);
				var wordsStringNormalized = wordsStringOriginal.toLowerCase();
				wordsStringNormalized = util.removeAccents(wordsStringNormalized);

				var searchWordsFromUser = [], searchWordsFromUserNormalized = [], searchWordsFromUserNormalizedAccordingToOptions = [];
				if (env.options.search_tags_only) {
					searchWordsFromUser = [decodeURIComponent(wordsString).replace(/_/g, " ")];
					searchWordsFromUserNormalizedAccordingToOptions = [decodeURIComponent(wordsStringNormalizedAccordingToOptions)];
					searchWordsFromUserNormalized = [decodeURIComponent(wordsStringNormalized)];
				} else {
					searchWordsFromUser = wordsString.split('_');
					searchWordsFromUserNormalizedAccordingToOptions = wordsStringNormalizedAccordingToOptions.split(' ');
					searchWordsFromUserNormalized = wordsStringNormalized.split(' ');
				}
				var removedStopWords;

				// remove the stopwords from the search terms
				let stopWordsPromise = phFl.getStopWords();
				stopWordsPromise.then(
					function () {
						[searchWordsFromUser, searchWordsFromUserNormalized, searchWordsFromUserNormalizedAccordingToOptions, removedStopWords] =
							phFl.removeStopWords(searchWordsFromUser, searchWordsFromUserNormalized, searchWordsFromUserNormalizedAccordingToOptions);

						// re-build the original map album
						var clickHistory = env.mapAlbum.clickHistory;
						env.mapAlbum = new Album();
						let playPromise = tF.playClickElement(clickHistory);
						playPromise.then(
							function popupReady() {
								if (env.options.search_any_word) {
									// at least one word
									let mediaResult = new Media([]);
									searchWordsFromUserNormalizedAccordingToOptions.forEach(
										function(normalizedSearchWord, index) {
											let mediaInMapAlbum = new Media(env.mapAlbum.media);
											mediaInMapAlbum.filterAgainstOneWordAndAlbumSearchedIn(normalizedSearchWord);
											mediaResult = util.arrayUnion(mediaResult, mediaInMapAlbum, function(a, b) {return a.isEqual(b);});
										}
									);
									env.mapAlbum.media = mediaResult;
								} else {
									env.mapAlbum.media.filterAgainstEveryWord(searchWordsFromUserNormalizedAccordingToOptions);
								}
								tF.prepareAndDoPopupUpdate();
								if (! env.options.search_inside_words && removedStopWords.length) {
									// say that some search word hasn't been used
									let stopWordsFound = " - <span class='italic'>" + removedStopWords.length + " " + util._t("#removed-stopwords") + ": ";
									for (let i = 0; i < removedStopWords.length; i ++) {
										if (i)
											stopWordsFound += ", ";
										stopWordsFound += removedStopWords[i];
									}
									stopWordsFound += "</span>";
									$("#popup-photo-count").append(stopWordsFound);
								}
							}
						);
					},
					function() {
						console.trace();
					}
				);
			} else {
				// produce a new hash in order to perform the search
				bySearchViewHash += env.options.cache_folder_separator;
				if (env.options.search_inside_words)
					searchOptions += 'i' + env.options.search_options_separator;
				if (env.options.search_any_word)
					searchOptions += 'n' + env.options.search_options_separator;
				if (env.options.search_case_sensitive)
					searchOptions += 'c' + env.options.search_options_separator;
				if (env.options.search_accent_sensitive)
					searchOptions += 'a' + env.options.search_options_separator;
				if (env.options.search_tags_only)
					searchOptions += 't' + env.options.search_options_separator;
				if (env.options.search_current_album)
					searchOptions += 'o' + env.options.search_options_separator;
				bySearchViewHash += searchOptions + wordsString;

				bySearchViewHash += env.options.cache_folder_separator + env.options.cache_base_to_search_in;

				if (bySearchViewHash !== window.location.hash) {
					$("#loading").show();
					window.location.hash = bySearchViewHash;
				}
			}
		}

		menuF.highlightMenu();
		return false;
	});


	/* Entry point for most events */

	$('#search-field').keypress(
		function(ev) {
			// $("#search-menu ul").removeClass("hidden");
			if (ev.which === 13 || ev.keyCode === 13) {
				//Enter key pressed, trigger search button click event
				$('#search-button').click();
				menuF.focusSearchField();
				$("#search-field").blur();
				return false;
			}
		}
	);

	$("input[id='search-field']").on(
		"input",
		function() {
			if ($("input[id='search-field']").val() || ! util.isSearchHash())
				util.highlightSearchedWords(true);
			else if (util.isSearchHash())
				util.highlightSearchedWords();
		}
	);


	$("li#inside-words").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleInsideWordsSearch();
		}
	);
	$("li#any-word").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleAnyWordSearch();
		}
	);
	$("li#case-sensitive").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleCaseSensitiveSearch();
		}
	);
	$("li#accent-sensitive").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleAccentSensitiveSearch();
		}
	);
	$("li#tags-only").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleTagsOnlySearch();
		}
	);
	$("li#album-search").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			util.toggleCurrentAbumSearch();
		}
	);

	$(".download-album.everything.all.full").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.everything.all.full").hasClass("active")) {
				var thisAlbum = env.currentAlbum;
				if (util.isPopup())
					thisAlbum = env.mapAlbum;
				thisAlbum.downloadAlbum(true);
				return false;
			}
		}
	);
	$(".download-album.everything.all.sized").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.everything.all.sized").hasClass("active")) {
				var thisAlbum = env.currentAlbum;
				if (util.isPopup())
					thisAlbum = env.mapAlbum;
				thisAlbum.downloadAlbum(true, "all", $(".download-album.everything.all.sized").attr("size"));
				return false;
			}
		}
	);
	$(".download-album.everything.images.full").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.everything.images.full").hasClass("active")) {
				var thisAlbum = env.currentAlbum;
				if (util.isPopup())
				thisAlbum = env.mapAlbum;
				thisAlbum.downloadAlbum(true, "images");
				return false;
			}
		}
	);
	$(".download-album.everything.images.sized").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.everything.images.sized").hasClass("active")) {
				var thisAlbum = env.currentAlbum;
				if (util.isPopup())
				thisAlbum = env.mapAlbum;
				thisAlbum.downloadAlbum(true, "images", $(".download-album.everything.images.sized").attr("size"));
				return false;
			}
		}
	);
	$(".download-album.everything.videos.full").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.everything.videos.full").hasClass("active")) {
				var thisAlbum = env.currentAlbum;
				if (util.isPopup())
				thisAlbum = env.mapAlbum;
				thisAlbum.downloadAlbum(true, "videos");
				return false;
			}
		}
	);
	$(".download-album.everything.videos.sized").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.everything.videos.sized").hasClass("active")) {
				var thisAlbum = env.currentAlbum;
				if (util.isPopup())
				thisAlbum = env.mapAlbum;
				thisAlbum.downloadAlbum(true, "videos", $(".download-album.everything.videos.sized").attr("size"));
				return false;
			}
		}
	);

	$(".download-album.media-only.all.full").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.media-only.all").hasClass("active")) {
				var thisAlbum = env.currentAlbum;
				if (util.isPopup())
				thisAlbum = env.mapAlbum;
				thisAlbum.downloadAlbum(false, "all");
				return false;
			}
		}
	);
	$(".download-album.media-only.all.sized").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.media-only.all.sized").hasClass("active")) {
				var thisAlbum = env.currentAlbum;
				if (util.isPopup())
				thisAlbum = env.mapAlbum;
				thisAlbum.downloadAlbum(false, "all", $(".download-album.media-only.all.sized").attr("size"));
				return false;
			}
		}
	);
	$(".download-album.media-only.images.full").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.media-only.images").hasClass("active")) {
				var thisAlbum = env.currentAlbum;
				if (util.isPopup())
				thisAlbum = env.mapAlbum;
				thisAlbum.downloadAlbum(false, "images");
				return false;
			}
		}
	);
	$(".download-album.media-only.images.sized").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.media-only.images.sized").hasClass("active")) {
				var thisAlbum = env.currentAlbum;
				if (util.isPopup())
				thisAlbum = env.mapAlbum;
				thisAlbum.downloadAlbum(false, "images", $(".download-album.media-only.images.sized").attr("size"));
				return false;
			}
		}
	);
	$(".download-album.media-only.videos.full").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.media-only.videos").hasClass("active")) {
				var thisAlbum = env.currentAlbum;
				if (util.isPopup())
				thisAlbum = env.mapAlbum;
				thisAlbum.downloadAlbum(false, "videos");
				return false;
			}
		}
	);
	$(".download-album.media-only.videos.sized").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			if ($(".download-album.media-only.videos.sized").hasClass("active")) {
				var thisAlbum = env.currentAlbum;
				if (util.isPopup())
					thisAlbum = env.mapAlbum;
				thisAlbum.downloadAlbum(false, "videos", $(".download-album.media-only.videos.sized").attr("size"));
				return false;
			}
		}
	);
	$(".download-album.selection.active").off("click").on(
		"click",
		function() {
			menuF.addHighlightToItem($(this));
			menuF.showHideDownloadSelectionInfo();
			return false;
		}
	);

	// binds the click events to the sort buttons

	$("#right-and-search-menu li.show-title").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleTitle(ev);
		}
	);
	$("#right-and-search-menu li.show-descriptions").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleDescriptions(ev);
		}
	);
	$("#right-and-search-menu li.show-tags").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleTags(ev);
		}
	);
	$("#right-and-search-menu li.show-bottom-thumbnails").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleBottomThumbnails(ev);
		}
	);
	$("#right-and-search-menu li.slide").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleSlideMode(ev);
		}
	);
	$("#right-and-search-menu li.spaced").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleSpacing(ev);
		}
	);
	$("#right-and-search-menu li.show-album-names").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleAlbumNames(ev);
		}
	);
	$("#right-and-search-menu li.show-media-counts").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleMediaCount(ev);
		}
	);
	$("#right-and-search-menu li.show-media-names").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleMediaNames(ev);
		}
	);
	$("#right-and-search-menu li.square-album-thumbnails").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleAlbumsSquare(ev);
		}
	);
	$("#right-and-search-menu li.square-media-thumbnails").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleMediaSquare(ev);
		}
	);
	$("#restore").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.restoreSettings(ev);
		}
	);
	$("#show-big-albums").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			util.toggleBigAlbumsShow(ev);
		}
	);
	$("#save-data").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			tF.toggleSaveData(ev);
			env.isASaveDataChange = true;
			// recreate the page, so that the ui elements related to positions are shown or hidden
			$(window).hashchange();
		}
	);
	$("#search-icon").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			menuF.toggleSearchMenu(ev);
		}
	);

	$("#slideshow-icon").attr("title", util._t(".slideshow-icon-title"));

	if (env.isAnyMobile) {
		$("#right-and-search-menu .info-icon").hide();
	} else {
		$(".info-icon").off("mouseenter").on(
			"mouseenter",
			function() {
				util.showContextualHelp();
			}
		).off("mouseleave").on(
			"mouseleave",
			function() {
				util.hideContextualHelp();
			}
		);
	}

	$("#menu-icon").off("click").on(
		"click",
		function(ev) {
			ev.stopPropagation();
			menuF.addHighlightToItem($(this));
			menuF.toggleRightMenu(ev);
		}
	);

	$(
		function() {
			$("#description-wrapper").off("mouseenter mouseleave").on(
				"mouseenter",
				util.removeDescriptionOpacity
			).on(
				"mouseleave",
				util.addDescriptionOpacity
			);
		}
	);
	pS.addAlbumGesturesDetection();

	$("#auth-form").submit(
		function() {
			// This function checks the password looking for a file with the encrypted password name in the passwords subdir
			// the code in the found password file is inserted into env.guessedPasswordsMd5, and at the hash change the content unveiled by that password will be shown

			var passwordObject = $("#password");
			var encryptedPassword = md5(passwordObject.val());
			passwordObject.val("");

			var promise = phFl.getJsonFile(util.pathJoin([env.options.passwords_subdir, encryptedPassword]));
			promise.then(
				function(jsonCode) {
					passwordObject.css("background-color", "rgb(200, 200, 200)");
					var passwordCode = jsonCode.passwordCode;

					if (env.guessedPasswordCodes.length && env.guessedPasswordCodes.includes(passwordCode)) {
						passwordObject.css("background-color", "red");
						passwordObject.on(
							'input',
							function() {
								passwordObject.css("background-color", "");
								passwordObject.off('input');
							}
						);
					} else {
						env.guessedPasswordCodes.push(passwordCode);
						env.guessedPasswordsMd5.push(encryptedPassword);

						$("#loading").show();

						if (util.isMap() || util.isPopup()) {
							// the map must be generated again including the points that only carry protected content
							env.mapRefreshType = "refresh";

							if (util.isPopup()) {
								env.popupRefreshType = "mapAlbum";
								if (util.isShiftOrControl())
									$(".shift-or-control .leaflet-popup-close-button")[0].click();
								$(".media-popup .leaflet-popup-close-button")[0].click();
							} else {
								env.popupRefreshType = "none";
							}
							// close the map
							$('.modal-close')[0].click();
						}

						env.isFromAuthForm = true;
						$(window).hashchange();
					}
				},
				function() {
					passwordObject.css("background-color", "red");
					passwordObject.on(
						'input',
						function() {
							passwordObject.css("background-color", "");
							passwordObject.off('input');
						}
					);
				}
			);

			// var ajaxOptions = {
			// 	type: "GET",
			// 	dataType: "json",
			// 	url: util.pathJoin(["cache", env.options.passwords_subdir, encryptedPassword]),
			// 	success: ,
			// 	error:
			// };
			// $.ajax(ajaxOptions);

			return false;
		}
	);


	$(window).hashchange(
		function() {
			$(
				function() {
					// activate lazy loader
					if (util.isPopup())
						$('#popup-images-wrapper').scroll();
					else if (env.currentMedia !== null)
						$("#album-view").scroll();
					else
						$(window).scroll();

				}
			);
			$("#auth-text").hide();
			// $("#thumbs").show();
			$("#subalbums").removeClass("hidden");
			$("#album-view, #media-view, #my-modal").css("opacity", "");

			// $("#album-view").removeClass("hidden");
			$("link[rel=image_src]").remove();
			$("link[rel=video_src]").remove();
			// $("#right-and-search-menu").removeClass("expanded");

			if (util.isMap() || util.isPopup()) {
				// we are in a map: close it
				$('.modal-close')[0].click();
			}

			var optionsPromise = menuF.getOptions();
			optionsPromise.then(
				function() {
					[env.albumCacheBase, env.mediaCacheBase, env.mediaFolderCacheBase, env.foundAlbumCacheBase, env.collectionCacheBase] = PhotoFloat.decodeHash(window.location.hash);

					util.translate();
					$("#loading").show();

					if (! util.isSearchHash()) {
						// restore current album search flag to its default value
						env.options.search_current_album = true;
						menuF.setBooleanCookie("searchCurrentAlbum", env.options.search_current_album);
					}

					if (util.isPhp() && typeof postData !== "undefined" && postData !== null) {
						util.readPostData();
					}

					// parseHashAndReturnAlbumAndMediaIndex returns an array of 2 elements:
					// - the requested album
					// - the requested media index (if applicable)
					var hashPromise = phFl.parseHashAndReturnAlbumAndMediaIndex();
					hashPromise.then(
						function([album, mediaIndex]) {
							if (env.isABrowsingModeChangeFromMouseClick || env.isASaveDataChange) {
								if (env.isABrowsingModeChangeFromMouseClick)
									env.isABrowsingModeChangeFromMouseClick = false;
								if (env.isASaveDataChange)
									env.isASaveDataChange = false;
								menuF.openRightMenu();
							} else if (album.isSearch() && ! album.numsMediaInSubTree.imagesAndVideosTotal())
								menuF.openSearchMenu(album);
							else
								menuF.closeMenu();
							album.prepareForShowing(mediaIndex);
						},
						function(album) {
							function checkHigherAncestor() {
								if (album.isSearch())
									menuF.openSearchMenu(album);

								let upHash = util.upHash(higherHash);
								if (! higherHash.length || upHash === higherHash) {
									// the top album has been reached and no unprotected nor protected content has been found
									if (album.isEmpty || album.hasVeiledProtectedContent())
										$("#protected-content-unveil")[0].click();
								} else {
									higherHash = upHash;
									let cacheBase = higherHash.substring(env.hashBeginning.length);
									let getAlbumPromise = phFl.getAlbum(cacheBase, checkHigherAncestor, {getMedia: false, getPositions: false});
									getAlbumPromise.then(
										function(upAlbum) {
											if (upAlbum.hasVeiledProtectedContent() && ! env.fromEscKey) {
											// if (upAlbum.hasVeiledProtectedContent() && ! env.fromEscKey) {
												$("#loading").hide();
												$("#protected-content-unveil")[0].click();
											} else {
												util.errorThenGoUp();
											}
										}
									);
								}
							}
							// end of auxiliary function

							// neither the unprotected nor the protected album exist
							// the user could have opened a protected album link: the password can be asked, but only if some ancestor album has protected content
							let higherHash = location.hash;
							checkHigherAncestor();
						}
					);
				},
				function() {
					$("#album-view").fadeOut(200);
					$("#media-view").fadeOut(200);
					$("#album-view").stop().fadeIn(3500);
					$("#media-view").stop().fadeIn(3500);
					$("#error-options-file").stop().fadeIn(200);
					$("#error-options-file, #error-overlay, #auth-text").fadeOut(2500);
				}
			);
		}
	);

	// Execution starts here
	// An array of promises is used in order to prepare for new formats (avif, jxl)
	// they aren't supported yet because Pillow doesn't support them
	var formatPromises = [];
	var webpPromise = new Promise(
		function(resolve_webpPromise) {
			Modernizr.on(
				"webp",
				function(webpSupported) {
					if (webpSupported) {
						// format supported
						$("html").addClass("webp");
					} else {
						// not-supported
						$("html").addClass("notwebp");
					}
					resolve_webpPromise();
				}
			);
		}
	);
	formatPromises.push(webpPromise);

	Promise.all(formatPromises).then(
		function() {
			$(window).hashchange();
		}
	);
});
