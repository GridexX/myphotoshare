(function() {
	var lastSelectionAlbumIndex = 0;
	var lastMapAlbumIndex = 0;

	/* constructor */
	function Utilities() {
	}

	Utilities.openInNewTab = function(hash) {
		// albums is a list of objects {albumName: album}
		var typeOfPackedAlbum, stringifiedPackedAlbum, albumName;

		var newFormObject = $(
			"<form>",
			{
				action: hash,
				target: "_blank",
				method: "post"
			}
		);

		let albums = [];
		if (Utilities.somethingIsInMapAlbum()) {
			albums.push(env.mapAlbum);
		}
		if (Utilities.somethingIsSelected()) {
			albums.push(env.selectionAlbum);
		}

		for (let iAlbum = 0; iAlbum < albums.length; iAlbum ++) {
			var packedAlbum = lzwCompress.pack(albums[iAlbum].toJson());
			// var packedAlbum = lzwCompress.pack(JSON.decycle(albums[albumName]));
			if (Array.isArray(packedAlbum)) {
				stringifiedPackedAlbum = packedAlbum.join();
				typeOfPackedAlbum = "array";
			} else {
				typeOfPackedAlbum = "string";
				stringifiedPackedAlbum = packedAlbum;
			}
			if (albums[iAlbum].isSelection())
				albumName = "selectionAlbum";
			else
				albumName = "mapAlbum";

			let iString = iAlbum.toString();
			newFormObject.append(
				$(
					"<input>",
					{
						name: "albumName_" + iString,
						value: albumName,
						type: "hidden"
					}
				)
			).append(
				$(
					"<input>",
					{
						name: "stringifiedPackedAlbum_" + iString,
						value: stringifiedPackedAlbum,
						type: "hidden"
					}
				)
			).append(
				$(
					"<input>",
					{
						name: "typeOfPackedAlbum_" + iString,
						value: typeOfPackedAlbum,
						type: "hidden"
					}
				)
			);
		}

		newFormObject.append(
			$(
				"<input>",
				{
					name: "selectorClickedToOpenTheMap",
					value: env.selectorClickedToOpenTheMap,
					type: "hidden"
				}
			)
		);

		let currentAlbumIs = "";
		if (env.currentAlbum === env.mapAlbum)
			currentAlbumIs = "mapAlbum";
		else if (env.currentAlbum === env.selectionAlbum)
			currentAlbumIs = "selectionAlbum";
		newFormObject.append(
			$(
				"<input>",
				{
					name: "currentAlbumIs",
					value: currentAlbumIs,
					type: "hidden"
				}
			)
		);

		if (env.guessedPasswordsMd5.length) {
			newFormObject.append(
				$(
					"<input>",
					{
						name: "guessedPasswordsMd5",
						value: env.guessedPasswordsMd5.join('-'),
						type: "hidden"
					}
				)
			).append(
				$(
					"<input>",
					{
						name: "guessedPasswordCodes",
						value: env.guessedPasswordCodes.join('-'),
						type: "hidden"
					}
				)
			);
		}
		newFormObject.hide().appendTo("body").submit().remove();
		return false;
	};

	Utilities.prototype.readPostData = function() {
		if (postData.guessedPasswordsMd5) {
			env.guessedPasswordsMd5 = postData.guessedPasswordsMd5.split('-');
			env.guessedPasswordCodes = postData.guessedPasswordCodes.split('-');
			delete postData.guessedPasswordsMd5;
		}
		env.selectorClickedToOpenTheMap = postData.selectorClickedToOpenTheMap;

		let stringifiedPackedAlbum, packedAlbum, albumName, uncompressedAlbum;
		let index = 0;

		while (postData.hasOwnProperty("albumName_" + index.toString())) {
			albumName = postData["albumName_" + index.toString()];
			stringifiedPackedAlbum = postData["stringifiedPackedAlbum_" + index.toString()];
			if (postData["typeOfPackedAlbum_" + index.toString()] === "array") {
				packedAlbum = stringifiedPackedAlbum.split(',').map(x => parseInt(x, 10));
			} else {
				packedAlbum = stringifiedPackedAlbum;
			}
			uncompressedAlbum = new Album(JSON.parse(lzwCompress.unpack(packedAlbum)));
			if (albumName === "mapAlbum") {
				env.mapAlbum = uncompressedAlbum;
			} else if (albumName === "selectionAlbum") {
				env.selectionAlbum = uncompressedAlbum;
			}
			index ++;
		}
		if (postData.currentAlbumIs === "mapAlbum")
			env.currentAlbum = env.mapAlbum;
		else if (postData.currentAlbumIs === "selectionAlbum")
			env.currentAlbum = env.selectionAlbum;
		// invalidate the variable so that it's not used any more
		postData = null;
	};

	Utilities.prototype.initializeOrGetMapRootAlbum = function() {
		// prepare the root of the map albums and put it in the cache
		var rootMapAlbum = env.cache.getAlbum(env.options.by_map_string);
		if (! rootMapAlbum) {
			rootMapAlbum = new Album(env.options.by_map_string);
			env.cache.putAlbum(rootMapAlbum);
		}
		return rootMapAlbum;
	};

	Utilities.prototype.initializeMapAlbum = function() {
		var rootMapAlbum = env.cache.getAlbum(env.options.by_map_string);
		// if (! rootMapAlbum)

		lastMapAlbumIndex ++;

		// initializes the map album
		var newMapAlbum = new Album(env.options.by_map_string + env.options.cache_folder_separator + lastMapAlbumIndex + env.options.cache_folder_separator + env.currentAlbum.cacheBase);
		newMapAlbum.path = newMapAlbum.cacheBase.replace(env.options.cache_folder_separator, "/");

		rootMapAlbum.numsMediaInSubTree.sum(newMapAlbum.numsMediaInSubTree);
		rootMapAlbum.subalbums.push(newMapAlbum);
		if (env.options.expose_image_positions) {
			rootMapAlbum.positionsAndMediaInTree.mergePositionsAndMedia(newMapAlbum.positionsAndMediaInTree);
			rootMapAlbum.numPositionsInTree = rootMapAlbum.positionsAndMediaInTree.length;
		}

		newMapAlbum.ancestorsCacheBase = rootMapAlbum.ancestorsCacheBase.slice();
		newMapAlbum.ancestorsCacheBase.push(newMapAlbum.cacheBase);

		newMapAlbum.ancestorsNames = [env.options.by_map_string, newMapAlbum.cacheBase];

		return newMapAlbum;
	};

	Utilities.prototype.initializeSearchAlbumBegin = function(albumCacheBase) {
		var newSearchAlbum = new Album(albumCacheBase);
		newSearchAlbum.path = newSearchAlbum.cacheBase.replace(env.options.cache_folder_separator, "/");
		newSearchAlbum.ancestorsNames = [env.options.by_search_string, newSearchAlbum.cacheBase];
		newSearchAlbum.includedFilesByCodesSimpleCombination = new IncludedFiles({",": {}});

		return newSearchAlbum;
	};

	Utilities.initializeSearchRootAlbum = function() {
		var rootSearchAlbum = new Album(env.options.by_search_string);
		env.cache.putAlbum(rootSearchAlbum);
	};


	Utilities.prototype.initializeSelectionRootAlbum = function() {
		// prepare the root of the selections albums and put it in the cache
		var rootSelectionAlbum = env.cache.getAlbum(env.options.by_selection_string);
		if (! rootSelectionAlbum) {
			rootSelectionAlbum = new Album(env.options.by_selection_string);
			env.cache.putAlbum(rootSelectionAlbum);
		}

		return rootSelectionAlbum;
	};

	Utilities.prototype.initializeSelectionAlbum = function() {
		// initializes the selection album

		var rootSelectionAlbum = env.cache.getAlbum(env.options.by_selection_string);

		lastSelectionAlbumIndex ++;

		env.selectionAlbum = new Album(env.options.by_selection_string + env.options.cache_folder_separator + lastSelectionAlbumIndex);
		env.selectionAlbum.path = env.selectionAlbum.cacheBase.replace(env.options.cache_folder_separator, "/");

		rootSelectionAlbum.numsMediaInSubTree.sum(env.selectionAlbum.numsMediaInSubTree);
		rootSelectionAlbum.subalbums.push(env.selectionAlbum);
		if (env.options.expose_image_positions && ! env.options.save_data) {
			rootSelectionAlbum.positionsAndMediaInTree.mergePositionsAndMedia(env.selectionAlbum.positionsAndMediaInTree);
			rootSelectionAlbum.numPositionsInTree = rootSelectionAlbum.positionsAndMediaInTree.length;
		}

		env.selectionAlbum.ancestorsCacheBase = rootSelectionAlbum.ancestorsCacheBase.slice();
		env.selectionAlbum.ancestorsCacheBase.push(env.selectionAlbum.cacheBase);
		env.selectionAlbum.ancestorsNames = [env.options.by_selection_string, env.selectionAlbum.cacheBase];
	};

	Utilities.prototype.convertProtectedCacheBaseToCodesSimpleCombination = function(protectedCacheBase) {
		var protectedDirectory = protectedCacheBase.split("/")[0];
		var [albumMd5, mediaMd5] = protectedDirectory.substring(env.options.protected_directories_prefix.length).split(',');
		if (albumMd5 && mediaMd5)
			return [Utilities.convertMd5ToCode(albumMd5), Utilities.convertMd5ToCode(mediaMd5)].join(',');
		else if (albumMd5)
			return [Utilities.convertMd5ToCode(albumMd5), ''].join(',');
		else if (mediaMd5)
			return ['', Utilities.convertMd5ToCode(mediaMd5)].join(',');
		else
			return "";
	};

	Utilities.prototype.convertProtectedDirectoryToCodesSimpleCombination = function(protectedDirectory) {
		var [albumMd5, mediaMd5] = protectedDirectory.substring(env.options.protected_directories_prefix.length).split(',');
		if (albumMd5 && mediaMd5)
			return [Utilities.convertMd5ToCode(albumMd5), Utilities.convertMd5ToCode(mediaMd5)].join(',');
		else if (albumMd5)
			return [Utilities.convertMd5ToCode(albumMd5), ''].join(',');
		else if (mediaMd5)
			return ['', Utilities.convertMd5ToCode(mediaMd5)].join(',');
		else
			return "";
	};

	Utilities._s = function(key) {
		return env.shortcuts[env.language][key];
	};

	Utilities._t = function(key) {
		var shortcut = "";
		if (! env.isAnyMobile) {
			shortcut = env.shortcuts[env.language][key + "-shortcut"];
			if (shortcut !== undefined)
				shortcut = " [" + shortcut + "]";
			else
				shortcut = "";
		}

		return env.translations[env.language][key] + shortcut;
	};

	Utilities.prototype.translate = function() {
		for (let key in env.translations.en) {
			if (env.translations[env.language].hasOwnProperty(key)) {
				if (key === '.title-string' && document.title.substr(0, 5) != "<?php" || key === "#started-slideshow-interval")
					// don't set page title, php has already set it
					continue;
				let keyObject = $(key);
				if (keyObject.length)
					keyObject.html(Utilities._t(key));
			}
		}
		for (let key in env.shortcuts.en) {
			if (env.shortcuts[env.language].hasOwnProperty(key)) {
				let keyObject = $(key);
				if (keyObject.length)
					keyObject.html(Utilities._s(key));
			}
		}
		$("#save-data").attr("title", Utilities._t("#save-data-tip"));
	};

	Utilities.prototype.windowVerticalScrollbarWidth = function() {
		// from https://davidwalsh.name/detect-scrollbar-width

		// Create the measurement node
		var scrollDiv = document.createElement("div");
		scrollDiv.className = "scrollbar-measure";
		document.body.appendChild(scrollDiv);

		// Get the scrollbar width
		var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;

		// Delete the DIV
		document.body.removeChild(scrollDiv);
		return scrollbarWidth;
	};

	Utilities.prototype.addBySomethingSubdir = function(cacheBase)	{
		var table = {};
		table[env.options.folders_string] = env.options.regular_album_subdir;
		table[env.options.by_date_string] = env.options.by_date_album_subdir;
		table[env.options.by_gps_string] = env.options.by_gps_album_subdir;
		table[env.options.by_search_string] = env.options.by_search_album_subdir;
		for (const string in table) {
			if (cacheBase.indexOf(string) === 0) {
				cacheBase = Utilities.pathJoin([table[string], cacheBase]);
				break;
			}
		}
		return cacheBase;
	};

	Utilities.prototype.cloneObject = function(object) {
		return Object.assign({}, object);
	};

	Utilities.prototype.arrayIntersect = function(a, b) {
		if (b.length > a.length) {
			// indexOf to loop over shorter
			[a, b] = [b, a];
		}

		let intersection = [];
		for (let i = 0; i < b.length; i ++) {
			let elementB = b[i];
			if (a.indexOf(elementB) !== -1)
				intersection.push(elementB);
		}

		return intersection;
	};

	Utilities.mediaOrSubalbumsIntersectionForSearches = function(a, b) {
		if (! a.length || ! b.length) {
			a.length = 0;
			return;
		}

		var property = 'albumName';
		if (! a[0].hasOwnProperty('albumName'))
			// searched albums hasn't albumName property
			property = 'path';

		b.forEach(
			function(elementB) {
				let found = false;
				let indexA;
				for (indexA = a.length - 1; indexA >= 0; indexA --) {
					let elementA = a[indexA];
					var aValue = elementA[property];
					var bValue = elementB[property];
					if (property === 'albumName') {
						aValue = Utilities.pathJoin([aValue, elementA.name]);
						bValue = Utilities.pathJoin([bValue, elementB.name]);
					}
					if (Utilities.normalizeAccordingToOptions(bValue) === Utilities.normalizeAccordingToOptions(aValue)) {
						found = true;
						break;
					}
				}
				if (! found)
					a.splice(indexA, 1);
			}
		);
	};


	Utilities.prototype.mediaOrSubalbumsUnionForSearches = function(a, b) {
		if (! a.length) {
			a.push(... b);
			return;
		}
		if (! b.length) {
			return;
		}

		var property = 'albumName';
		if (! a[0].hasOwnProperty('albumName'))
			// searched albums haven't albumName property
			property = 'path';

		for (var i = 0; i < b.length; i ++) {
			let elementB = b[i];
			if (! a.some(
				function (elementA) {
					var bValue = elementB[property];
					var aValue = elementA[property];
					if (property === 'albumName') {
						bValue = Utilities.pathJoin([bValue, elementB.name]);
						aValue = Utilities.pathJoin([aValue, elementA.name]);
					}
					return Utilities.normalizeAccordingToOptions(bValue) === Utilities.normalizeAccordingToOptions(aValue);
				})
			)
				a.push(elementB);
		}
	};

	Utilities.prototype.arrayUnion = function(a, b, equalityFunction = null) {
		if (! a.length)
			return b;
		if (! b.length)
			return a;
		// begin cloning the first array
		var union = a.slice(0);
		var i;

		if (equalityFunction === null) {
			for (i = 0; i < b.length; i ++) {
				let elementB = b[i];
				if (union.indexOf(elementB) === -1)
					union.push(elementB);
			}
		} else {
			for (i = 0; i < b.length; i ++) {
				let elementB = b[i];
				if (
					a.every(
						function notEqual(element) {
							return ! equalityFunction(element, elementB);
						}
					)
				)
					union.push(elementB);
			}
		}
		return union;
	};

	Utilities.normalizeAccordingToOptions = function(object) {
		var string = object;
		if (typeof object === "object")
			string = string.join('|');

		if (! env.options.search_case_sensitive)
			string = string.toLowerCase();
		if (! env.options.search_accent_sensitive)
			string = Utilities.removeAccents(string);

		if (typeof object === "object")
			object = string.split('|');
		else
			object = string;

		return object;
	};

	Utilities.prototype.encodeNonLetters = function(object) {
		var string = object;
		if (typeof object === "object")
			string = string.join('|');

		string = string.replace(/([^\p{L}_])/ug, match => match === "-" ? "%2D" : encodeURIComponent(match));

		if (typeof object === "object")
			object = string.split('|');
		else
			object = string;

		return object;
	};

	Utilities.removeAccents = function(string) {
		string = string.normalize('NFD');
		var stringArray = Array.from(string);
		var resultString = '';
		for (var i = 0; i < stringArray.length; i ++) {
			if (env.options.unicode_combining_marks.indexOf(stringArray[i]) === -1)
				resultString += stringArray[i];
		}
		return resultString;
	};

	Utilities.pathJoin = function(pathArr) {
		var result = '';
		for (var i = 0; i < pathArr.length; ++i) {
			if (i < pathArr.length - 1 && pathArr[i] && pathArr[i][pathArr[i].length - 1] != "/")
				pathArr[i] += '/';
			if (i && pathArr[i] && pathArr[i][0] === "/")
				pathArr[i] = pathArr[i].slice(1);
			result += pathArr[i];
		}
		return result;
	};

	// see https://stackoverflow.com/questions/1069666/sorting-javascript-object-by-property-value
	Utilities.prototype.sortBy = function(albumOrMediaList, fieldArray) {
		albumOrMediaList.sort(
			function(a,b) {
				var aValue, bValue, iField;
				for (iField = 0; iField < fieldArray.length; iField ++) {
					if (a.hasOwnProperty(fieldArray[iField])) {
						aValue = a[fieldArray[iField]].toLowerCase();
						break;
					}
				}
				for (iField = 0; iField < fieldArray.length; iField ++) {
					if (b.hasOwnProperty(fieldArray[iField])) {
						bValue = b[fieldArray[iField]].toLowerCase();
						break;
					}
				}
				return aValue < bValue ? -1 : aValue > bValue ? 1 : 0;
			}
		);
	};

	Utilities.prototype.sortByDate = function (subalbums) {
		subalbums.sort(
			function(a, b) {
				return a.date < b.date ? -1 : a.date > b.date ? 1 : 0;
			}
		);
	};

	Utilities.isAnyRootCacheBase = function(cacheBase) {
		var result =
			[env.options.folders_string, env.options.by_date_string, env.options.by_gps_string].indexOf(cacheBase) !== -1 ||
			Utilities.isSearchCacheBase(cacheBase) ||
			Utilities.isMapCacheBase(cacheBase) ||
			Utilities.isSelectionCacheBase(cacheBase);
		return result;
	};

	Utilities.prototype.trimExtension = function(name) {
		var index = name.lastIndexOf(".");
		if (index !== -1)
			return name.substring(0, index);
		return name;
	};

	Utilities.isPhp = function() {
		return typeof isPhp === "function";
	};

	Utilities.isFolderCacheBase = function(cacheBase) {
		return cacheBase === env.options.folders_string || cacheBase.indexOf(env.options.foldersStringWithTrailingSeparator) === 0;
	};

	Utilities.isByDateCacheBase = function(cacheBase) {
		return cacheBase === env.options.by_date_string || cacheBase.indexOf(env.options.byDateStringWithTrailingSeparator) === 0;
	};

	Utilities.isByGpsCacheBase = function(cacheBase) {
		return cacheBase === env.options.by_gps_string || cacheBase.indexOf(env.options.byGpsStringWithTrailingSeparator) === 0;
	};

	Utilities.isSearchCacheBase = function(cacheBase) {
		return cacheBase.indexOf(env.options.bySearchStringWithTrailingSeparator) === 0;
	};

	Utilities.isSelectionCacheBase = function(cacheBase) {
		return cacheBase.indexOf(env.options.bySelectionStringWithTrailingSeparator) === 0;
	};

	Utilities.prototype.isCollectionCacheBase = function(cacheBase) {
		return Utilities.isMapCacheBase(cacheBase) || Utilities.isSearchCacheBase(cacheBase) || Utilities.isSelectionCacheBase(cacheBase);
	};

	Utilities.isMapCacheBase = function(cacheBase) {
		return cacheBase.indexOf(env.options.byMapStringWithTrailingSeparator) === 0;
	};

	Utilities.isSearchHash = function() {
		var cacheBase = PhotoFloat.convertHashToCacheBase(location.hash);
		if (Utilities.isSearchCacheBase(cacheBase) || env.collectionCacheBase !== null)
			return true;
		else
			return false;
	};

	Utilities.prototype.noResults = function(album, resolveParseHash, selector) {
		// no media found or other search fail, show the message
		env.currentAlbum = album;
		TopFunctions.setTitle("album", null);
		$("#subalbums, #thumbs, #media-view").addClass("hidden-by-no-results");
		$("#loading").hide();
		if (typeof selector === "undefined")
			selector = '#no-results';
		$(".search-failed").hide();
		$(selector).stop().fadeIn(
			1000,
			function() {
				resolveParseHash([album, -1]);
			}
		);
	};

	Utilities.prototype.stripHtmlAndReplaceEntities = function(htmlString) {
		// converto for for html page title
		// strip html (https://stackoverflow.com/questions/822452/strip-html-from-text-javascript#822464)
		// and replaces &raquo; with \u00bb
		return htmlString.replace(/<(?:.|\n)*?>/gm, '').replace(/&raquo;/g, '\u00bb');
	};

	Utilities.transformAltPlaceName = function(altPlaceName) {
		var underscoreIndex = altPlaceName.lastIndexOf('_');
		if (underscoreIndex != -1) {
			var number = altPlaceName.substring(underscoreIndex + 1);
			var base = altPlaceName.substring(0, underscoreIndex);
			return base + ' (' + Utilities._t('.subalbum') + parseInt(number) + ')';
		} else {
			return altPlaceName;
		}
	};

	Utilities.albumButtonWidth = function(thumbnailWidth) {
		if (env.options.albums_slide_style) {
			return Math.ceil(thumbnailWidth * (1 + 2 * env.slideMarginFactor) + 2 * env.slideAlbumButtonBorder + 2 * env.slideBorder);
		} else {
			return Math.ceil(thumbnailWidth + 1 * env.options.spacing);
		}
	};

	Utilities.thumbnailWidth = function(albumButtonWidth) {
		if (env.options.albums_slide_style) {
			return Math.floor((albumButtonWidth - 2 * env.slideAlbumButtonBorder - 2 * env.slideBorder) / (1 + 2 * env.slideMarginFactor));
		} else {
			return Math.floor(albumButtonWidth - 1 * env.options.spacing);
		}
	};

	Utilities.removeFolderString = function (cacheBase) {
		if (this.isFolderCacheBase(cacheBase)) {
			cacheBase = cacheBase.substring(env.options.folders_string.length);
			if (cacheBase.length > 0)
				cacheBase = cacheBase.substring(1);
		}
		return cacheBase;
	};

	Utilities.somethingIsInMapAlbum = function() {
		if (env.mapAlbum.hasOwnProperty("numsMediaInSubTree") && env.mapAlbum.numsMediaInSubTree.imagesAndVideosTotal())
			return true;
		else
			return false;
	};

	Utilities.somethingIsSearched = function() {
		if (
			env.searchAlbum.hasOwnProperty("numsMediaInSubTree") && env.searchAlbum.numsMediaInSubTree.imagesAndVideosTotal() ||
			env.searchAlbum.hasOwnProperty("subalbums") && env.searchAlbum.subalbums.length
		)
			return true;
		else
			return false;
	};

	Utilities.nothingIsSelected = function() {
		if (env.selectionAlbum.isEmpty()) {
			return true;
		} else {
			if (env.selectionAlbum.media.length || env.selectionAlbum.subalbums.length)
				return false;
			else
				return true;
		}
	};

	Utilities.somethingIsSelected = function() {
		return ! Utilities.nothingIsSelected();
	};

	Utilities.prototype.albumIsSelected = function(album) {
		if (env.selectionAlbum.isEmpty()) {
			return false;
		} else {
			var index = env.selectionAlbum.subalbums.findIndex(x => x.isEqual(album));
			if (index > -1)
				return true;
			else
				return false;
		}
	};

	Utilities.isShiftOrControl = function() {
		return $(".shift-or-control").length ? true : false;
	};

	Utilities.isPopup = function() {
		return $(".media-popup.leaflet-popup").html() ? true : false;
	};

	Utilities.isMap = function() {
		return ($('#mapdiv').html() ? true : false) && ! Utilities.isPopup();
	};

	Utilities.setTitleOptions = function() {
		if (env.options.hide_title) {
			$(".title").addClass("hidden-by-option");
		} else {
			$(".title").removeClass("hidden-by-option");
			let newWidth = env.windowWidth;
			if (env.windowWidth > 700)
				newWidth -= parseInt($("#right-and-search-menu").css("width")) + 10;
				// avoid that the title remain below right buttons
			$(".title").css("width", newWidth);
		}

		if (! env.options.show_album_media_count)
			$(".title-count").addClass("hidden-by-option");
		else
			$(".title-count").removeClass("hidden-by-option");

		$(".title").css("font-size", env.options.title_font_size);
		$(".title-anchor").css("color", env.options.title_color);
		$(".title-anchor").hover(function() {
			//mouse over
			$(this).css("color", env.options.title_color_hover);
		}, function() {
			//mouse out
			$(this).css("color", env.options.title_color);
		});
	};

	Utilities.prototype.changeToBySelectionView = function(ev, thisMedia = null) {
		TopFunctions.showBrowsingModeMessage(ev, "#by-selection-browsing");
		var isShiftOrControl = Utilities.isShiftOrControl();
		var isPopup = Utilities.isPopup();
		var isMap = ($('#mapdiv').html() ? true : false) && ! isPopup;
		if (isShiftOrControl) {
			// close the shift/control buttons
			$(".shift-or-control .leaflet-popup-close-button")[0].click();
		}
		if (isPopup) {
			// the popup is there: close it
			env.highlightedObjectId = null;
			$("media-popup .leaflet-popup-close-button")[0].click();
		}
		if (isMap || isPopup) {
			// we are in a map: close it
			$('.modal-close')[0].click();
		}

		if (thisMedia) {
			$(".title").removeClass("hidden-by-pinch");
			$("#album-and-media-container.show-media #thumbs").removeClass("hidden-by-pinch");
			window.location.href = PhotoFloat.encodeHash(env.selectionAlbum.cacheBase, thisMedia);
		} else {
			window.location.href = PhotoFloat.encodeHash(env.selectionAlbum.cacheBase, null);
		}
		return false;
	};

	Utilities.setMediaOptions = function() {
		$(".media-name").css("color", env.options.media_name_color);

		if (! env.options.show_media_names_below_thumbs)
			$(".thumb-and-caption-container .media-name").addClass("hidden-by-option");
		else
			$(".thumb-and-caption-container .media-name").removeClass("hidden-by-option");

		$(".thumb-and-caption-container").css("margin-right", env.options.spacing.toString() + "px");
		if ($("#album-and-media-container").hasClass("show-media"))
			$(".thumb-and-caption-container").css("margin-bottom", "0");
		else
			$(".thumb-and-caption-container").css("margin-bottom", env.options.spacing.toString() + "px");

		if (env.options.hide_descriptions)
			$(".media-description").addClass("hidden-by-option");
		else
			$(".media-description").removeClass("hidden-by-option");

		if (env.options.hide_tags)
			$(".media-tags").addClass("hidden-by-option");
		else
			$(".media-tags").removeClass("hidden-by-option");

		$("#album-and-media-container #thumbs").removeClass("hidden-by-option");
		if (env.options.hide_bottom_thumbnails) {
			$("#album-and-media-container.show-media #thumbs").addClass("hidden-by-option");
		}
	};

	Utilities.prototype.setSubalbumsOptions = function() {
		let scrollBarWidth = window.innerWidth - document.body.clientWidth || 15;

		// resize down the album buttons if they are too wide
		let albumViewWidth =
			$("body").width() -
			parseInt($("#album-view").css("padding-left")) -
			parseInt($("#album-view").css("padding-right")) -
			scrollBarWidth;
		env.correctedAlbumThumbSize = env.options.album_thumb_size;
		let correctedAlbumButtonSize = Utilities.albumButtonWidth(env.options.album_thumb_size);
		if (albumViewWidth / (correctedAlbumButtonSize + env.options.spacing) < env.options.min_album_thumbnail) {
			env.correctedAlbumThumbSize = Math.floor(Utilities.thumbnailWidth(albumViewWidth / env.options.min_album_thumbnail - env.options.spacing)) - 1;
			correctedAlbumButtonSize = Utilities.albumButtonWidth(env.correctedAlbumThumbSize);
		}
		let captionFontSize = Math.round(Utilities.em2px("body", 1) * env.correctedAlbumThumbSize / env.options.album_thumb_size);
		let captionHeight = parseInt(captionFontSize * 1.1) + 1;
		let margin = 0;
		if (env.options.albums_slide_style)
			margin = Math.round(env.correctedAlbumThumbSize * env.slideMarginFactor);

		let buttonAndCaptionHeight = correctedAlbumButtonSize + captionHeight;

		let slideBorder = 0;
		if (env.options.albums_slide_style)
			slideBorder = env.slideBorder;

		if (env.currentAlbum.isFolder() && ! env.options.show_album_names_below_thumbs)
			$(".album-name").addClass("hidden-by-option");
		else
			$(".album-name").removeClass("hidden-by-option");

		if (env.options.albums_slide_style) {
			$(".album-name").css("color", env.options.album_slide_name_color);
			$(".album-button-and-caption").css("background-color", env.options.album_slide_background_color);
			$(".album-button").css("background-color", env.options.album_slide_background_color);
			$(".album-caption, .album-caption .real-name").css("color", env.options.album_slide_caption_color);
			$(".album-button-and-caption").addClass("slide");
		} else {
			$(".album-name").css("color", env.options.album_name_color);
			$(".album-button-and-caption").css("background-color", "");
			$(".album-button").css("background-color", "");
			$(".album-caption, .album-caption .real-name").css("color", env.options.album_caption_color);
			$(".album-button").css("border", "none");
			$(".album-button-and-caption").removeClass("slide");
		}

		let marginBottom = env.options.spacing;
		if (! env.options.albums_slide_style)
			marginBottom += Utilities.em2px("body", 2);
		$(".album-button-and-caption").css("width", (correctedAlbumButtonSize - 2 * slideBorder) + "px");
		$(".album-button-and-caption").css("height", buttonAndCaptionHeight + "px");
		$(".album-button-and-caption").css("margin-right", env.options.spacing.toString() + "px");
		$(".album-button-and-caption").css("margin-bottom", marginBottom.toString() + "px");

		$(".album-button").css("margin", margin + "px");
		$(".album-button").css("width", env.correctedAlbumThumbSize + "px");
		$(".album-button").css("height", env.correctedAlbumThumbSize + "px");

		$(".album-caption").css("width", env.correctedAlbumThumbSize + "px");
		$(".album-caption").css("height", captionHeight + "px");
		$(".album-caption").css("font-size", captionFontSize + "px");

		$(".album-tags").css("font-size", (Math.round(captionFontSize * 0.75)) + "px");

		if (
			$("#subalbums .album-button img.thumbnail").length &&
			$("#subalbums").is(":visible")
		) {
			let firstThumbnail = $("#subalbums .album-button img.thumbnail").first();
			if (firstThumbnail.attr("src") !== "img/image-placeholder.jpg") {
				var mustBeSquare = (env.options.album_thumb_type.indexOf("square") > -1);
				var isSquare = (firstThumbnail.attr("src").substr(- env.options.format.length - 2, 1) === "s");
				if (isSquare !== mustBeSquare) {
					$("#subalbums .album-button img.thumbnail").each(
						function() {
							var attribute;
							if (["af." + env.options.format, "as." + env.options.format].indexOf($(this).attr("src").substr(-3 - env.options.format.length)) !== -1)
								attribute = "src";
							else
								// this value is for thumbnails not processed by LazyLoad yet
								attribute = "data-src";

							var srcArray = $(this).attr(attribute).split("");
							var charPosition = srcArray.length - env.options.format.length - 2;
							srcArray[charPosition] = srcArray[charPosition] === "s" ? "f" : "s";
							$(this).attr(attribute, srcArray.join(""));
						}
					);
				}
			}
		}


		if (! env.options.show_album_media_count)
			$(".album-caption-count").addClass("hidden-by-option");
		else
			$(".album-caption-count").removeClass("hidden-by-option");


		if (env.options.hide_descriptions)
			$(".album-description").addClass("hidden-by-option");
		else
			$(".album-description").removeClass("hidden-by-option");

		if (env.options.hide_tags)
			$(".album-tags").addClass("hidden-by-option");
		else
			$(".album-tags").removeClass("hidden-by-option");
	};

	Utilities.addTagLink = function(tag) {
		// tags can be phrases (e.g. with automatic tags from person recognition)

		// now replace space -> underscore
		var tagForHref = tag.replace(/ /g, "_");
		// all non-letter character must be converted to space
		tagForHref = tagForHref.replace(/([^\p{L}_])/ug, match => match === "-" ? "%2D" : encodeURIComponent(match));

		var hash = env.hashBeginning + env.options.by_search_string + env.options.cache_folder_separator +  "t" + env.options.search_options_separator + "o" + env.options.search_options_separator + tagForHref + env.options.cache_folder_separator + env.currentAlbum.cacheBase;
		return "<a href='" + hash + "'>" + tag + "</a>";
	};

	Utilities.em2px = function(selector, em) {
		var emSize = parseFloat($(selector).css("font-size"));
		return (em * emSize);
	};

	Utilities.currentSizeAndIndex = function() {
		// Returns the pixel size of the photo in DOM and the corresponding reduction index
		// If the original photo is in the DOM, returns its size and -1

		var currentReduction = $(".media-box#center .media-box-inner img").attr("src");

		// check if it's a reduction
		for (var i = 0; i < env.options.reduced_sizes.length; i ++) {
			if (currentReduction === env.currentMedia.mediaPath(env.options.reduced_sizes[i])) {
				return [env.options.reduced_sizes[i], i];
			}
		}

		// default: it's the original image
		return [Math.max(env.currentMedia.metadata.size[0], env.currentMedia.metadata.size[1]), -1];
	};

	Utilities.nextSizeAndIndex = function() {
		// Returns the size of the reduction immediately bigger than that in the DOM and its reduction index
		// Returns the original photo size and -1 if the reduction in the DOM is the biggest one
		// Returns [false, false] if the original image is already in the DOM

		var [fake_currentReductionSize, currentReductionIndex] = Utilities.currentSizeAndIndex();
		if (currentReductionIndex === -1)
			return [false, false];

		if (currentReductionIndex === 0)
			return [Math.max(env.currentMedia.metadata.size[0], env.currentMedia.metadata.size[1]), -1];

		return [env.options.reduced_sizes[currentReductionIndex - 1], currentReductionIndex - 1];
	};

	Utilities.prototype.nextReduction = function() {
		// Returns the file name of the reduction with the next bigger size than the reduction in DOM,
		// possibly the original photo
		// Returns false if the original photo is already in the DOM

		var [nextReductionSize, nextReductionIndex] = Utilities.nextSizeAndIndex();

		if (nextReductionIndex === false)
			// it's already the original image
			return false;
		if (nextReductionIndex === -1)
			return Utilities.pathJoin([env.currentMedia.albumName, env.currentMedia.name]);

		return env.currentMedia.mediaPath(nextReductionSize);
	};

	Utilities.prototype.mediaBoxContainerHeight = function() {
		var heightForMediaAndTitle;
		// env.windowHeight = $(window).innerHeight();
		heightForMediaAndTitle = env.windowHeight;
		if ($("#album-view").is(":visible"))
			heightForMediaAndTitle -= $("#album-view")[0].offsetHeight;

		return heightForMediaAndTitle;
	};

	Utilities.setFinalOptions = function(self, initialize) {
		Utilities.setSocialButtons();
		Utilities.setTitleOptions();
		Utilities.setMediaOptions();
		if (self !== null) {
			Utilities.setPrevNextVisibility();
			Utilities.setPrevNextPosition();
			if (self.isImage()) {
				if (initialize)
					PinchSwipe.initializePincheSwipe();
				Utilities.setPinchButtonsPosition();
				Utilities.setPinchButtonsVisibility();
			}
			Utilities.setSelectButtonPosition();
			Utilities.setSlideshowButtonsPosition();
		}
		Utilities.setDescriptionOptions();
		Utilities.correctElementPositions();
	};

	Utilities.prototype.resizeSingleMediaWithPrevAndNext = function(self, album) {
		var previousWindowWidth = env.windowWidth;
		var previousWindowHeight = env.windowHeight;
		env.windowWidth = $(window).innerWidth();
		env.windowHeight = $(window).innerHeight();
		if (env.windowWidth === previousWindowWidth && env.windowHeight === previousWindowHeight && ! env.isFullScreenToggling)
			// avoid considering a resize when the mobile browser shows/hides the location bar
			return;

		env.originalMediaBoxContainerContent =
			$(env.originalMediaBoxContainerContent).css("width", env.windowWidth)[0].outerHTML;

		env.isFullScreenToggling = false;

		$("#loading").show();
		Utilities.setTitleOptions();

		if (album.media.length && ! $("#thumbs").children().length)
			album.showMedia();

		var event = {data: {}};
		event.data.resize = true;
		event.data.id = "center";

		// prev and next tree in the DOM must be given the correct sizes
		$(".media-box#left, .media-box#right").css("width", env.windowWidth);
		$(".media-box#left, .media-box#right").css("height", env.windowHeight);

		var scalePromise = self.scale(event);
		scalePromise.then(
			function() {
				Utilities.setFinalOptions(self, true);
			}
		);

		var geotaggedContentIsHidden = Utilities.geotaggedContentIsHidden();
		if (
			geotaggedContentIsHidden && album.nonGeotagged.numsMedia.imagesAndVideosTotal() > 1 ||
			! geotaggedContentIsHidden && album.numsMedia.imagesAndVideosTotal() > 1
		) {
			event.data.id = "left";
			env.prevMedia.scale(event);

			event.data.id = "right";
			env.nextMedia.scale(event);
		}

		if (Utilities.isMap() || Utilities.isPopup()) {
			// the map must be generated again including the points that only carry protected content
			env.mapRefreshType = "resize";

			if (Utilities.isPopup()) {
				env.popupRefreshType = "mapAlbum";
				env.highlightedObjectId = null;
				if (Utilities.isShiftOrControl())
					$(".shift-or-control .leaflet-popup-close-button")[0].click();
				$(".media-popup .leaflet-popup-close-button")[0].click();
			} else {
				env.popupRefreshType = "none";
			}

			// close the map and reopen it
			$('.modal-close')[0].click();
			$(env.selectorClickedToOpenTheMap).trigger("click", ["fromTrigger"]);
		}

		MenuFunctions.updateMenu();
	};

	Utilities.prototype.roundDecimals = function (float, decimalPlaces = env.zoomDecimalPlaces) {
		if (Math.abs(float) < Number("10e" + (-decimalPlaces)))
			return 0;
		else
			return Number(Math.round(float + "e" + decimalPlaces) + "e" + (- decimalPlaces));
	};

	Utilities.prototype.isLoaded = function(imgSrc) {
		return env.loadedImages.indexOf(imgSrc) !== -1;
	};

	Utilities.prototype.setLoaded = function(imgSrc) {
		env.loadedImages.push(imgSrc);
	};

	Utilities.geotaggedContentIsHidden = function() {
		return env.options.expose_image_positions && $("#fullscreen-wrapper").hasClass("hide-geotagged");
	};

	Utilities.prototype.nextObjectForHighlighting = function(object) {
		var isPopup = Utilities.isPopup();
		var nextObject;

		if (isPopup) {
			nextObject = object.next();
			if (! nextObject.length)
				 nextObject = object.parent().children().first();
		} else {
			let numVisibleSubalbums = env.currentAlbum.subalbums.length;
			let numVisibleMedia = env.currentAlbum.media.length;
			let filter = "*";
			let mediaFilter = "*";
			let subalbumsFilter = "*";
			if (Utilities.geotaggedContentIsHidden()) {
				numVisibleSubalbums = $("#subalbums > a:not(.all-gps)").length;
				numVisibleMedia = $("#thumbs > a:not(.gps)").length;
				mediaFilter = ":not(.gps)";
				subalbumsFilter = ":not(.all-gps)";
				filter = mediaFilter;
				if (Utilities.objectIsASubalbum(object))
					filter = subalbumsFilter;
			}
			let nextObjectParent = object.parent().nextAll(filter).first();
			if (nextObjectParent.length) {
				nextObject = nextObjectParent.children();
			} else {
				if (
					Utilities.objectIsASingleMedia(object) && numVisibleMedia === 1 && ! numVisibleSubalbums ||
					Utilities.objectIsASubalbum(object) && numVisibleSubalbums === 1 && ! numVisibleMedia
				)
					return object;
				else if (Utilities.objectIsASingleMedia(object) && numVisibleSubalbums)
					nextObject = $(".album-button-and-caption").parent().filter(subalbumsFilter).first().children();
				else if (Utilities.objectIsASingleMedia(object) && ! numVisibleSubalbums)
					nextObject = $(".thumb-and-caption-container").parent().prevAll(mediaFilter).last().children();
				else if (Utilities.objectIsASubalbum(object) && numVisibleMedia)
					nextObject = $(".thumb-and-caption-container").parent().filter(mediaFilter).first().children();
				else if (Utilities.objectIsASubalbum(object) && ! numVisibleMedia)
					nextObject = $(".album-button-and-caption").parent().prevAll(subalbumsFilter).last().children();
			}
		}

		return nextObject;
	};

	Utilities.prototype.prevObjectForHighlighting = function(object) {
		var isPopup = Utilities.isPopup();
		var prevObject;

		if (isPopup) {
			prevObject = object.prev();
			if (! prevObject.length)
				 prevObject = object.parent().children().last();
		} else {
			let numVisibleSubalbums = env.currentAlbum.subalbums.length;
			let numVisibleMedia = env.currentAlbum.media.length;
			let filter = "*";
			let mediaFilter = "*";
			let subalbumsFilter = "*";
			if (Utilities.geotaggedContentIsHidden()) {
				numVisibleSubalbums = $("#subalbums > a:not(.all-gps)").length;
				numVisibleMedia = $("#thumbs > a:not(.gps)").length;
				mediaFilter = ":not(.gps)";
				subalbumsFilter = ":not(.all-gps)";
				filter = mediaFilter;
				if (Utilities.objectIsASubalbum(object))
					filter = subalbumsFilter;
			}
			let prevObjectParent = object.parent().prevAll(filter).first();
			if (prevObjectParent.length) {
				prevObject = prevObjectParent.children();
			} else {
				if (
					Utilities.objectIsASingleMedia(object) && numVisibleMedia === 1 && ! numVisibleSubalbums ||
					Utilities.objectIsASubalbum(object) && numVisibleSubalbums === 1 && ! numVisibleMedia
				)
					return object;
				else if (Utilities.objectIsASingleMedia(object) && numVisibleSubalbums)
					prevObject = $(".album-button-and-caption").parent().filter(subalbumsFilter).last().children();
				else if (Utilities.objectIsASingleMedia(object) && ! numVisibleSubalbums)
					prevObject = $(".thumb-and-caption-container").parent().nextAll(mediaFilter).last().children();
				else if (Utilities.objectIsASubalbum(object) && numVisibleMedia)
					prevObject = $(".thumb-and-caption-container").parent().filter(mediaFilter).last().children();
				else if (Utilities.objectIsASubalbum(object) && ! numVisibleMedia)
					prevObject = $(".album-button-and-caption").parent().nextAll(subalbumsFilter).last().children();
			}
		}

		return prevObject;
	};

	Utilities.prototype.selectBoxObject = function(object) {
		var isPopup = Utilities.isPopup();
		var selector = "", selectBoxObject;
		if (isPopup)
			selector = "#popup-images-wrapper ";
		if (Utilities.objectIsASubalbum(object) || isPopup) {
			selector += "#" + object.attr("id");
		} else {
			selector += "#" + object.parent().attr("id");
		}
		selectBoxObject = $(selector + " .select-box");
		return selectBoxObject;
	};

	Utilities.prototype.addClickToHiddenGeotaggedMediaPhrase = function() {
		// if ($(".hidden-geotagged-media").is(":visible")) {
		$(".hidden-geotagged-media").off("click").on(
			"click",
			function() {
				$("#hide-geotagged-media").click();
			}
		);
		// }
	};

	Utilities.removeHighligths = function() {
		if (Utilities.isPopup()) {
			$("#popup-images-wrapper .highlighted-object").removeClass("highlighted-object");
		} else {
			$("#thumbs .highlighted-object").removeClass("highlighted-object");
			$("#subalbums .highlighted-object").removeClass("highlighted-object");
		}
	};

	Utilities.prototype.addHighlightToMediaOrSubalbum = function(object, remove = true) {
		if (remove)
			Utilities.removeHighligths();
		object.addClass("highlighted-object");
	};

	Utilities.prototype.highlightedObject = function(inThumbs = false) {
		if (Utilities.isPopup() && ! inThumbs) {
			return $("#popup-images-wrapper .highlighted-object");
		} else {
			return $("#album-and-media-container .highlighted-object");
		}
	};

	Utilities.objectIsASubalbum = function(object) {
		return object.hasClass("album-button-and-caption");
	};

	Utilities.objectIsASingleMedia = function(object) {
		return object.hasClass("thumb-and-caption-container");
	};

	Utilities.prototype.aSubalbumIsHighlighted = function() {
		return $("#subalbums .highlighted-object").length > 0;
	};

	Utilities.prototype.aSingleMediaIsHighlighted = function() {
		return $("#thumbs .highlighted-object").length > 0;
	};

	Utilities.prototype.scrollAlbumViewToHighlightedSubalbum = function(object) {
		var numVisibleSubalbums = env.currentAlbum.subalbums.length;
		var filter = "*";
		if (Utilities.geotaggedContentIsHidden()) {
			numVisibleSubalbums = $("#subalbums > a:not(.all-gps)").length;
			filter = ":not(.all-gps)";
		}

		if (! Utilities.isPopup() && $("#subalbums").is(":visible") && numVisibleSubalbums) {
			if (object !== undefined && object.length) {
				$(window).scrollTop(object.offset().top + object.height() / 2 - env.windowHeight / 2);
				// // the following instruction is needed to activate the lazy loader
				// $(window).scroll();
			}
		}
	};

	Utilities.prototype.scrollAlbumViewToHighlightedThumb = function(object) {
		var numVisibleMedia = env.currentAlbum.subalbums.length;
		var filter = "*";
		if (Utilities.geotaggedContentIsHidden()) {
			numVisibleMedia = $("#thumbs > a:not(.gps)").length;
			filter = ":not(.gps)";
		}
		if ($("#thumbs").is(":visible")) {
			let thumbObject = object.children(".thumb-container").children(".thumbnail");

			if (thumbObject.length) {
				let offset;
				offset = thumbObject.offset().top - $("#album-view").offset().top + thumbObject.height() / 2 - env.windowHeight / 2;
				if (offset < 0)
					offset = 0;

				let scrollableObject = $(window);
				scrollableObject.scrollTop(offset);
				// // the following instruction is needed to activate the lazy loader
				// scrollableObject.scroll();
			}
		}
	};

	Utilities.prototype.scrollPopupToHighlightedThumb = function(object) {
		var scrollableObject = $("#popup-images-wrapper");
		var thumbObject = object.children(".thumb-container").children(".thumbnail");
		var popupHeight = scrollableObject.height();

		var offset = scrollableObject.scrollTop() + thumbObject.offset().top - scrollableObject.offset().top + thumbObject.height() / 2 - popupHeight / 2;
		if (offset < 0)
			offset = 0;
		scrollableObject.scrollTop(offset);
		// // the following instruction is needed to activate the lazy loader
		// scrollableObject.scroll();
	};

	Utilities.prototype.scrollBottomMediaToHighlightedThumb = function() {
		if (! Utilities.isPopup() && $("#thumbs").is(":visible") && env.currentMedia !== null) {
			let thumbObject = $("#" + env.currentMedia.foldersCacheBase + "--" + env.currentMedia.cacheBase);
			if (thumbObject[0] !== undefined && ! env.currentAlbumIsAlbumWithOneMedia) {
				let scroller = $("#album-view");
				scroller.scrollLeft(thumbObject.parent().position().left + scroller.scrollLeft() - scroller.width() / 2 + thumbObject.width() / 2);
				$(".thumb-container").removeClass("current-thumb");
				thumbObject.parent().addClass("current-thumb");
				Utilities.addMediaLazyLoader();
			}
		}
	};

	Utilities.prototype.checkAlbumWithOneMedia = function() {
		if (env.currentAlbum !== null)
			env.currentAlbumIsAlbumWithOneMedia = env.currentAlbum.isAlbumWithOneMedia();
		else
			env.currentAlbumIsAlbumWithOneMedia = false;

		if (env.previousAlbum !== null)
			env.previousAlbumIsAlbumWithOneMedia = env.previousAlbum.isAlbumWithOneMedia();
		else
			env.previousAlbumIsAlbumWithOneMedia = false;

		if (env.currentAlbumIsAlbumWithOneMedia) {
			if (Utilities.geotaggedContentIsHidden()) {
				env.currentMedia = env.currentAlbum.media.filter(ithMedia => ! ithMedia.hasGpsData())[0];
				env.currentMediaIndex = env.currentAlbum.media.findIndex(singleMedia => singleMedia.isEqual(env.currentMedia));
			} else {
				env.currentMedia = env.currentAlbum.media[0];
				env.currentMediaIndex = 0;
			}
			env.nextMedia = null;
			env.prevMedia = null;
		}
	};

	Utilities.prototype.showHideSlideshowIcon = function() {
		// manage slideshow
		var geotaggedContentIsHidden = Utilities.geotaggedContentIsHidden();
		var tooBig =
			env.currentAlbum.isTransversal() &&
			env.currentAlbum.path.split("/").length < 4 && (
				! geotaggedContentIsHidden &&
				env.currentAlbum.nonGeotagged.numsMedia.imagesAndVideosTotal() > env.options.big_virtual_folders_threshold ||
				geotaggedContentIsHidden &&
				env.currentAlbum.numsMedia.imagesAndVideosTotal() > env.options.big_virtual_folders_threshold
			) &&
			! env.options.show_big_virtual_folders;
		if (
			Utilities.isMap() || Utilities.isPopup() ||
			env.currentMedia !== null && env.currentAlbumIsAlbumWithOneMedia ||
			env.currentMedia === null && (
			geotaggedContentIsHidden && ! env.currentAlbum.nonGeotagged.numsMedia.imagesAndVideosTotal() ||
			! geotaggedContentIsHidden && ! env.currentAlbum.numsMedia.imagesAndVideosTotal()
			) ||
			tooBig
		) {
			$("#slideshow-icon").hide();
			$("#slideshow-icon").off("click");
		} else {
			$("#slideshow-icon").show();
			$("#slideshow-icon").off("click").on(
				"click",
				function(ev) {
					ev.stopPropagation();
					env.currentAlbum.slideshow(ev);
				}
			);
		}
	};

	Utilities.setSocialCopyButtonSize = function() {
		var social = env.options.social.replace(/"/g, "").replace(/,/g, " ").replace(/  /g, " ").trim().split(" ");
		if (Utilities.bottomSocialButtons()) {
			$(".ssk-show-url img").css("height", $(".ssk-" + social[0]).css("height"));
		} else {
			$(".ssk-show-url img").css("width", $(".ssk-" + social[0]).css("width"));
		}
		$(".ssk-show-url img")
			.attr("alt", Utilities._t("#tiny-url-icon-alt-text"))
			.attr("title", Utilities._t("#tiny-url-icon-title"));
	};

	Utilities.setSocialButtons = function() {
		function getReducedSizeIndex(optimalSize) {
			let absoluteDifferences = env.options.reduced_sizes.map(size => Math.abs(size - optimalSize));
			let minimumDifference = Math.min(... absoluteDifferences);
			reducedSizesIndex = absoluteDifferences.findIndex(size => size === minimumDifference);
			return reducedSizesIndex;
		}

		if (env.currentAlbum.isVirtual() || Utilities.isMap() || Utilities.isPopup()) {
			$("#social").hide();
			return;
		} else {
			$("#social").show();
		}
		Utilities.setSocialCopyButtonSize();

		var hash, myShareUrl = "";
		var mediaParameter, mediaWidth, mediaHeight, whatsappMediaWidth, whatsappMediaHeight;
		var widthParameter, heightParameter, whatsappWidthParameter, whatsappHeightParameter;
		var myShareText;

		if (false && ! env.isAnyMobile) {
			$(".ssk-whatsapp").hide();
		} else {
			// with touchscreens luminosity on hover cannot be used
			$(".album-button-and-caption").css("opacity", 1);
			$(".thumb-container").css("opacity", 1);
			$(".album-button-random-media-link").css("opacity", 1);
		}

		var urlWithoutHash = location.href.split("#")[0];

		// image size for sharing must be > 200 and ~ 1200x630, https://kaydee.net/blog/open-graph-image/
		// but whatsapp doesn't show some image with size 1200 => reduce
		var reducedSizesIndex, whatsappReducedSizesIndex;
		if (! env.options.reduced_sizes.length || Math.max(... env.options.reduced_sizes) < 200)
			// false means original image: it won't be used
			reducedSizesIndex = false;
		else {
			// use the size nearest to optimal
			reducedSizesIndex = getReducedSizeIndex(1200);
			whatsappReducedSizesIndex = getReducedSizeIndex(800);
		}

		var logo = "img/myphotoshareLogo.jpg";
		var logoSize = 1024;
		var whatsAppMediaParameter = "";

		if (
			env.currentMedia === null ||
			reducedSizesIndex === false
		) {
			// use the album composite image, if it exists; otherwise, use MyPhotoShare logo
			if (env.currentAlbum.hasOwnProperty("compositeImageSize")) {
				mediaParameter = Utilities.pathJoin([
					"cache",
					env.options.cache_album_subdir,
					// always use jpg image for sharing
					env.currentAlbum.cacheBase + ".jpg"
					]);
				widthParameter = env.currentAlbum.compositeImageSize;
				heightParameter = env.currentAlbum.compositeImageSize;
			} else {
				mediaParameter = logo;
				widthParameter = logoSize;
				heightParameter = logoSize;
			}
		} else {
			// current media !== null
			if (env.currentMedia.hasOwnProperty("protected")) {
				mediaParameter = logo;
				widthParameter = logoSize;
				heightParameter = logoSize;
			} else {
				let prefix = Utilities.removeFolderString(env.currentMedia.foldersCacheBase);
				if (prefix)
					prefix += env.options.cache_folder_separator;

				if (env.currentMedia && env.currentMedia.isVideo()) {
					mediaParameter = Utilities.pathJoin([
						"cache",
						env.currentMedia.cacheSubdir,
						// always use jpg image for sharing
						prefix + env.currentMedia.cacheBase + env.options.cache_folder_separator + env.currentMedia.imageSize + ".jpg"
					]);
					widthParameter = env.currentMedia.metadata.size[0];
					heightParameter = env.currentMedia.metadata.size[1];
				} else if (env.currentMedia && env.currentMedia.isImage()) {
					mediaWidth = env.currentMedia.metadata.size[0];
					mediaHeight = env.currentMedia.metadata.size[1];
					whatsappMediaWidth = mediaWidth;
					whatsappMediaHeight = mediaHeight;

					mediaParameter = Utilities.pathJoin([
						"cache",
						env.currentMedia.cacheSubdir,
						// always use jpg image for sharing
						prefix + env.currentMedia.cacheBase + env.options.cache_folder_separator + env.options.reduced_sizes[reducedSizesIndex] + ".jpg"
					]);
					whatsAppMediaParameter = Utilities.pathJoin([
						"cache",
						env.currentMedia.cacheSubdir,
						// always use jpg image for sharing
						prefix + env.currentMedia.cacheBase + env.options.cache_folder_separator + env.options.reduced_sizes[whatsappReducedSizesIndex] + ".jpg"
					]);

					if (mediaWidth > mediaHeight) {
						widthParameter = env.options.reduced_sizes[reducedSizesIndex];
						heightParameter = Math.round(env.options.reduced_sizes[reducedSizesIndex] * mediaHeight / mediaWidth);
						whatsappWidthParameter = env.options.reduced_sizes[whatsappReducedSizesIndex];
						whatsappHeightParameter = Math.round(env.options.reduced_sizes[whatsappReducedSizesIndex] * mediaHeight / mediaWidth);
					} else {
						heightParameter = env.options.reduced_sizes[reducedSizesIndex];
						widthParameter = Math.round(env.options.reduced_sizes[reducedSizesIndex] * mediaWidth / mediaHeight);
						whatsappWidthParameter = env.options.reduced_sizes[whatsappReducedSizesIndex];
						whatsappHeightParameter = Math.round(env.options.reduced_sizes[whatsappReducedSizesIndex] * mediaWidth / mediaHeight);
					}
				}
			}
		}

		myShareText = env.options.page_title[env.language] + " : ";
		if (env.currentMedia !== null)
			myShareText += env.currentMedia.name;
		else
			myShareText += env.currentAlbum.name;

		myShareUrl = urlWithoutHash;
		myShareUrl += '?url=' + encodeURIComponent(urlWithoutHash);
		myShareUrl += '&title=' + encodeURIComponent(myShareText);
		if (env.currentMedia !== null && env.currentMedia.metadata.hasOwnProperty("title") && env.currentMedia.metadata.title)
			myShareUrl += '&desc=' + encodeURIComponent(env.currentMedia.metadata.title);
		else if (env.currentMedia === null && env.currentAlbum.hasOwnProperty("title") && env.currentAlbum.title)
			myShareUrl += '&desc=' + encodeURIComponent(env.currentAlbum.title);
		hash = location.hash;
		if (hash) {
			myShareUrl += '&hash=' + encodeURIComponent(hash.substring(1));
		}
		if (env.options.debug_js)
			// the following line is needed for debugging purposes in order to bypass the server cache;
			// without the random number, the php code is executed only the first time the url is loaded,
			// in subsequent requests the content is fetched from cache
			myShareUrl += '&r=' + Math.floor(Math.random() * 10000000);
		var whatsAppUrl = myShareUrl;
		myShareUrl += '&w=' + widthParameter;
		myShareUrl += '&h=' + heightParameter;
		myShareUrl += '&m=' + encodeURIComponent(mediaParameter);
		if (whatsAppMediaParameter) {
			whatsAppUrl += '&w=' + whatsappWidthParameter;
			whatsAppUrl += '&h=' + whatsappHeightParameter;
			whatsAppUrl += '&m=' + encodeURIComponent(whatsAppMediaParameter);
		} else {
			whatsAppUrl = myShareUrl;
		}
		if (hash) {
			myShareUrl += hash;
			whatsAppUrl += hash;
		}

		jQuery.removeData(".ssk");
		$('.ssk').attr('data-text', myShareText);
		$('.ssk-facebook').attr('data-url', myShareUrl);
		$('.ssk-whatsapp').attr('data-url', whatsAppUrl);
		$('.ssk-twitter').attr('data-url', whatsAppUrl);
		$('.ssk-google-plus').attr('data-url', myShareUrl);
		$('.ssk-email').attr('data-url', location.href);

		$(".ssk-show-url").off("click").on(
			"click",
			function(ev) {
				let md5Promise = Utilities.getMd5ForTinyUrl(whatsAppUrl);
				md5Promise.then(
					function(tinyUrlParameter) {
						$("#album-and-media-container").stop().fadeTo(500, 0.1);
						var shareUrl = window.location.origin + window.location.pathname + "?t=" + tinyUrlParameter;
						$("#tiny-url").html(Utilities._t("#tiny-url"));
						$("#tiny-url").append(
							"<form id='tiny-url-form'>" +
								"<input type='text' id='tiny-url-for-copying' name='url' value='" + shareUrl + "'>" +
								"<input type='submit' id='tiny-url-button' value='" + Utilities._t("#tiny-url-button") + "'>" +
							"</form>"
						);
						$("#tiny-url-button").off("click").on(
							"click",
							function() {
								document.execCommand('copy');
								$("#tiny-url").append("<div>" + Utilities._t("#tiny-url-copied") + "</div>");
								$("#tiny-url").fadeOut(4000);
								$("#album-and-media-container").stop().fadeTo(4000, 1);
								return false;
							}
						);
						$("#tiny-url").fadeIn(500);
						$("#tiny-url-for-copying").focus().select();
					}
				);
				return false;
			}
		);

		// initialize social buttons (http://socialsharekit.com/)
		SocialShareKit.init({});
		if (! Modernizr.flexbox && Utilities.bottomSocialButtons()) {
			var numSocial = 5;
			var socialWidth = Math.floor(window.innerWidth / numSocial);
			$('.ssk').width(socialWidth * 2 + "px");
		}
	};

	Utilities.getMd5ForTinyUrl = function(myphotoshareUrl) {
		return new Promise(
			function(resolve_getMd5ForTinyUrl) {
				if (Utilities.isPhp()) {
					var request = new XMLHttpRequest();
					request.onreadystatechange = function() {
						if (this.readyState === 4) {
							if (this.status >= 200 && this.status < 400) {
								resolve_getMd5ForTinyUrl(this.responseText);
							}
						}
					};
					request.open("GET", window.location.origin + window.location.pathname + "?longurl=" + encodeURIComponent(myphotoshareUrl), true);
					// request.setRequestHeader('Content-Type', 'application/json');
					request.send();
				} else {
					// not php
					resolve_getMd5ForTinyUrl(false);
				}
			}
		);
	};

	Utilities.addClickToPopupPhoto = function(element) {
		element.parent().parent().off("click").on(
			"click",
			function(ev) {
				$("#album-and-media-container").addClass("show-media");
				ev.stopPropagation();
				ev.preventDefault();
				var imgData = JSON.parse(element.attr("data"));
				// called after an element was successfully handled
				env.highlightedObjectId = null;
				if (Utilities.isShiftOrControl())
					$(".shift-or-control .leaflet-popup-close-button")[0].click();
				$(".media-popup .leaflet-popup-close-button")[0].click();
				// $('#popup #popup-content').html("");
				$('.modal-close')[0].click();
				env.popupRefreshType = "previousAlbum";
				env.mapRefreshType = "none";
				window.location.href = imgData.mediaHash;
			}
		);
		if (Utilities.isPhp) {
			// execution enters here if we are using index.php
			element.parent().parent().off("auxclick").on(
				"auxclick",
				function (ev) {
					if (ev.which === 2) {
						var imgData = JSON.parse(element.attr("data"));
						Utilities.openInNewTab(imgData.mediaHash);
						return false;
					}
				}
			);
		}

		var mediaBoxSelectElement = element.siblings('a');
		var id = mediaBoxSelectElement.attr("id");
		mediaBoxSelectElement.off("click").on(
			"click",
			{id: id},
			function(ev) {
				ev.stopPropagation();
				ev.preventDefault();
				var imgData = JSON.parse(element.attr("data"));
				var cachedAlbum = env.cache.getAlbum(imgData.albumCacheBase);
				var name = imgData.mediaHash.split('/').pop();
				var matchedMedia = cachedAlbum.media.find(singleMedia => name === singleMedia.cacheBase);
				var promise = PhotoFloat.getAlbum(matchedMedia.foldersCacheBase, null, {getMedia: true, getPositions: ! env.options.save_data});
				promise.then(
					function(foldersAlbum) {
						matchedMedia.toggleSelectedStatus(foldersAlbum, '#' + id);
					}
				);
			}
		);
	};

	Utilities.addMediaLazyLoader = function() {
		var threshold = env.options.media_thumb_size;
		if (env.options.save_data)
			threshold = 0;
		$(
			function() {
				$("img.lazyload-popup-media").Lazy(
					{
						afterLoad: Utilities.addClickToPopupPhoto,
						autoDestroy: true,
						onError: function(element) {
							console.log(element[0]);
						},
						chainable: false,
						threshold: threshold,
						scrollDirection: "vertical",
						appendScroll: $('#popup-images-wrapper')
					}
				);

				$("#album-and-media-container:not(.show-media) #thumbs img.lazyload-media").Lazy(
					{
						threshold: threshold,
						scrollDirection: "vertical",
						appendScroll: $(window)
					}
				);

				$("#album-and-media-container.show-media #thumbs img.lazyload-media").Lazy(
					{
						threshold: threshold,
						scrollDirection: "horizontal",
						appendScroll: $("#album-view")
					}
				);
			}
		);
	};

	Utilities.prototype.getMediaFromImgObject = function(object) {
		var src = object.attr("data-src");
		if (object.attr("data-src") === undefined)
			src = object.attr("src");
		var splittedSrc = src.split("/")[2].split(env.options.cache_folder_separator);
		var randomMediaAlbumCacheBase;
		if (splittedSrc.length === 2) {
			randomMediaAlbumCacheBase = env.options.folders_string;
		} else {
			randomMediaAlbumCacheBase = splittedSrc.slice(0, -2).join(env.options.cache_folder_separator);
			if (
				[
					env.options.by_date_string,
					env.options.by_gps_string,
					env.options.by_search_string,
					env.options.by_map_string,
					env.options.by_selection_string
				].indexOf(splittedSrc[0]) === -1
			)
				randomMediaAlbumCacheBase = env.options.folders_string + env.options.cache_folder_separator + randomMediaAlbumCacheBase;
		}
		var randomMediaCacheBase = splittedSrc[splittedSrc.length - 2];
		var randomMedia = env.cache.getMedia(randomMediaAlbumCacheBase, randomMediaCacheBase);
		return [randomMedia, randomMediaAlbumCacheBase];
	};

	Utilities.dateElementForFolderName = function(folderArray, index) {
		if (index === 1 || index === 3)
			return parseInt(folderArray[index]);
		else if (index === 2)
			return Utilities._t("#month-" + folderArray[index]);
	};

	Utilities.addSpanToFirstAndSecondLine = function(firstLine, secondLine) {
		var result = [], index = 0;
		if (firstLine) {
			result[index] = "<span class='first-line'>" + firstLine + "</span>";
			index ++;
		}
		if (secondLine)
			result[index] = "<span class='second-line'>" + secondLine + "</span>";
		return result;
	};

	Utilities.convertByDateAncestorNames = function(ancestorsNames) {
		if (ancestorsNames[0] === env.options.by_date_string && ancestorsNames.length > 2) {
			let result = ancestorsNames.slice();
			result[2] = Utilities._t("#month-" + result[2]);
			return result;
		} else {
			return ancestorsNames;
		}

	};

	Utilities.prototype.generateAlbumCaptionsForCollections = function(album) {
		var raquo = " <span class='gray'>&raquo;</span> ";
		var folderArray = album.cacheBase.split(env.options.cache_folder_separator);

		var firstLine;
		if (album.isByDate()) {
			firstLine = Utilities.dateElementForFolderName(folderArray, folderArray.length - 1);
		} else if (album.isByGps()) {
			if (album.name === '')
				firstLine = Utilities._t('.not-specified');
			else if (album.hasOwnProperty('altName'))
				firstLine = Utilities.transformAltPlaceName(album.altName);
			else
				firstLine = album.nameForShowing(null, true, true);
		} else {
			firstLine = album.nameForShowing(null, true, true);
		}

		var secondLine = '';
		if (album.isByDate()) {
			secondLine += "<span class='gray'>(";
			if (folderArray.length === 2) {
				secondLine += Utilities._t("#year-album");
			} else if (folderArray.length === 3) {
				secondLine += Utilities._t("#month-album") + " ";
			} else if (folderArray.length === 4) {
				secondLine += Utilities._t("#day-album") + ", ";
			}
			secondLine += "</span>";
			if (folderArray.length > 2) {
				if (folderArray.length === 4)
					secondLine += Utilities.dateElementForFolderName(folderArray, 2) + " ";
				secondLine += Utilities.dateElementForFolderName(folderArray, 1);
			}
			secondLine += "<span class='gray'>)</span>";
		} else if (album.isByGps()) {
			for (let iCacheBase = 1; iCacheBase < album.ancestorsCacheBase.length - 1; iCacheBase ++) {
				let albumName;
				if (album.ancestorsNames[iCacheBase] === '')
					albumName = Utilities._t('.not-specified');
				else
					albumName = album.ancestorsNames[iCacheBase];
				if (iCacheBase === 1)
					secondLine = "<span class='gray'>(" + Utilities._t("#by-gps-album-in") + "</span> ";
				// let marker = "<marker>" + iCacheBase + "</marker>";
				// secondLine += marker;
				secondLine += "<a href='" + env.hashBeginning + album.ancestorsCacheBase[iCacheBase] + "'>" + albumName + "</a>";
				if (iCacheBase < album.ancestorsCacheBase.length - 2)
					secondLine += raquo;
				if (iCacheBase === album.ancestorsCacheBase.length - 2)
					secondLine += "<span class='gray'>)</span>";

				// secondLine = secondLine.replace(marker, "<a href='" + env.hashBeginning + album.ancestorsCacheBase[iCacheBase] + "'>" + albumName + "</a>");
			}
			if (! secondLine)
				secondLine = "<span class='gray'>(" + Utilities._t("#by-gps-album") + ")</span>";
		} else if (album.cacheBase === env.options.folders_string) {
			secondLine += "<span class='gray'>(" + Utilities._t("#root-album") + ")</span>";
		} else {
			for (let iCacheBase = 0; iCacheBase < album.ancestorsCacheBase.length - 1; iCacheBase ++) {
				if (iCacheBase === 0 && album.ancestorsCacheBase.length === 2) {
					secondLine = "<span class='gray'>(" + Utilities._t("#regular-album") + ")</span> ";
				} else {
					if (iCacheBase === 1)
						secondLine = "<span class='gray'>(" + Utilities._t("#regular-album-in") + "</span> ";
					// let marker = "<marker>" + iCacheBase + "</marker>";
					// secondLine += marker;
					secondLine += "<a href='" + env.hashBeginning + album.ancestorsCacheBase[iCacheBase] + "'>" + album.ancestorsNames[iCacheBase] + "</a>";
					if (iCacheBase < album.ancestorsCacheBase.length - 2)
						secondLine += raquo;
					if (iCacheBase === album.ancestorsCacheBase.length - 2)
						secondLine += "<span class='gray'>)</span>";
					// secondLine = secondLine.replace(marker, "<a href='" + env.hashBeginning + album.ancestorsCacheBase[iCacheBase] + "'>" + album.ancestorsNames[iCacheBase] + "</a>");
				}
			}
		}
		var captionsForCollection = Utilities.addSpanToFirstAndSecondLine(firstLine, secondLine);
		var captionForCollectionSorting = Utilities.convertByDateAncestorNames(album.ancestorsNames).slice(1).reverse().join(env.options.cache_folder_separator).replace(/^0+/, '');
		return [captionsForCollection, captionForCollectionSorting];
	};

	Utilities.prototype.generateSingleMediaCaptionsForCollections = function(singleMedia, album) {
		var raquo = " <span class='gray'>&raquo;</span> ";
		var folderArray = album.cacheBase.split(env.options.cache_folder_separator);

		var secondLine = '';
		if (album.isByDate()) {
			secondLine += "<span class='gray'>(";
			if (folderArray.length === 2) {
				secondLine += Utilities._t("#in-year-album") + " ";
			} else if (folderArray.length === 3) {
				secondLine += Utilities._t("#in-month-album") + " ";
			} else if (folderArray.length === 4) {
				secondLine += Utilities._t("#in-day-album") + " ";
			}
			secondLine += "</span>";
			if (folderArray.length > 3)
				secondLine += "<a href='" + env.hashBeginning + album.ancestorsCacheBase[3] + "'>" + Utilities.dateElementForFolderName(folderArray, 3) + "</a>" + " ";
			if (folderArray.length > 2)
				secondLine += "<a href='" + env.hashBeginning + album.ancestorsCacheBase[2] + "'>" + Utilities.dateElementForFolderName(folderArray, 2) + "</a>" + " ";
			if (folderArray.length > 1)
				secondLine += "<a href='" + env.hashBeginning + album.ancestorsCacheBase[1] + "'>" + Utilities.dateElementForFolderName(folderArray, 1) + "</a>";
			secondLine += "<span class='gray'>)</span>";
		} else if (album.isByGps()) {
			for (let iCacheBase = 1; iCacheBase < album.ancestorsCacheBase.length; iCacheBase ++) {
				let albumName;
				if (album.ancestorsNames[iCacheBase] === "")
					albumName = Utilities._t('.not-specified');
				else
					albumName = Utilities.transformAltPlaceName(album.ancestorsNames[iCacheBase]);
				if (iCacheBase === 1)
					secondLine += "<span class='gray'>(" + Utilities._t("#in-by-gps-album") + "</span> ";
				// let marker = "<marker>" + iCacheBase + "</marker>";
				// secondLine += marker;
				secondLine += "<a href='" + env.hashBeginning + album.ancestorsCacheBase[iCacheBase] + "'>" + albumName + "</a>";
				if (iCacheBase < album.ancestorsCacheBase.length - 1)
					secondLine += raquo;
				if (iCacheBase === album.ancestorsCacheBase.length - 1)
					secondLine += "<span class='gray'>)</span>";

				// secondLine = secondLine.replace(marker, "<a href='" + env.hashBeginning + album.ancestorsCacheBase[iCacheBase] + "'>" + albumName + "</a>");
			}
			if (! secondLine)
				secondLine = "<span class='gray'>(" + Utilities._t("#by-gps-album") + ")</span>";
		// } else if (album.cacheBase === env.options.folders_string) {
		// 	secondLine += "<span class='gray'>(" + Utilities._t("#root-album") + ")</span>";
		} else {
			for (let iCacheBase = 1; iCacheBase < album.ancestorsCacheBase.length; iCacheBase ++) {
				if (iCacheBase === 0 && album.ancestorsCacheBase.length === 2) {
					secondLine += "<span class='gray'>(" + Utilities._t("#regular-album") + ")</span>";
				} else {
					if (iCacheBase === 1)
						secondLine += "<span class='gray'>(" + Utilities._t("#in") + "</span> ";
					// let marker = "<marker>" + iCacheBase + "</marker>";
					// secondLine += marker;
					secondLine += "<a href='" + env.hashBeginning + album.ancestorsCacheBase[iCacheBase] + "'>" + album.ancestorsNames[iCacheBase] + "</a>";
					if (iCacheBase < album.ancestorsCacheBase.length - 1)
						secondLine += raquo;
					if (iCacheBase === album.ancestorsCacheBase.length - 1)
						secondLine += "<span class='gray'>)</span>";
					// secondLine = secondLine.replace(marker, "<a href='" + env.hashBeginning + album.ancestorsCacheBase[iCacheBase] + "'>" + album.ancestorsNames[iCacheBase] + "</a>");
				}
			}
		}

		var [nameForShowing, titleForShowing] = singleMedia.nameAndTitleForShowing(true, true);
		var captionsForCollection = Utilities.addSpanToFirstAndSecondLine(nameForShowing, secondLine);
		var captionForCollectionSorting = nameForShowing + env.options.cache_folder_separator + Utilities.convertByDateAncestorNames(album.ancestorsNames).slice(1).reverse().join(env.options.cache_folder_separator).replace(/^0+/, '');
		return [captionsForCollection, captionForCollectionSorting, titleForShowing];
	};

	Utilities.prototype.nameForShowing = function(albumOrSubalbum, parentAlbum, html, br) {
		var folderName = '';
		if (albumOrSubalbum.cacheBase === env.options.by_date_string) {
			folderName = "(" + Utilities._t("#by-date") + ")";
		} else if (albumOrSubalbum.cacheBase === env.options.by_gps_string) {
			folderName = "(" + Utilities._t("#by-gps") + ")";
		} else if (parentAlbum && parentAlbum.isByDate() || albumOrSubalbum.isByDate()) {
			let folderArray = albumOrSubalbum.cacheBase.split(env.options.cache_folder_separator);
			if (folderArray.length === 2) {
				folderName += parseInt(folderArray[1]);
			} else if (folderArray.length === 3)
				folderName += " " + Utilities._t("#month-" + folderArray[2]);
			else if (folderArray.length === 4)
				folderName += Utilities._t("#day") + " " + parseInt(folderArray[3]);
		} else if (parentAlbum && parentAlbum.isByGps() || albumOrSubalbum.isByGps()) {
			if (albumOrSubalbum.name === '')
				folderName = Utilities._t('.not-specified');
			else if (albumOrSubalbum.hasOwnProperty('altName'))
				folderName = Utilities.transformAltPlaceName(albumOrSubalbum.altName);
			else
				folderName = albumOrSubalbum.name;
		} else if (albumOrSubalbum.hasOwnProperty("title") && albumOrSubalbum.title && albumOrSubalbum.title !== albumOrSubalbum.name) {
			folderName = albumOrSubalbum.title;
			if (! br) {
				// remove the tags fronm the title
				folderName = folderName.replace(/<[^>]*>?/gm, ' ');
			}

			if (albumOrSubalbum.name) {
				if (html && br)
					folderName += env.br + "<span class='real-name'>[" + albumOrSubalbum.name + "]</span>";
				else if (html)
					folderName += " <span class='real-name'>[" + albumOrSubalbum.name + "]</span>";
				else
					folderName += " [" + albumOrSubalbum.name + "]";
			}
		} else {
			folderName = albumOrSubalbum.name;
		}

		return folderName;
	};

	Utilities.setPrevNextPosition = function() {
		var titleHeight = 0;
		if ($(".media-box#center .title").is(":visible"))
			titleHeight = $(".media-box#center .title").outerHeight();

		var prevNextHeight = parseInt($("#next").outerHeight());
		if (! env.fullScreenStatus || ! $(".media-box#center").hasClass("rotation")) {
			// position next/prev buttons verticallly centered in media-box-inner
			var mediaBoxInnerHeight = parseInt($(".media-box#center .media-box-inner").css("height"));
			$("#next, #prev").css("top", titleHeight + (mediaBoxInnerHeight - prevNextHeight) / 2);
			$("#prev, #next").css("right", "");
			$("#prev, #next").removeClass("rotate-90");
			$("#prev").css("left", 0);
			// $("#prev").css("top", "");
			$("#next").css("right", 0);
			$("#next").css("bottom", "");

			Utilities.setLinksVisibility();
		} else {
			let prevNextWidth = parseInt($("#next").outerWidth());
			let mediaBoxInnerWidth = parseInt($(".media-box#center .media-box-inner").css("width"));
			$("#prev, #next").css("top", "");
			$("#prev, #next").css("right", (mediaBoxInnerWidth - prevNextHeight) / 2);
			$("#prev, #next").addClass("rotate-90");
			$("#prev").css("left", "unset");
			$("#prev").css("top", - prevNextWidth / 2);
			$("#next").css("bottom", - prevNextWidth / 2);
		}
	};

	Utilities.setSlideshowButtonsPosition = function() {
		let slideshowContainerHeight = parseInt($("#slideshow-buttons").outerHeight());
		let slideshowContainerWidth = parseInt($("#slideshow-buttons").outerWidth());
		if (env.slideshowId && env.currentMedia.needsAutoRotation()) {
			$("#slideshow-buttons").addClass("rotate-90");
			$("#slideshow-buttons").css("bottom", ((env.windowHeight - slideshowContainerHeight) / 2).toString() + "px");
			$("#slideshow-buttons").css("left", "");
			$("#slideshow-buttons").css("right", "");
			if (env.currentMedia.isImage())
				$("#slideshow-buttons").css(
					"left",
					($("#prev").outerWidth(true) + 10 - (slideshowContainerWidth - slideshowContainerHeight) / 2).toString() + "px"
				);
			else
				$("#slideshow-buttons").css(
					"right",
					($("#next").outerWidth(true) + 10 - (slideshowContainerWidth - slideshowContainerHeight) / 2).toString() + "px"
				);
		} else {
			$("#slideshow-buttons").removeClass("rotate-90");
			$("#slideshow-buttons").css("left", ((env.windowWidth - slideshowContainerWidth) / 2).toString() + "px");
			$("#slideshow-buttons").css("bottom", "20px");
		}
	};

	Utilities.setPinchButtonsPosition = function(containerHeight, containerWidth) {
		// calculate and set pinch buttons position

		var mediaElement = $(".media-box#center .media-box-inner img");
		var titleHeight, thumbsHeight;
		if ($(".media-box#center .title").is(":visible"))
			titleHeight = $(".media-box#center .title").height();
		else
			titleHeight = 0;
		if ($("#thumbs").is(":visible"))
			thumbsHeight = $("#thumbs").height();
		else
			thumbsHeight = 0;
		var distanceFromImageBorder = 15;
		// if (containerHeight === undefined) {
		containerHeight = env.windowHeight - titleHeight - thumbsHeight;
		containerWidth = env.windowWidth;

		if (env.slideshowId && env.currentMedia.needsAutoRotation()) {
			let right, bottom;
			let windowRatio = env.windowWidth / env.windowHeight;
			let rotatedImageRatio = mediaElement.attr("height") / mediaElement.attr("width");
			let pinchContainerWidth = parseInt($("#pinch-container").outerWidth());
			$("#pinch-container").addClass("rotate-90");
			if (windowRatio > rotatedImageRatio) {
				right = Math.round((env.windowWidth - env.windowHeight * rotatedImageRatio) / 2) + pinchContainerWidth + distanceFromImageBorder;
				bottom = distanceFromImageBorder;
			} else {
				right = pinchContainerWidth + distanceFromImageBorder;
				bottom = Math.round((env.windowHeight - env.windowWidth / rotatedImageRatio) / 2) + distanceFromImageBorder;
			}
			$("#pinch-container").css("top", "");
			$("#pinch-container").css("right", right.toString() + "px");
			$("#pinch-container").css("bottom", bottom.toString() + "px");
		} else {
			let actualHeight = mediaElement.height();
			let actualWidth = mediaElement.width();
			let top = Math.round(titleHeight + (containerHeight - actualHeight) / 2 + distanceFromImageBorder);
			let right = Math.round((containerWidth - actualWidth) / 2 + distanceFromImageBorder);
			$("#pinch-container").css("bottom", "");
			$("#pinch-container").css("top", top.toString() + "px");
			$("#pinch-container").css("right", right.toString() + "px");
			$("#pinch-container").removeClass("rotate-90");
		}
	};

	Utilities.setPinchButtonsVisibility = function() {
		$("#pinch-container").removeClass("hidden");

		if (! env.currentMedia || env.currentMedia.isVideo()) {
			$("#pinch-container").hide();
		} else {
			$("#pinch-container").show();

			$("#pinch-in").off("click");
			$("#pinch-in").off("click").on(
				"click",
				function(ev) {
					PinchSwipe.pinchIn(null, null);
				}
			);
			$("#pinch-in").removeClass("disabled");

			$("#pinch-out").off("click");
			if (
				$("#center .title").hasClass("hidden-by-pinch") ||
				(env.fullScreenStatus || env.isAnyMobile) && env.currentZoom > env.initialZoom
			) {
				$("#pinch-out").removeClass("disabled");
				$("#pinch-out").off("click").on(
					"click",
					function(ev) {
						PinchSwipe.pinchOut(null, null);
					}
				);
			} else {
				$("#pinch-out").addClass("disabled");
			}
		}
	};

	Utilities.prototype.horizontalScrollBarThickness = function(element) {
		var thickness = element.offsetHeight - element.clientHeight;
		if (! thickness && env.currentAlbum.hasOwnProperty("media")) {
			// sometimes thickness is 0, but the scroll bar could be there
			// let's try to suppose if it's there
			let totalThumbsSize = env.options.media_thumb_size * env.currentAlbum.media.length;
			if (env.options.media_thumb_type.indexOf("fixed_height") > -1) {
				let sum = 0;
				totalThumbsSize = env.currentAlbum.media.forEach(
					singleMedia => {
						sum += env.options.media_thumb_size / singleMedia.metadata.size[1] * singleMedia.metadata.size[0];
					}
				);
			}
			if (env.options.spacing)
				totalThumbsSize += env.options.spacing * (env.currentAlbum.media.length - 1);

			if (totalThumbsSize > env.windowWidth) {
				// the scrollbar is there
				thickness = 15;

			}
		}
		return thickness;
	};

	Utilities.setSelectButtonPosition = function(containerHeight, containerWidth) {
		// calculate and set pinch buttons position
		var bottom, left, top;
		if ($(".select-box").attr("src") === undefined)
			return false;

		var mediaElement = $(".media-box#center .media-box-inner img");
		var actualHeight = mediaElement.height();
		var actualWidth = mediaElement.width();
		var titleHeight = 0, thumbsHeight = 0;
		var distanceFromImageBorder = 15;
		containerHeight = env.windowHeight;
		containerWidth = env.windowWidth;
		if (env.slideshowId && env.currentMedia.needsAutoRotation()) {
			let windowRatio = env.windowWidth / env.windowHeight;
			let rotatedImageRatio = mediaElement.attr("height") / mediaElement.attr("width");
			if (windowRatio > rotatedImageRatio) {
				left = Math.round((env.windowWidth - env.windowHeight * rotatedImageRatio) / 2) + distanceFromImageBorder;
				top = distanceFromImageBorder;
			} else {
				left = distanceFromImageBorder;
				top = Math.round((env.windowHeight - env.windowWidth / rotatedImageRatio) / 2) + distanceFromImageBorder;
			}
		} else {
			if ($(".media-box#center .title").is(":visible"))
				titleHeight = $(".media-box#center .title").height();
			if ($("#thumbs").is(":visible"))
				thumbsHeight = Math.max($("#thumbs").height(), parseInt($("#thumbs").css("height")));
			containerHeight -= titleHeight + thumbsHeight;
			bottom = Math.round(thumbsHeight + (containerHeight - actualHeight) / 2) + distanceFromImageBorder;
			left = Math.round((containerWidth - actualWidth) / 2) + distanceFromImageBorder;
		}

		$("#media-select-box .select-box").css("top", "");
		$("#media-select-box .select-box").css("left", "");
		$("#media-select-box .select-box").css("left", left.toString() + "px");
		$("#media-select-box .select-box").css("bottom", "");
		if (bottom !== undefined)
			$("#media-select-box .select-box").css("bottom", bottom.toString() + "px");
		else
			$("#media-select-box .select-box").css("top", top.toString() + "px");

		return true;
	};

	Utilities.correctElementPositions = function() {
		function MoveMediaBarAboveBottomSocial() {
			if (
				env.currentMedia !== null &&
				Utilities.bottomSocialButtons() &&
				Utilities.areColliding($(".media-box#center .media-bar"), $("#social > div"))
			) {
				// move the media bar above the social buttons
				$(".media-box#center .media-bar").css("bottom", ($("#social > div").outerHeight()) + "px");
			}
		}

		function separateLateralSocialAndPrev() {
			if (
				env.currentMedia !== null &&
				! env.currentAlbumIsAlbumWithOneMedia &&
				Utilities.lateralSocialButtons() &&
				Utilities.areColliding($("#social > div"), $("#prev"))
			) {
				if (parseFloat($("#prev").css("bottom")) > $("#social > div").outerHeight()) {
					// move social buttons below prev button
					$("#social > div")
						.removeClass("ssk-center")
						.css("top", (parseFloat($("#prev").css("top")) + $("#prev").outerHeight()) + "px" );
				} else {
					// move social buttons to the right of prev button
					$("#social > div").css("left", ($("#prev").outerWidth(true)) + "px");
				}
			}
		}

		function moveSelectBoxAboveBottomSocial() {
			// move the select box above the social buttons and the media bar
			if (
				env.currentMedia !== null &&
				Utilities.bottomSocialButtons() &&
				Utilities.areColliding($("#media-select-box .select-box"), $("#social > div"))
			) {
				$("#media-select-box .select-box").css("bottom", ($("#social > div").outerHeight() + 10) + "px");
			}
		}

		function moveSelectBoxAboveMediaBar() {
			if (
				env.currentMedia !== null &&
				Utilities.areColliding($("#media-select-box .select-box"), $(".media-box#center .media-bar .links"))
			) {
				$("#media-select-box .select-box").css(
					"bottom",
					(env.windowHeight - $(".media-box#center .media-bar .links").offset().top + 10).toString() + "px"
				);
			}
		}

		function moveSelectBoxAtTheRightOfPrev() {
			// move the select box at the right of the prev button and lateral social buttons
			if (
				env.currentMedia !== null &&
				! env.currentAlbumIsAlbumWithOneMedia &&
				Utilities.areColliding($("#media-select-box .select-box"), $("#prev"))
			) {
				if (env.slideshowId && env.currentMedia.needsAutoRotation())
					$("#media-select-box .select-box").css("top", ($("#prev").outerWidth(true) + 20) + "px");
				else
					$("#media-select-box .select-box").css("left", ($("#prev").outerWidth(true) + 20) + "px");
			}
		}

		function moveSelectBoxAtTheRightOfLateralSocial() {
			if (
				env.currentMedia !== null &&
				Utilities.lateralSocialButtons() &&
				Utilities.areColliding($("#media-select-box .select-box"), $("#social > div"))
			) {
				$("#media-select-box .select-box").css("left", ($("#social > div").outerWidth(true) + 20) + "px");
			}
		}

		function moveSelectBoxAboveSlideshow() {
			if (
				env.currentMedia !== null &&
				! env.currentAlbumIsAlbumWithOneMedia &&
				env.slideshowId &&
				Utilities.areColliding($("#media-select-box .select-box"), $("#slideshow-buttons"))
			) {
				if (env.currentMedia.needsAutoRotation())
					$("#media-select-box .select-box").css(
						"left",
						($("#slideshow-buttons").outerHeight(true) + parseInt($("#slideshow-buttons").css("left")) + 20) + "px"
					);
				else
					$("#media-select-box .select-box").css(
						"bottom",
						($("#slideshow-buttons").outerHeight(true) + parseInt($("#slideshow-buttons").css("bottom")) + 20) + "px"
					);
			}
		}

		function movePinchAtTheLeftOfNext() {
			// correct pinch buttons position
			if (
				env.currentMedia !== null &&
				! env.currentAlbumIsAlbumWithOneMedia &&
				Utilities.areColliding($("#pinch-container"), $("#next"))
			) {
				if (env.slideshowId && env.currentMedia.needsAutoRotation())
					$("#pinch-container").css("bottom", ($("#next").outerWidth(true) + 20) + "px");
				else
					$("#pinch-container").css("right", ($("#next").outerWidth(true) + 20) + "px");
			}
		}

		function movePinchBelowMenuButtons() {
			// correct pinch buttons position
			if (
				env.currentMedia !== null &&
				Utilities.areColliding($("#pinch-container"), $("#right-and-search-menu"))
			) {
				$("#pinch-container").css("top", ($("#right-and-search-menu").outerHeight(true) + 20) + "px");
			}
		}

		function moveDescriptionAtTheLeftOfNext() {
			// correct description/tags box position
			// always moves at the left of next, even if not colliding
			if (
				env.currentMedia !== null &&
				! env.currentAlbumIsAlbumWithOneMedia
			) {
				$("#description-wrapper").css("right", ($("#next").outerWidth(true) + 10) + "px");
			}
		}

		function moveDescriptionAboveBottomSocial() {
			if (
				Utilities.bottomSocialButtons() &&
				Utilities.areColliding($("#description-wrapper"), $("#social > div"))
			) {
				// move the descriptiont/tags box above the social buttons
				$("#description-wrapper").css("bottom", ($("#social > div").outerHeight() + 10) + "px");
			}
		}

		function MoveDescriptionAboveMediaBar() {
			if (
				env.currentMedia !== null &&
				Utilities.areColliding($("#description-wrapper"), $(".media-box#center .media-bar .links"))
			) {
				// move the descriptiont/tags box above the media bar
				let thumbsHeight = 0;
				if ($("#thumbs").is(":visible"))
					thumbsHeight = $("#thumbs").outerHeight();
				$("#description-wrapper").css(
					"bottom",
					(env.windowHeight - $(".media-box#center .media-bar .links").offset().top + 10).toString() + "px"
				);
			}
		}

		function moveDescriptionAtTheLeftOfPinch() {
			if (
				env.currentMedia !== null &&
				Utilities.areColliding($("#description-wrapper"), $("#pinch-container"))
			) {
				// move the descriptiont/tags box to the left of the pinch buttons
				$("#description-wrapper").css(
					"right",
					(parseFloat($("#pinch-container").css("right")) + $("#pinch-container").outerWidth(true) + 10) + "px"
				);
			}
		}

		$("#social > div").removeClass("ssk-bottom").addClass("ssk-center");
		$("#social > div").css("left", "").css("top", "");
		Utilities.setSocialButtons();

		var mediaBarHeigth = parseFloat($(".media-box#center .media-bar").outerHeight());
		if (! mediaBarHeigth)
			mediaBarHeigth = 30;
		$(".media-box#center .media-bar").css("bottom", "");

		MoveMediaBarAboveBottomSocial();
		separateLateralSocialAndPrev();
		moveSelectBoxAboveBottomSocial();
		moveSelectBoxAboveMediaBar();
		moveSelectBoxAtTheRightOfPrev();
		moveSelectBoxAtTheRightOfLateralSocial();
		moveSelectBoxAboveSlideshow();
		movePinchBelowMenuButtons();
		movePinchAtTheLeftOfNext();
		moveDescriptionAtTheLeftOfNext();
		moveDescriptionAtTheLeftOfPinch();
		moveDescriptionAboveBottomSocial();
		MoveDescriptionAboveMediaBar();
		moveDescriptionAtTheLeftOfNext();
		moveDescriptionAtTheLeftOfPinch();
	};

	Utilities.prototype.sumNumsProtectedMediaOfArray = function(arrayOfAlbumsOrSubalbunms) {
		var result = new NumsProtected({}), i, codesComplexcombination, albumOrSubalbum;

		for (i = 0; i < arrayOfAlbumsOrSubalbunms.length; i ++) {
			albumOrSubalbum = arrayOfAlbumsOrSubalbunms[i];
			for (codesComplexcombination in albumOrSubalbum.numsProtectedMediaInSubTree) {
				if (albumOrSubalbum.numsProtectedMediaInSubTree.hasOwnProperty(codesComplexcombination) && codesComplexcombination !== ",") {
					if (! result.hasOwnProperty(codesComplexcombination))
						result[codesComplexcombination] = new ImagesAndVideos();
					result[codesComplexcombination].sum(albumOrSubalbum.numsProtectedMediaInSubTree[codesComplexcombination]);
				}
			}
		}

		return result;
	};


	Utilities.areColliding = function(jQueryObject1, jQueryObject2) {
		function isNotLittle(number) {
			return number > - env.minimumDistanceForCollisions;
		}

		function isInside(corner, square) {
			// return square[0] <= corner[0] && corner[0] <= square[2] && square[1] <= corner[1] && corner[1] <= square[3];
			return (
				isNotLittle(corner[0] - square[0]) &&
				isNotLittle(square[2] - corner[0]) &&
				isNotLittle(corner[1] - square[1]) &&
				isNotLittle(square[3] - corner[1])
			);
		}

		function intersect(side, sides2) {
			return sides2.some(side2 => intersects(side, side2));
		}

		function intersects(side1, side2) {
			var side1Horizontal = false;
			var side2Horizontal = false;
			if (side1[1][0] === side1[0][0])
				side1Horizontal = true;
			if (side2[1][0] === side2[0][0])
				side2Horizontal = true;

			if (side1Horizontal === side2Horizontal) {
				if (side1Horizontal)
					return (
						side1[0][0] === side2[0][0] &&
						side1.some(
							point =>
								isNotLittle(point[1] - side2[0][1]) &&
								isNotLittle(point[1] - side2[1][1])
						)
					);
				else
					return (
						side1[0][1] === side2[0][1] &&
						side1.some(
							point =>
								isNotLittle(point[0] - side2[0][0]) &&
								isNotLittle(point[1] - side2[1][0])
						)
					);
			} else {
				if (side1Horizontal)
					return (
						isNotLittle(side1[0][0] - side2[0][0]) &&
						isNotLittle(side2[1][0] - side1[0][0]) &&
						isNotLittle(side2[0][1] - side1[0][1]) &&
						isNotLittle(side1[1][1] - side2[0][1])
					);
				else
					return (
						isNotLittle(side2[0][0] - side1[0][0]) &&
						isNotLittle(side1[1][0] - side2[0][0]) &&
						isNotLittle(side1[0][1] - side2[0][1]) &&
						isNotLittle(side2[1][1] - side1[0][1])
					);
			}
		}

		if (! jQueryObject1.is(":visible") || ! jQueryObject2.is(":visible"))
			return false;
		var offset1 = jQueryObject1.offset();
		var top1 = offset1.top;
		var left1 = offset1.left;
		var height1 = jQueryObject1.outerHeight(true);
		var width1 = jQueryObject1.outerWidth(true);
		var bottom1 = offset1.top + height1;
		var right1 = offset1.left + width1;

		// Div 2 data
		var offset2 = jQueryObject2.offset();
		var top2 = offset2.top;
		var left2 = offset2.left;
		var height2 = jQueryObject2.outerHeight(true);
		var width2 = jQueryObject2.outerWidth(true);
		var bottom2 = offset2.top + height2;
		var right2 = offset2.left + width2;

		var corners1 = [
			[top1, left1],
			[top1, right1],
			[bottom1, left1],
			[bottom1, right1]
		];
		var corners2 = [
			[top2, left2],
			[top2, right2],
			[bottom2, left2],
			[bottom2, right2]
		];
		var sides1 = [
			[[top1, left1], [top1, right1]],
			[[top1, right1], [bottom1, right1]],
			[[bottom1, left1], [bottom1, right1]],
			[[top1, left1], [bottom1, left1]]
		];
		var sides2 = [
			[[top2, left2], [top2, right2]],
			[[top2, right2], [bottom2, right2]],
			[[bottom2, left2], [bottom2, right2]],
			[[top2, left2], [bottom2, left2]]
		];
		var square1 = [top1, left1, bottom1, right1];
		var square2 = [top2, left2, bottom2, right2];

		var colliding =
			corners1.some(corner => isInside(corner, square2)) ||
			corners2.some(corner => isInside(corner, square1)) ||
			sides1.some(side => intersect(side, sides2));

		return colliding;
	};

	Utilities.degreesToRadians = function(degrees) {
		var pi = Math.PI;
		return degrees * (pi/180);
	};

	Utilities.prototype.escapeSingleQuotes = function(text) {
		return text.replace(/'/g, "\\'");
	};

	Utilities.xDistanceBetweenCoordinatePoints = function(point1, point2) {
		return Math.max(
			Utilities.distanceBetweenCoordinatePoints({lng: point1.lng, lat: point1.lat}, {lng: point2.lng, lat: point1.lat}),
			Utilities.distanceBetweenCoordinatePoints({lng: point1.lng, lat: point2.lat}, {lng: point2.lng, lat: point2.lat})
		);
	};

	Utilities.yDistanceBetweenCoordinatePoints = function(point1, point2) {
		return Utilities.distanceBetweenCoordinatePoints({lng: point1.lng, lat: point1.lat}, {lng: point1.lng, lat: point2.lat});
	};

	Utilities.distanceBetweenCoordinatePoints = function(point1, point2) {
		// converted from Geonames.py
		// Calculate the great circle distance in meters between two points on the earth (specified in decimal degrees)

		// convert decimal degrees to radians
		var r_lon1 = Utilities.degreesToRadians(point1.lng);
		var r_lat1 = Utilities.degreesToRadians(point1.lat);
		var r_lon2 = Utilities.degreesToRadians(point2.lng);
		var r_lat2 = Utilities.degreesToRadians(point2.lat);
		// haversine formula
		var d_r_lon = r_lon2 - r_lon1;
		var d_r_lat = r_lat2 - r_lat1;
		var a = Math.pow(Math.sin(d_r_lat / 2), 2) + Math.cos(r_lat1) * Math.cos(r_lat2) * Math.pow(Math.sin(d_r_lon / 2), 2);
		var c = 2 * Math.asin(Math.sqrt(a));
		var earth_radius = 6371000;  // radius of the earth in m
		var dist = earth_radius * c;
		return dist;
	};

	Utilities.lateralSocialButtons = function() {
		return $(".ssk-group").css("display") === "block";
	};

	Utilities.bottomSocialButtons = function() {
		return $(".ssk-group").css("display") === "flex";
	};

	Utilities.setLinksVisibility = function() {
		if (env.isAnyMobile) {
			$(".media-box .links").css("display", "inline").css("opacity", 0.5).stop().fadeTo("slow", 0.25);
		} else {
			$("#media-view").off();
			$("#media-view").off('mouseover').on(
				'mouseover',
				function() {
					$(".media-box .links").stop().fadeTo("slow", 0.50).css("display", "inline");
				}
			);
			$("#media-view").off('mouseout').on(
				'mouseout',
				function() {
					$(".media-box .links").stop().fadeOut("slow");
				}
			);
		}
	};

	Utilities.setPrevNextVisibility = function() {
		if (env.currentMedia === null || env.currentAlbumIsAlbumWithOneMedia)
			$("#next, #prev").hide();
		else
			$("#next, #prev").show();
		if (env.isAnyMobile) {
			$("#next, #prev").css("display", "inline").css("opacity", 0.5);
		} else {
			$("#next, #prev").off('mouseenter mouseleave');
			$("#next, #prev").off('mouseenter').on(
				'mouseenter',
				function() {
					$(this).stop().fadeTo("fast", 1);
				}
			);

			$("#next, #prev").off('mouseleave').on(
				'mouseleave',
				function() {
					$(this).stop().fadeTo("fast", 0.4);
				}
			);
		}
	};

	Utilities.addDescriptionOpacity = function() {
		$("#description-wrapper").css("opacity", "0.3");
		$("#album-and-media-container").css("opacity", "1");
	};

	Utilities.prototype.removeDescriptionOpacity = function() {
		$("#description-wrapper").css("opacity", "1");
		$("#album-and-media-container").css("opacity", "0.3");
	};

	Utilities.prototype.hideId = function(id) {
		$(id).hide();
	};

	Utilities.formatDescription = function(text) {
		// Replace CRLF by <p> and remove all useless <br>.
		text = text.replace(/<(\/?\w+)>\s*\n\s*<(\/?\w+)>/g, "<$1><$2>");
		text = text.replace(/\n/g, "</p><p>");
		if (text.substring(0, 3) !== "<p ")
			text = "<p>" + text + "</p>";
		return text;
	};

	Utilities.adaptSubalbumCaptionHeight = function() {
		// check for overflow in album-caption class in order to adapt album caption height to the string length
		// when diving into search subalbum, the whole album path is showed and it can be lengthy

		// execute twice, because the first run can activate the scroll bar and thus mess things
		for (var i = 0; i < 2; i ++) {
			var maxHeight = 0;
			var top = false;
			var objects = [];
			var counter = 0;
			var length = $('.album-caption').length;
			$('.album-caption').each(
				function() {
					counter ++;
					var newTop = $(this).parent().offset().top;
					if (top !== false && newTop != top) {
						// adapt!
						objects.forEach(
							function(object) {
								// object.css("height", maxHeight + 'px');
								var difference = maxHeight - parseFloat(object.css("height"));
								object.parent().css("height", (object.parent().height() + difference) + 'px');
								object.css("height", maxHeight + 'px');
							}
						);
						// newTop value must be recalculated
						newTop = $(this).parent().offset().top;
						maxHeight = 0;
						objects = [];
					}
					top = newTop;
					objects.push($(this));
					var thisHeight = $(this)[0].scrollHeight;
					maxHeight = (thisHeight > maxHeight) ? thisHeight : maxHeight;

					// one ore adaptation is needed for the last line
					if (counter === length) {
						// adapt!
						objects.forEach(
							function(object) {
								// object.css("height", maxHeight + 'px');
								var difference = maxHeight - parseFloat(object.css("height"));
								object.parent().css("height", (object.parent().height() + difference) + 'px');
								object.css("height", maxHeight + 'px');
							}
						);
					}
				}
			);
		}
	};

	Utilities.adaptMediaCaptionHeight = function(inPopup) {
		// check for overflow in media-caption class in order to adapt media caption height to the string length

		// execute twice, because the first run can activate the scroll bar and thus mess things
		for (var i = 0; i < 2; i ++) {
			var baseSelector = "#thumbs";
			if (inPopup)
				baseSelector = "#popup-images-wrapper";
			var maxHeight = 0;
			var top = false;
			var objects = [];
			var counter = 0;
			var length = $(baseSelector + " .media-caption").length;
			$(baseSelector + " .media-caption").css("height", 0);
			$(baseSelector + " .media-caption").each(
				function() {
					counter ++;
					var newTop = $(this).offset().top;
					if (top !== false && newTop !== top) {
						// adapt!
						objects.forEach(
							function(object) {
								object.css("height", maxHeight + 'px');
							}
						);
						// newTop value must be recalculated
						newTop = $(this).offset().top;
						maxHeight = 0;
						objects = [];
					}
					top = newTop;
					objects.push($(this));
					var thisHeight = $(this)[0].scrollHeight;
					maxHeight = (thisHeight > maxHeight) ? thisHeight : maxHeight;

					// one ore adaptation is needed for the last line
					if (counter === length) {
						// adapt!
						objects.forEach(
							function(object) {
								object.css("height", maxHeight + 'px');
							}
						);
					}
				}
			);
		}
	};

	Utilities.hasProperty = function(object, property) {
		if (! object.hasOwnProperty(property))
			return false;
		else
			// this[property] is array or string
			return object[property].length > 0;
	};

	Utilities.hasSomeDescription = function(albumOrSingleMediaOrMetadata, property = null) {
		var myObject;
		if (albumOrSingleMediaOrMetadata instanceof SingleMedia)
			myObject = albumOrSingleMediaOrMetadata.metadata;
		else
			myObject = albumOrSingleMediaOrMetadata;

		if (property)
			return Utilities.hasProperty(myObject, property);
		else
			return Utilities.hasProperty(myObject, "title") || Utilities.hasProperty(myObject, "description") || Utilities.hasProperty(myObject, "tags");
	};

	Utilities.prototype.setDescription = function(object) {
		var selector;
		if (object instanceof Album)
			selector = "#album-description-wrapper";
		else
			selector = "#single-media-description-wrapper";

		var hasTitle = Utilities.hasProperty(object, "title");
		var hasDescription = Utilities.hasProperty(object, "description");
		var hasTags = Utilities.hasProperty(object, "tags");

		if (
			! Utilities.hasSomeDescription(object) || (
				(
					! (hasTitle || hasDescription) || env.options.hide_descriptions
				) && (
					! hasTags || env.options.hide_tags
				)
			)
		) {
			$(selector).addClass("hidden");
		} else {
			$(selector).removeClass("hidden");
			$(selector).removeClass("hidden");

			// $(selector + " .description").css("max-height", (env.windowHeight / 2) + "px");

			if (! hasTitle && ! hasDescription) {
				$(selector + " .description-title").html("");
				$(selector + " .description-text").html("");
			} else {
				// $(selector + " .description").show();
				if (! hasTitle) {
					$(selector + " .description-title").hide();
					$(selector + " .description-title").html("");
				} else {
					$(selector + " .description-title").show();
					$(selector + " .description-title").html(Utilities.formatDescription(object.title));
				}

				if (! hasDescription) {
					$(selector + " .description-text").hide();
					$(selector + " .description-text").html("");
				} else {
					$(selector + " .description-text").show();
					$(selector + " .description-text").html(Utilities.formatDescription(object.description));
					$(selector + " .description-text p").addClass("description-p");
				}
			}

			if (! hasTags) {
				$(selector + " .description-tags").hide();
				$(selector + " .description-tags").html("");
			} else {
				let textualTags = Utilities._t("#tags") + ": <span class='tag'>" + object.tags.map(tag => Utilities.addTagLink(tag)).join("</span>, <span class='tag'>") + "</span>";
				$(selector + " .description-tags").show();
				$(selector + " .description-tags").html(textualTags);
			}
		}
	};

	Utilities.setDescriptionOptions = function() {
		var forceShowTags = false, hasDescription, hasTags ;

		if ($(".description-tags").html().indexOf(env.markTagBegin) > -1)
			forceShowTags = true;

		var albumHasSomeDescription = env.currentAlbum !== null && env.currentAlbum.hasSomeDescription();
		var singleMediaHasSomeDescription = env.currentMedia !== null && env.currentMedia.hasSomeDescription();

		var showAlbumDescription = albumHasSomeDescription && (forceShowTags || ! env.options.hide_descriptions || ! env.options.hide_tags);
		var showSingleMediaDescription = singleMediaHasSomeDescription && (forceShowTags || ! env.options.hide_descriptions || ! env.options.hide_tags);

		if (! showAlbumDescription && ! showSingleMediaDescription) {
			$("#description-wrapper").addClass("hidden-by-option");
		} else {
			$("#description-wrapper").removeClass("hidden-by-option");

			$("#album-description-wrapper").addClass("hidden-by-option");
			$("#single-media-description-wrapper").addClass("hidden-by-option");
			if (showAlbumDescription) {
				$("#album-description-wrapper").removeClass("hidden-by-option");
			}
			if (showSingleMediaDescription) {
				$("#single-media-description-wrapper").removeClass("hidden-by-option");
			}

			if (env.options.hide_descriptions)
				$(".description").addClass("hidden-by-option");
			else
				$(".description").removeClass("hidden-by-option");

			if (! forceShowTags && env.options.hide_tags)
				$(".description-tags").addClass("hidden-by-option");
			else
				$(".description-tags").removeClass("hidden-by-option");

			$("#description-wrapper").css("right", "");
			var thumbsHeight = 0;
			if (env.currentMedia !== null && $("#thumbs").is(":visible"))
				thumbsHeight = env.options.media_thumb_size + 20;
			$("#description-wrapper").css("bottom", thumbsHeight + 20);

			// $(".description-tags").css("right", $("#description-hide-show").outerWidth(true).toString() + "px");

			var maxHeight = Math.min(env.windowHeight / 4, 500);
			if (env.isAnyMobile)
				maxHeight = Math.min(env.windowHeight / 4, 400);

			var maxWidth = Math.min(env.windowWidth / 2, 500);
			if (env.isAnyMobile)
				maxWidth = Math.min(env.windowWidth / 2, 400);

			$("#description-wrapper").css("width", "");
			$("#description-wrapper").css("height", "");
			$("#description-wrapper").css("max-width", maxWidth.toString() + "px");
			for (var object of [env.currentMedia !== null ? env.currentMedia.metadata : null, env.currentAlbum]) {
				if (object === null)
					continue;
				let id;
				if (object instanceof Album)
					id = "#album-description-wrapper ";
				else
					id = "#single-media-description-wrapper ";
				hasDescription = Utilities.hasProperty(object, "title") || Utilities.hasProperty(object, "description");
				hasTags = Utilities.hasProperty(object, "tags");
				$(id + ".description").css("max-height", "");
				$(id + ".description-text").css("max-height", "");
				$(id + ".description-tags").css("max-height", "");
				$(id + ".description-tags").css("position", "");

				$(id + ".description-text").css("margin-bottom", "");
				$(id + ".description-title").css("margin-bottom", "");
				$(id + ".description-text").css("height", "");
				$(id + ".description").css("border", "");
				$(id + ".description-tags").css("position", "");
				$(id + ".description-tags").css("margin-left", "");
				// var bottomSpace = $("#description-hide-show").outerHeight();
				// if ($(".description-tags").is(":visible") && ! env.options.hide_tags && hasTags)
				// 	bottomSpace = Math.max(bottomSpace, $(".description-tags").outerHeight());
				if ($(id + ".description-text").is(":visible") && $(id + ".description-text").height() > 0) {
					// $(".description-text").css("margin-bottom", bottomSpace.toString() + "px");
				} else if ($(id + ".description-title").is(":visible") && $(id + ".description-title").height() > 0) {
					// $(".description-title").css("margin-bottom", bottomSpace.toString() + "px");
				} else {
					// $(".description").css("border", "0");
					$(id + ".description-tags").css("position", "relative");
					// $(".description-tags").css("margin-left", ($("#description-hide-show").outerWidth(true)) + "px");
				}

				// $(".description-tags").css("max-width", (maxWidth - 20).toString() + "px");
			}

			while (
				Math.max(
					$("#single-media-description-wrapper .description").outerWidth(true),
					$("#single-media-description-wrapper .description-tags").outerWidth(true),
					$("#album-description-wrapper .description").outerWidth(true),
					$("#album-description-wrapper .description-tags").outerWidth(true)
				) > $("#description-wrapper").innerWidth() &&
				$("#description-wrapper").width() < maxWidth
			) {
				$("#description-wrapper").css("width", ($("#description-wrapper").width() + 5) + "px");
			}

			// set visibility of the (possibly two) boxes
			if (albumHasSomeDescription && singleMediaHasSomeDescription) {
				// two boxes
				$("#description-show").hide();

				if (
					! $("#description-wrapper").hasClass("two") &&
					! $("#description-wrapper").hasClass("one") &&
					! $("#description-wrapper").hasClass("no")
				) {
					// first album/media, or comes from one box, box visible
					$("#description-wrapper").addClass("one");
					$("#description-wrapper").removeClass("yes");
				}

				if ($("#description-wrapper").hasClass("two")) {
					$("#description-hide").show();
					$("#description-show-1").hide();
					$("#description-show-2").hide();
					$(".description-wrapper").removeClass("hidden-by-button");
				} else if ($("#description-wrapper").hasClass("one")) {
					$("#description-hide").hide();
					$("#description-show-1").hide();
					$("#description-show-2").show();
					$("#album-description-wrapper").addClass("hidden-by-button");
					$("#single-media-description-wrapper").removeClass("hidden-by-button");
				} else if ($("#description-wrapper").hasClass("no")) {
					$("#description-hide").hide();
					$("#description-show-1").show();
					$("#description-show-2").hide();
					$(".description-wrapper").addClass("hidden-by-button");
				}
			} else {
				// one box
				$("#description-show-1").hide();
				$("#description-show-2").hide();

				if (
					! $("#description-wrapper").hasClass("yes") &&
					! $("#description-wrapper").hasClass("no")
				) {
					// first album/media, or comes from one box, box visible
					$("#description-wrapper").addClass("yes");
					$("#description-wrapper").removeClass("one");
					$("#description-wrapper").removeClass("two");
				}

				if ($("#description-wrapper").hasClass("yes")) {
					$("#description-hide").show();
					$("#description-show").hide();
					$(".description-wrapper").removeClass("hidden-by-button");
				} else if ($("#description-wrapper").hasClass("no")) {
					$("#description-hide").hide();
					$("#description-show").show();
					$(".description-wrapper").addClass("hidden-by-button");
				}
			}

			if ($(".description-text").is(":visible") && ! env.options.hide_descriptions && hasDescription) {
				$(".description-text").css("max-height", maxHeight.toString() + "px");
			} else if ($(".description-tags").is(":visible") && ! env.options.hide_tags && hasTags) {
				$(".description-tags").css("max-height", maxHeight.toString() + "px");
			} else {
				$(".description").css("max-height", maxHeight.toString() + "px");
			}

			$("#description-hide-show").off("click").on(
				"click",
				function() {
					$("#description-hide-show").css("position", "");
					if (albumHasSomeDescription && singleMediaHasSomeDescription) {
						// two boxes

						if ($("#description-wrapper").hasClass("two")) {
							$("#description-wrapper").removeClass("two");
							$("#description-wrapper").addClass("no");
							Utilities.addDescriptionOpacity();
						} else if ($("#description-wrapper").hasClass("one")) {
							$("#description-wrapper").removeClass("one");
							$("#description-wrapper").addClass("two");
						} else if ($("#description-wrapper").hasClass("no")) {
							$("#description-wrapper").removeClass("no");
							$("#description-wrapper").addClass("one");
						}
					} else {
						// one box

						if ($("#description-wrapper").hasClass("yes")) {
							$("#description-wrapper").removeClass("yes");
							$("#description-wrapper").addClass("no");
							Utilities.addDescriptionOpacity();
						} else if ($("#description-wrapper").hasClass("no")) {
							$("#description-wrapper").removeClass("no");
							$("#description-wrapper").addClass("yes");
						}
					}
					Utilities.setFinalOptions(env.currentMedia, false);
				}
			);
		}
	};

	Utilities.prototype.numClassesInWorking = function() {
		return $("#working")[0].classList.length > 1;
	};

	Utilities.prototype.mediaBoxGenerator = function(id) {
		if (env.originalMediaBoxContainerContent.indexOf("width") === -1) {
			// add the css width and height property at 0, so that the width is correct immediately
			env.originalMediaBoxContainerContent = $(env.originalMediaBoxContainerContent)
				.css("width", 0)
				.css("height", 0)
				[0].outerHTML;
		}

		if (id === 'left')
			$("#media-box-container").prepend(env.originalMediaBoxContainerContent.replace('id="center"', 'id="left"'));
		else if (id === 'right')
			$("#media-box-container").append(env.originalMediaBoxContainerContent.replace('id="center"', 'id="right"'));
		$(".media-box#" + id + " .metadata").css("display", $(".media-box#center .metadata").css("display"));
	};

	Utilities.prototype.showAuthForm = function(event, maybeProtectedContent = false) {
		$("#album-view, #media-view, #my-modal, #no-results").css("opacity", "0.2");
		$("#loading").hide();

		MenuFunctions.closeMenu();

		$("#no-results").hide();
		$("#auth-text").stop().fadeIn(1000);
		$("#password").focus();

		$('#auth-close').off("click").on(
			"click",
			function() {
				$("#auth-text").hide();
				$("#album-view, #media-view, #my-modal").css("opacity", "");
				if (env.currentAlbum === null) {
					env.fromEscKey = true;
					$(window).hashchange();
				} else if (maybeProtectedContent) {
					window.location.href = Utilities.upHash();
				}
			}
		);
	};

	Utilities.prototype.showPasswordRequestForm = function(event) {
		$("#auth-form").hide();
		$("#password-request-form").show();
		$("#please-fill").hide();
		$("#identity").attr("title", Utilities._t("#identity-explication"));
	};

	Utilities.upHash = function(hash) {
		var resultCacheBase;
		if (hash === undefined)
			hash = window.location.hash;
		var [albumCacheBase, mediaCacheBase, fake_mediaFolderCacheBase, foundAlbumCacheBase, collectionCacheBase] = PhotoFloat.decodeHash(hash);

		if (mediaCacheBase === null || env.currentAlbum !== null && env.currentAlbumIsAlbumWithOneMedia) {
			// hash of an album: go up in the album tree
			if (collectionCacheBase !== null) {
				if (albumCacheBase === foundAlbumCacheBase)
					resultCacheBase = collectionCacheBase;
				else {
					// we must go up in the sub folder
					albumCacheBase = albumCacheBase.split(env.options.cache_folder_separator).slice(0, -1).join(env.options.cache_folder_separator);
					resultCacheBase = Utilities.pathJoin([
						albumCacheBase,
						foundAlbumCacheBase,
						collectionCacheBase
					]);
				}
			} else {
				if (albumCacheBase === env.options.folders_string) {
					// stay there
					resultCacheBase = albumCacheBase;
				} else if ([env.options.by_date_string, env.options.by_gps_string].indexOf(albumCacheBase) !== -1) {
					// go to folders root
					resultCacheBase = env.options.folders_string;
				} else if (Utilities.isSearchCacheBase(albumCacheBase) || Utilities.isMapCacheBase(albumCacheBase)) {
					// the return folder must be extracted from the album hash
					// resultCacheBase = albumCacheBase.split(env.options.cache_folder_separator).slice(2).join(env.options.cache_folder_separator);
					resultCacheBase = env.options.cache_base_to_search_in;
				} else {
					let album = env.cache.getAlbum(albumCacheBase);
					if (Utilities.isSelectionCacheBase(albumCacheBase) && ! album) {
						resultCacheBase = env.options.folders_string;
					} else if (Utilities.isSelectionCacheBase(albumCacheBase) && album.media.length > 1) {
						// if all the media belong to the same album => the parent
						// other ways => the common root of the selected media
						let minimumLength = 100000;
						let parts = [];
						for (let iMedia = 0; iMedia < album.media.length; iMedia ++) {
							let splittedSelectedMediaCacheBase = album.media[iMedia].foldersCacheBase.split(env.options.cache_folder_separator);
							if (splittedSelectedMediaCacheBase.length < minimumLength)
								minimumLength = splittedSelectedMediaCacheBase.length;
						}
						for (let iPart = 0; iPart < minimumLength; iPart ++)
							parts[iPart] = [];
						for (let iMedia = 0; iMedia < album.media.length; iMedia ++) {
							let splittedSelectedMediaCacheBase = album.media[iMedia].foldersCacheBase.split(env.options.cache_folder_separator);
							for (let iPart = 0; iPart < minimumLength; iPart ++) {
								if (! iPart)
									parts[iPart][iMedia] = splittedSelectedMediaCacheBase[iPart];
								else
									parts[iPart][iMedia] = parts[iPart - 1][iMedia] + env.options.cache_folder_separator + splittedSelectedMediaCacheBase[iPart];
							}
						}
						resultCacheBase = '';
						for (let iPart = 0; iPart < minimumLength; iPart ++) {
							if (parts[iPart].some((val, i, arr) => val !== arr[0])) {
								break;
							} else {
								resultCacheBase = parts[iPart][0];
							}
						}
					} else if (Utilities.isSelectionCacheBase(albumCacheBase) && album.media.length === 1) {
						resultCacheBase = album.media[0].foldersCacheBase;
					} else {
						// we must go up in the sub folders tree
						resultCacheBase = albumCacheBase.split(env.options.cache_folder_separator).slice(0, -1).join(env.options.cache_folder_separator);
					}
				}
			}
		} else {
			// hash of a media: remove the media
			if (collectionCacheBase !== null || Utilities.isFolderCacheBase(albumCacheBase)) {
				// media in found album or in one of its subalbum
				// or
				// media in folder hash:
				// remove the trailing media
				resultCacheBase = Utilities.pathJoin(hash.split("/").slice(1, -1));
			} else {
				// all the other cases
				// remove the trailing media and the folder it's inside
				resultCacheBase = Utilities.pathJoin(hash.split("/").slice(1, -2));
			}
		}

		return env.hashBeginning + resultCacheBase;
	};


	/* Error displays */
	Utilities.prototype.errorThenGoUp = function(error) {
		if (error === 403) {
			$("#auth-text").stop().fadeIn(1000);
			$("#password").focus();
		} else {
			// Jason's code only had the following line
			//$("#error-text").stop().fadeIn(2500);

			var rootHash = env.hashBeginning + env.options.folders_string;

			$("#album-view").fadeOut(200);
			$("#media-view").fadeOut(200);

			$("#loading").hide();
			if (window.location.hash === rootHash) {
				$("#error-text-folder").stop();
				$("#error-root-folder").stop().fadeIn(2000);
				$("#powered-by").show();
			} else {
				$("#error-text-folder").stop().fadeIn(
					200,
					function() {
						window.location.href = Utilities.upHash();
					}
				);
				$("#error-text-folder, #error-overlay, #auth-text").fadeOut(3500);
				$("#album-view").stop().fadeOut(100).fadeIn(3500);
				$("#media-view").stop().fadeOut(100).fadeIn(3500);
			}
		}
		// $("#error-overlay").fadeTo(500, 0.8);
		$("body, html").css("overflow", "hidden");
	};

	Utilities.convertMd5ToCode = function(md5) {
		var index = env.guessedPasswordsMd5.indexOf(md5);
		return env.guessedPasswordCodes[index];
	};

	Utilities.prototype.noOp = function() {
		console.trace();
	};

	Utilities.prototype.convertCodesListToMd5sList = function(codesList) {
		var i, index, md5sList = [];
		for (i = 0; i < codesList.length; i ++) {
			if (! codesList[i]) {
				md5sList.push('');
			} else {
				index = env.guessedPasswordCodes.indexOf(codesList[i]);
				if (index != -1)
					md5sList.push(env.guessedPasswordsMd5[index]);
			}
		}
		return md5sList;
	};

	Utilities.prototype.convertCodesComplexCombinationToCodesSimpleCombination = function(codesComplexCombination) {
		let [albumCodesComplexCombinationList, mediaCodesComplexCombinationList] = PhotoFloat.convertComplexCombinationsIntoLists(codesComplexCombination);
		if (albumCodesComplexCombinationList.length && mediaCodesComplexCombinationList.length)
			return [albumCodesComplexCombinationList[0], mediaCodesComplexCombinationList[0]].join (',');
		else if (albumCodesComplexCombinationList.length && ! mediaCodesComplexCombinationList.length)
			return [albumCodesComplexCombinationList[0], ''].join (',');
		else if (! albumCodesComplexCombinationList.length && mediaCodesComplexCombinationList.length)
			return ['', mediaCodesComplexCombinationList[0]].join (',');
		else
			return '';
	};

	Utilities.prototype.videoOK = function() {
		if (! Modernizr.video || ! Modernizr.video.h264)
			return false;
		else
			return true;
	};

	Utilities.prototype.addVideoUnsupportedMarker = function(id) {
		if (! Modernizr.video) {
			$(".media-box#" + id + " .media-box-inner").html('<div class="video-unsupported-html5"></div>');
			return false;
		}
		else if (! Modernizr.video.h264) {
			$(".media-box#" + id + " .media-box-inner").html('<div class="video-unsupported-h264"></div>');
			return false;
		} else
			return true;
	};

	Utilities.prototype.undie = function() {
		$(".error, #error-overlay, #auth-text", ".search-failed").fadeOut(500);
		$("body, html").css("overflow", "auto");
	};

	Utilities.prototype.humanFileSize = function(fileSize) {
		// from https://stackoverflow.com/questions/10420352/converting-file-size-in-bytes-to-human-readable-string
		if (! fileSize)
			return 0;
		var i = Math.floor(Math.log(fileSize) / Math.log(1024));
		return env.numberFormat.format((fileSize / Math.pow(1024, i)).toFixed(2) * 1) + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
	};

	Utilities.prototype.getAlbumNameFromCacheBase = function(cacheBase) {
		return new Promise(
			function(resolve_getAlbumNameFromAlbumHash) {
				let getAlbumPromise = PhotoFloat.getAlbum(cacheBase, Utilities.noOp, {getMedia: false, getPositions: false});
				getAlbumPromise.then(
					function(theAlbum) {
						var path;
						var splittedPath = theAlbum.path.split('/');
						if (splittedPath[0] === env.options.folders_string) {
							splittedPath[0] = "";
						} else if (splittedPath[0] === env.options.by_date_string) {
							splittedPath[0] = "(" + Utilities._t("#by-date") + ")";
						} else if (splittedPath[0] === env.options.by_gps_string) {
							splittedPath = theAlbum.ancestorsNames;
							splittedPath[0] = "(" + Utilities._t("#by-gps") + ")";
						} else if (splittedPath[0] === env.options.by_map_string) {
							splittedPath = ["(" + Utilities._t("#by-map") + ")"];
						}
						path = splittedPath.join('/');

						resolve_getAlbumNameFromAlbumHash(path);
					},
					function() {
						console.trace();
					}
				);
			}
		);
	};

	Utilities.prototype.toggleBigAlbumsShow = function(ev) {
		if ((ev.button === 0 || ev.button === undefined) && ! ev.shiftKey && ! ev.ctrlKey && ! ev.altKey) {
			if ($("#message-too-many-images").is(":visible")) {
				$("#message-too-many-images").css("display", "");
			}
			$("#loading").show();
			env.options.show_big_virtual_folders = ! env.options.show_big_virtual_folders;
			if (env.options.show_big_virtual_folders)
				$("#show-hide-them:hover").css("color", "").css("cursor", "");
			else
				$("#show-hide-them:hover").css("color", "inherit").css("cursor", "auto");
			MenuFunctions.setBooleanCookie("showBigVirtualFolders", env.options.show_big_virtual_folders);
			MenuFunctions.updateMenu();
			env.currentAlbum.showMedia();
			$("#loading").hide();
		}
		return false;
	};

	Utilities.prototype.toggleInsideWordsSearch = function() {
		env.options.search_inside_words = ! env.options.search_inside_words;
		MenuFunctions.setBooleanCookie("searchInsideWords", env.options.search_inside_words);
		MenuFunctions.updateMenu();
		if ($("#search-field").val().trim())
			$('#search-button').click();
	};

	Utilities.prototype.toggleAnyWordSearch = function() {
		env.options.search_any_word = ! env.options.search_any_word;
		MenuFunctions.setBooleanCookie("searchAnyWord", env.options.search_any_word);
		MenuFunctions.updateMenu();
		if (env.searchWords.length < 2)
			return;
		if ($("#search-field").val().trim())
			$('#search-button').click();
	};

	Utilities.prototype.toggleCaseSensitiveSearch = function() {
		env.options.search_case_sensitive = ! env.options.search_case_sensitive;
		MenuFunctions.setBooleanCookie("searchCaseSensitive", env.options.search_case_sensitive);
		MenuFunctions.updateMenu();
		if ($("#search-field").val().trim())
			$('#search-button').click();
	};

	Utilities.prototype.toggleAccentSensitiveSearch = function() {
		env.options.search_accent_sensitive = ! env.options.search_accent_sensitive;
		MenuFunctions.setBooleanCookie("searchAccentSensitive", env.options.search_accent_sensitive);
		MenuFunctions.updateMenu();
		if ($("#search-field").val().trim())
			$('#search-button').click();
	};

	Utilities.prototype.toggleTagsOnlySearch = function() {
		env.options.search_tags_only = ! env.options.search_tags_only;
		MenuFunctions.setBooleanCookie("searchTagsOnly", env.options.search_tags_only);
		MenuFunctions.updateMenu();
		if ($("#search-field").val().trim())
			$('#search-button').click();
	};

	Utilities.prototype.toggleCurrentAbumSearch = function() {
		env.options.search_current_album = ! env.options.search_current_album;
		MenuFunctions.setBooleanCookie("searchCurrentAlbum", env.options.search_current_album);
		MenuFunctions.updateMenu();
		if ($("#search-field").val().trim())
			$('#search-button').click();
	};

	Utilities.prototype.highlightSearchedWords = function(whenTyping = false) {
		var isSearch = Utilities.isSearchHash();
		if (isSearch || whenTyping) {
			// highlight the searched text

			let searchWords;
			if (whenTyping) {
				let searchString = $("#search-field").val().trim();
				if (env.options.search_tags_only) {
					searchWords = [searchString];
				} else {
					searchWords = searchString.split(' ');
				}
			} else {
				searchWords = env.searchWords;
			}

			let selector = ".title .media-name, .description, .description-tags .tag";

			let baseSelector = "";
			if (Utilities.isPopup())
				baseSelector = "#popup-images-wrapper";
			else if (env.currentMedia === null)
				baseSelector = "#album-and-media-container";

			let mediaNameSelector = ".media-name";
			let albumNameSelector = ".album-name";
			if ($("#subalbums .first-line, #thumbs .first-line").length) {
				mediaNameSelector += " .first-line";
				albumNameSelector += " .first-line";
			}

			if (baseSelector.length) {
				selector += ", " +
					baseSelector + " " + mediaNameSelector + ", " +
					baseSelector + " " + albumNameSelector + ", " +
					baseSelector + " .media-description, " +
					baseSelector + " .album-description, " +
					baseSelector + " .album-tags, " +
					baseSelector + " .media-tags";
			}

			var punctuationArray = ":;.,-–—‒_(){}[]¡!`´·#|@~&/*^¨'+=?¿<>\\\"".split("");
			const options = {
				caseSensitive: env.options.search_case_sensitive,
				diacritics: ! env.options.search_accent_sensitive,
				separateWordSearch: whenTyping ? true : env.options.search_any_word,
				accuracy: env.options.search_inside_words || whenTyping ? "partially" : {
					value: "exactly",
					limiters: punctuationArray
				},
				ignorePunctuation: punctuationArray
			};
			$(selector).unmark(
				{
					done: function() {
						$(selector).mark(
							searchWords,
							options
						);
					}
				}
			);

			// make the descritpion and the tags visible if highlighted
			for (let selector of ["#subalbums", "#thumbs"]) {
				let adapt = false;
				$(selector + " .description.ellipsis").each(
					function() {
						if ($(this).html().indexOf(env.markTagBegin) !== -1) {
							$(this).css("text-overflow", "unset").css("overflow", "visible").css("white-space", "unset");
							adapt = true;
						}
					}
				);
				$(selector + " .album-tags, " + selector + " .media-tags").each(
					function() {
						if ($(this).html().indexOf(env.markTagBegin) !== -1) {
							$(this).removeClass("hidden-by-option");
							adapt = true;
						}
					}
				);
				if (adapt) {
					if (selector === "#subalbums") {
						Utilities.adaptSubalbumCaptionHeight();
					} else {
						Utilities.adaptMediaCaptionHeight(false);
						Utilities.adaptMediaCaptionHeight(true);
					}
				}
			}

			// bottom right tags will be made visible if highlighted in Utilities.setDescriptionOptions()

			// make visible the file name if it's highlighted
			let html = $("#media-name-second-part").html();
			if (html && html.indexOf("<mark data-markjs=") !== -1) {
				$(".with-second-part").addClass("hovered");
			}
		}
	};

	Utilities.prototype.horizontalDistance = function(object1, object2) {
		var leftOffset1 = object1.offset().left;
		var leftOffset2 = object2.offset().left;
		var rightOffset1 = leftOffset1 + object1.outerWidth();
		var rightOffset2 = leftOffset2 + object2.outerWidth();
		return ((rightOffset2 + leftOffset2) / 2 - (rightOffset1 + leftOffset1) / 2);
	};

	Utilities.prototype.verticalDistance = function(object1, object2) {
		var img1 = object1.children().first().children("img");
		var img2 = object2.children().first().children("img");
		var topOffset1 = img1.offset().top;
		var topOffset2 = img2.offset().top;
		var bottomOffset1 = topOffset1 + img1.outerHeight();
		var bottomOffset2 = topOffset2 + img2.outerHeight();
		return ((topOffset2 + bottomOffset2) / 2 - (topOffset1 + bottomOffset1) / 2);
	};

	Utilities.prototype.tenYears = function() {
		// returns the expire interval for the cookies, in seconds
		return 10 * 365 * 24 * 60 * 60;
	};

	Utilities.prototype.settingsChanged = function() {
		return (
			env.albumNameSort !== env.options.default_album_name_sort ||
			env.albumReverseSort !== env.options.default_album_reverse_sort ||
			env.mediaNameSort !== env.options.default_media_name_sort ||
			env.mediaReverseSort !== env.options.default_media_reverse_sort ||
			env.defaultOptions.show_album_media_count !== env.options.show_album_media_count ||
			env.defaultOptions.hide_title !== env.options.hide_title ||
			$("#album-and-media-container").hasClass("show-title") ||
			env.defaultOptions.albums_slide_style !== env.options.albums_slide_style ||
			env.defaultOptions.album_thumb_type !== env.options.album_thumb_type ||
			env.defaultOptions.show_album_names_below_thumbs !== env.options.show_album_names_below_thumbs ||
			env.defaultOptions.media_thumb_type !== env.options.media_thumb_type ||
			env.defaultOptions.show_media_names_below_thumbs !== env.options.show_media_names_below_thumbs ||
			env.defaultOptions.hide_descriptions !== env.options.hide_descriptions ||
			env.defaultOptions.hide_tags !== env.options.hide_tags ||
			env.defaultOptions.thumb_spacing !== env.options.spacing ||
			env.defaultOptions.hide_bottom_thumbnails !== env.options.hide_bottom_thumbnails ||
			env.defaultOptions.save_data !== env.options.save_data ||
			env.options.search_inside_words ||
			env.options.search_any_word ||
			env.options.search_case_sensitive ||
			env.options.search_accent_sensitive ||
			env.options.search_tags_only ||
			! env.options.search_current_album ||
			env.options.show_big_virtual_folders ||
			env.slideshowInterval !== env.slideshowIntervalDefault
		);
	};

	Utilities.prototype.hideContextualHelp = function() {
		$("#contextual-help").stop().fadeTo(
			200,
			0,
			function() {
				$("#contextual-help").removeClass("visible");
				$("#contextual-help").hide();
			}
		);
		$("#album-and-media-container").stop().fadeTo(200, 1);
		if ($("#my-modal").hasClass("fadedOut")) {
			$("#my-modal").removeClass("fadedOut").stop().fadeTo(200, 1);
		}

	};

	Utilities.prototype.showContextualHelp = function() {
		if ($("#auth-text").is(":visible") || $("#contextual-help").hasClass("visible"))
			return;
		$("#album-and-media-container").stop().fadeTo(500, 0.1);
		if ($("#my-modal").is(":visible")) {
			$("#my-modal").addClass("fadedOut").stop().fadeTo(500, 0.1);
		}
		// hide everything except scope "any"
		$("#contextual-help .shortcuts tr td.scope:not(.any)").parent().hide();
		// show what is pertinent
		if ($("#right-and-search-menu .menu").hasClass("expanded")) {
			// is any menu
			$(
				"#contextual-help .shortcuts tr td.scope.menu, " +
				"#contextual-help .shortcuts tr td.scope.expandable-menu, " +
				"#contextual-help .shortcuts tr td.scope.menu-command"
			).parent().show();
		} else if (env.currentMedia !== null && ! Utilities.isMap() && ! Utilities.isPopup()) {
			// is a single media
			$(
				"#contextual-help .shortcuts tr td.scope.any-but-menu, " +
				"#contextual-help .shortcuts tr td.scope.album-and-single-media, " +
				"#contextual-help .shortcuts tr td.scope.single-media, " +
				"#contextual-help .shortcuts tr td.scope.root-albums-and-single-media, " +
				"#contextual-help .shortcuts tr td.scope.enlarged-image, " +
				"#contextual-help .shortcuts tr td.scope.video"
			).parent().show();
		} else if (! Utilities.isMap()) {
			// is an album or a popup
			$(
				"#contextual-help .shortcuts tr td.scope.any-but-menu, " +
				"#contextual-help .shortcuts tr td.scope.album, " +
				"#contextual-help .shortcuts tr td.scope.album-and-single-media"
			).parent().show();
			if (env.currentAlbum.isAnyRoot() && ! Utilities.isPopup()) {
				// is a root album
				$(
					"#contextual-help .shortcuts tr td.scope.any-but-menu, " +
					"#contextual-help .shortcuts tr td.scope.root-albums-and-single-media"
				).parent().show();
			}
		}
		$("#contextual-help").stop().fadeTo(
			10,
			0.1,
			function() {
				$("#contextual-help").addClass("visible");
				$("#contextual-help").css("width", "unset");
				var currentWidth = parseInt($("#contextual-help").css("width"));
				var tableCurrentWidth = parseInt($("#contextual-help table.shortcuts").css("width"));
				$("#contextual-help").css("width", Math.min(currentWidth, tableCurrentWidth + 100).toString() + "px");
				$("#contextual-help").stop().fadeTo(500, 1);
			}
		);
	};

	/* make static methods callable as member functions */
	Utilities.prototype.isFolderCacheBase = Utilities.isFolderCacheBase;
	Utilities.prototype.pathJoin = Utilities.pathJoin;
	Utilities.prototype.setLinksVisibility = Utilities.setLinksVisibility;
	Utilities.prototype.setPrevNextVisibility = Utilities.setPrevNextVisibility;
	Utilities.prototype.currentSizeAndIndex = Utilities.currentSizeAndIndex;
	Utilities.prototype.distanceBetweenCoordinatePoints = Utilities.distanceBetweenCoordinatePoints;
	Utilities.prototype.xDistanceBetweenCoordinatePoints = Utilities.xDistanceBetweenCoordinatePoints;
	Utilities.prototype.yDistanceBetweenCoordinatePoints = Utilities.yDistanceBetweenCoordinatePoints;
	Utilities.prototype.degreesToRadians = Utilities.degreesToRadians;
	Utilities.prototype.isByDateCacheBase = Utilities.isByDateCacheBase;
	Utilities.prototype.isByGpsCacheBase = Utilities.isByGpsCacheBase;
	Utilities.prototype.isSearchCacheBase = Utilities.isSearchCacheBase;
	Utilities.prototype.isSelectionCacheBase = Utilities.isSelectionCacheBase;
	Utilities.prototype.isMapCacheBase = Utilities.isMapCacheBase;
	Utilities.prototype.isAnyRootCacheBase = Utilities.isAnyRootCacheBase;
	Utilities.prototype.convertMd5ToCode = Utilities.convertMd5ToCode;
	Utilities.prototype._t = Utilities._t;
	Utilities.prototype._s = Utilities._s;
	Utilities.prototype.upHash = Utilities.upHash;
	Utilities.prototype.nothingIsSelected = Utilities.nothingIsSelected;
	Utilities.prototype.somethingIsSearched = Utilities.somethingIsSearched;
	Utilities.prototype.somethingIsInMapAlbum = Utilities.somethingIsInMapAlbum;
	Utilities.prototype.transformAltPlaceName = Utilities.transformAltPlaceName;
	Utilities.prototype.normalizeAccordingToOptions = Utilities.normalizeAccordingToOptions;
	Utilities.prototype.removeAccents = Utilities.removeAccents;
	Utilities.prototype.addTagLink = Utilities.addTagLink;
	Utilities.prototype.isShiftOrControl = Utilities.isShiftOrControl;
	Utilities.prototype.isPopup = Utilities.isPopup;
	Utilities.prototype.setTitleOptions = Utilities.setTitleOptions;
	Utilities.prototype.setSocialButtons = Utilities.setSocialButtons;
	Utilities.prototype.setMediaOptions = Utilities.setMediaOptions;
	Utilities.prototype.setFinalOptions = Utilities.setFinalOptions;
	Utilities.prototype.openInNewTab = Utilities.openInNewTab;
	Utilities.prototype.isMap = Utilities.isMap;
	Utilities.prototype.removeHighligths = Utilities.removeHighligths;
	Utilities.prototype.isSearchHash = Utilities.isSearchHash;
	Utilities.prototype.adaptSubalbumCaptionHeight = Utilities.adaptSubalbumCaptionHeight;
	Utilities.prototype.adaptMediaCaptionHeight = Utilities.adaptMediaCaptionHeight;
	Utilities.prototype.addDescriptionOpacity = Utilities.addDescriptionOpacity;
	Utilities.prototype.geotaggedContentIsHidden = Utilities.geotaggedContentIsHidden;
	Utilities.prototype.hasSomeDescription = Utilities.hasSomeDescription;
	Utilities.prototype.objectIsASubalbum = Utilities.objectIsASubalbum;
	Utilities.prototype.addMediaLazyLoader = Utilities.addMediaLazyLoader;
	Utilities.prototype.isPhp = Utilities.isPhp;

	window.Utilities = Utilities;
}());
