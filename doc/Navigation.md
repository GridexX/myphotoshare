# User interface

## Keyboard navigation

MyPhotoShare can be used entirely with a keyboard.

|         Scope         |         Key         |                           Action                                 |
|-----------------------|---------------------|------------------------------------------------------------------|
| any                   | ESC                 | Quit current action, go up in the albums tree, hide map, ecc.    |
| any but menu          | e                   | Open menu                                                        |
| any but menu          | m                   | Show map                                                         |
| any                   | f                   | Full screen                                                      |
|-----------------------|---------------------|------------------------------------------------------------------|
| menu                  | ESC                 | Closes the menu                                                  |
| menu                  | TAB, DOWN ARROW     | Move highlight to next menu entry                                |
| menu                  | SHIFT-TAB, UP ARROW | Move highlight to previous menu entry                            |
| menu                  | LEFT/RIGHT ARROW    | Toggle main/search menu                                          |
| expandable menu entry | ENTER, SPACE        | Toggle menu entry expansion                                      |
| menu commands         | ENTER, SPACE        | Activate menu command                                            |
|-----------------------|---------------------|------------------------------------------------------------------|
| root albums           | >                   | Change browsing mode: albums > date > GPS > search > selection   |
| root albums           | <                   | Change browsing mode reverse                                     |
| album                 | ENTER               | enter highlighted album/single media                             |
| album                 | LEFT ARROW          | move selection backward circularly                               |
| album                 | RIGHT ARROW         | move selection forward circularly                                |
| album                 | ]                   | Change album sorting: name asc > name desc > date desc > date asc |
| album                 | [                   | Change album sorting reverse                                     |
| album                 | }                   | Change media sorting: name asc > date asc > name desc > date desc |
| album                 | {                   | Change media sorting reverse                                     |
| album                 | h                   | Show/hide title                                                  |
| album                 | a                   | Select all subalbums and media                                   |
| album                 | SHIFT-a             | Unselect all media                                               |
| album                 | SPACE, CTRL-SPACE   | toggle selection status of highlighted subalbum/media            |
|-----------------------|---------------------|------------------------------------------------------------------|
| single media          | ENTER               | Next media                                                       |
| single media          | SHIFT-ENTER         | Previous media                                                   |
| single media          | RIGHT ARROW         | Next media                                                       |
| single media          | SHIFT-UP ARROW      | Album view                                                       |
| single media          | LEFT ARROW          | Previous media                                                   |
| single media          | s                   | Show metadata                                                    |
| single media          | o                   | Show original media                                              |
| single media          | d                   | Download original media                                          |
| single media          | +                   | Zoom in picture                                                  |
| single media          | -                   | Zoom out picture                                                 |
| single media          | SHIFT-SPACE         | Previous media                                                   |
| single media          | BACKSPACE           | Previous media                                                   |
| single media          | SHIFT-BACKSPACE     | Next media                                                       |
| single media          | n                   | Next media                                                       |
| single media          | SHIFT-n             | Previous media                                                   |
| single media          | p                   | Previous media                                                   |
| single media          | SHIFT-p             | Next media                                                       |
| single media          | SHIFT-PAGE UP       | Album view                                                       |
| single media          | >                   | Change browsing mode: albums > date > GPS > search > selection   |
| single media          | <                   | Change browsing mode reverse                                     |
| single media          | h                   | Show/hide title and bottom thumbnails                            |
| single media          | 0 .. 9              | Zoom level                                                       |
| single media          | CTRL-SPACE          | toggle selection status                                          |
| video                 | SPACE               | play/stop                                                        |
| picture               | SPACE               | Next picture                                                     |
| enlarged picture      | ARROWS              | Drag                                                             |
| enlarged picture      | SHIFT-ARROWS        | Drag faster                                                      |
| enlarged picture      | PAGE UP/DOWN        | Drag                                                             |
| enlarged picture      | SHIFT-PAGE UP/DOWN  | Drag faster                                                      |
|-----------------------|---------------------|------------------------------------------------------------------|
| slideshow (not video) | SPACE               | pause/play                                                     |
|-----------------------|---------------------|------------------------------------------------------------------|

### Accessibility (_not implemented yet_)

HTML access keys are also enabled for some HTML items and can be used with assistive browsers.

| Key              | Action                                                     |
|------------------|------------------------------------------------------------|
| p                | Previous media or folder                                   |
| n                | Next media or folder                                       |
| +                | Zoom in picture                                            |
| -                | Zoom out picture                                           |
| e                | Open menu                                                  |
| k                | Search keyword                                             |
| b                | Browse folder options                                      |
| t                | Sort folder options                                        |
| s                | Sort picture options                                       |
| a                | Display options                                            |
| d                | Download options                                           |

Unfortunately, access keys depend on the browser and platform.

| Browser    | Windows                        | Linux                             | Mac                               |
|------------|--------------------------------|-----------------------------------|-----------------------------------|
| Chrome     | [Alt] + [SHIFT] + [key]        | [Control] + [Alt] + [key]         | [Control] + [Alt] + [key]         |
| Firefox    | [Alt] + [SHIFT] + [key]        | [Alt] + [SHIFT] + [key]           | [Control] + [Alt] + [key]         |
| Edge       | [Alt] + [key]                  |                                   |                                   |
| IE         | [Alt] + [key]                  |                                   |                                   |
| Safari     | [Alt] + [key]                  |                                   | [Control] + [Alt] + [key]         |
| Opera      | [Alt] + [key]                  |                                   | [Control] + [Alt] + [key]         |

## Mouse
